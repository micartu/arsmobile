//
//  DTAListChooserInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAListChooserInteractor.h"

#import "DTAListChooserInteractorOutput.h"

@interface DTAListChooserInteractorTests : XCTestCase

@property (nonatomic, strong) DTAListChooserInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAListChooserInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTAListChooserInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAListChooserInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAListChooserInteractorInput

@end
