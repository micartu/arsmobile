//
//  DTAListChooserViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAListChooserViewController.h"

#import "DTAListChooserViewOutput.h"

@interface DTAListChooserViewControllerTests : XCTestCase

@property (nonatomic, strong) DTAListChooserViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAListChooserViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTAListChooserViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAListChooserViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTAListChooserViewInput

@end
