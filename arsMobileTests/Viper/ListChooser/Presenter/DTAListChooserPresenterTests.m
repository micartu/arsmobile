//
//  DTAListChooserPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAListChooserPresenter.h"

#import "DTAListChooserViewInput.h"
#import "DTAListChooserInteractorInput.h"
#import "DTAListChooserRouterInput.h"

@interface DTAListChooserPresenterTests : XCTestCase

@property (nonatomic, strong) DTAListChooserPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTAListChooserPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTAListChooserPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTAListChooserInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTAListChooserRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTAListChooserViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAListChooserModuleInput

#pragma mark - Testing of methods DTAListChooserViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTAListChooserInteractorOutput

@end
