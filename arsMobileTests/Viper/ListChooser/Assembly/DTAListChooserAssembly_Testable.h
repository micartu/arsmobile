//
//  DTAListChooserAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserAssembly.h"

@class DTAListChooserViewController;
@class DTAListChooserInteractor;
@class DTAListChooserPresenter;
@class DTAListChooserRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTAListChooserAssembly ()

- (DTAListChooserViewController *)viewListChooser;
- (DTAListChooserPresenter *)presenterListChooser;
- (DTAListChooserInteractor *)interactorListChooser;
- (DTAListChooserRouter *)routerListChooser;

@end
