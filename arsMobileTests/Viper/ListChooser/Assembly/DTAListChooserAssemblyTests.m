//
//  DTAListChooserAssemblyTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <RamblerTyphoonUtils/AssemblyTesting.h>
#import <Typhoon/Typhoon.h>

#import "DTAListChooserAssembly.h"
#import "DTAListChooserAssembly_Testable.h"

#import "DTAListChooserViewController.h"
#import "DTAListChooserPresenter.h"
#import "DTAListChooserInteractor.h"
#import "DTAListChooserRouter.h"

@interface DTAListChooserAssemblyTests : RamblerTyphoonAssemblyTests

@property (nonatomic, strong) DTAListChooserAssembly *assembly;

@end

@implementation DTAListChooserAssemblyTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.assembly = [[DTAListChooserAssembly alloc] init];
    [self.assembly activate];
}

- (void)tearDown {
    self.assembly = nil;

    [super tearDown];
}

#pragma mark - Testing of elements creation

- (void)testThatAssemblyCreatesViewController {
    // given
    Class targetClass = [DTAListChooserViewController class];
    NSArray *protocols = @[
                           @protocol(DTAListChooserViewInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly viewListChooser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesPresenter {
    // given
    Class targetClass = [DTAListChooserPresenter class];
    NSArray *protocols = @[
                           @protocol(DTAListChooserModuleInput),
                           @protocol(DTAListChooserViewOutput),
                           @protocol(DTAListChooserInteractorOutput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(interactor),
                              RamblerSelector(view),
                              RamblerSelector(router)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly presenterListChooser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesInteractor {
    // given
    Class targetClass = [DTAListChooserInteractor class];
    NSArray *protocols = @[
                           @protocol(DTAListChooserInteractorInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly interactorListChooser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesRouter {
    // given
    Class targetClass = [DTAListChooserRouter class];
    NSArray *protocols = @[
                           @protocol(DTAListChooserRouterInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(transitionHandler)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly routerListChooser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

@end
