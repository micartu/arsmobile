//
//  DTAListChooserRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAListChooserRouter.h"

@interface DTAListChooserRouterTests : XCTestCase

@property (nonatomic, strong) DTAListChooserRouter *router;

@end

@implementation DTAListChooserRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTAListChooserRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
