//
//  DTADiscountAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountAssembly.h"

@class DTADiscountViewController;
@class DTADiscountInteractor;
@class DTADiscountPresenter;
@class DTADiscountRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTADiscountAssembly ()

- (DTADiscountViewController *)viewDiscount;
- (DTADiscountPresenter *)presenterDiscount;
- (DTADiscountInteractor *)interactorDiscount;
- (DTADiscountRouter *)routerDiscount;

@end
