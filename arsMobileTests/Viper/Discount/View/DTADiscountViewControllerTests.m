//
//  DTADiscountViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTADiscountViewController.h"

#import "DTADiscountViewOutput.h"

@interface DTADiscountViewControllerTests : XCTestCase

@property (nonatomic, strong) DTADiscountViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTADiscountViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTADiscountViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTADiscountViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTADiscountViewInput

@end
