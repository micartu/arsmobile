//
//  DTADiscountRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTADiscountRouter.h"

@interface DTADiscountRouterTests : XCTestCase

@property (nonatomic, strong) DTADiscountRouter *router;

@end

@implementation DTADiscountRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTADiscountRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
