//
//  DTADiscountInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTADiscountInteractor.h"

#import "DTADiscountInteractorOutput.h"

@interface DTADiscountInteractorTests : XCTestCase

@property (nonatomic, strong) DTADiscountInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTADiscountInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTADiscountInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTADiscountInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTADiscountInteractorInput

@end
