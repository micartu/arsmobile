//
//  DTADiscountPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTADiscountPresenter.h"

#import "DTADiscountViewInput.h"
#import "DTADiscountInteractorInput.h"
#import "DTADiscountRouterInput.h"

@interface DTADiscountPresenterTests : XCTestCase

@property (nonatomic, strong) DTADiscountPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTADiscountPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTADiscountPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTADiscountInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTADiscountRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTADiscountViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTADiscountModuleInput

#pragma mark - Testing of methods DTADiscountViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTADiscountInteractorOutput

@end
