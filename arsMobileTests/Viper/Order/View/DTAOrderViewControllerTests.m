//
//  DTAOrderViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAOrderViewController.h"

#import "DTAOrderViewOutput.h"

@interface DTAOrderViewControllerTests : XCTestCase

@property (nonatomic, strong) DTAOrderViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAOrderViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTAOrderViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAOrderViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTAOrderViewInput

@end
