//
//  DTAOrderInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAOrderInteractor.h"

#import "DTAOrderInteractorOutput.h"

@interface DTAOrderInteractorTests : XCTestCase

@property (nonatomic, strong) DTAOrderInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAOrderInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTAOrderInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAOrderInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAOrderInteractorInput

@end
