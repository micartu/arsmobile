//
//  DTAOrderRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAOrderRouter.h"

@interface DTAOrderRouterTests : XCTestCase

@property (nonatomic, strong) DTAOrderRouter *router;

@end

@implementation DTAOrderRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTAOrderRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
