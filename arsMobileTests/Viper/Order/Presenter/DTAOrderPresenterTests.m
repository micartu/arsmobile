//
//  DTAOrderPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAOrderPresenter.h"

#import "DTAOrderViewInput.h"
#import "DTAOrderInteractorInput.h"
#import "DTAOrderRouterInput.h"

@interface DTAOrderPresenterTests : XCTestCase

@property (nonatomic, strong) DTAOrderPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTAOrderPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTAOrderPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTAOrderInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTAOrderRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTAOrderViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAOrderModuleInput

#pragma mark - Testing of methods DTAOrderViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTAOrderInteractorOutput

@end
