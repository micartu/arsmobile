//
//  DTAOrderAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderAssembly.h"

@class DTAOrderViewController;
@class DTAOrderInteractor;
@class DTAOrderPresenter;
@class DTAOrderRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTAOrderAssembly ()

- (DTAOrderViewController *)viewOrder;
- (DTAOrderPresenter *)presenterOrder;
- (DTAOrderInteractor *)interactorOrder;
- (DTAOrderRouter *)routerOrder;

@end
