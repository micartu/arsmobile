//
//  DTAOrderAssemblyTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <RamblerTyphoonUtils/AssemblyTesting.h>
#import <Typhoon/Typhoon.h>

#import "DTAOrderAssembly.h"
#import "DTAOrderAssembly_Testable.h"

#import "DTAOrderViewController.h"
#import "DTAOrderPresenter.h"
#import "DTAOrderInteractor.h"
#import "DTAOrderRouter.h"

@interface DTAOrderAssemblyTests : RamblerTyphoonAssemblyTests

@property (nonatomic, strong) DTAOrderAssembly *assembly;

@end

@implementation DTAOrderAssemblyTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.assembly = [[DTAOrderAssembly alloc] init];
    [self.assembly activate];
}

- (void)tearDown {
    self.assembly = nil;

    [super tearDown];
}

#pragma mark - Testing of elements creation

- (void)testThatAssemblyCreatesViewController {
    // given
    Class targetClass = [DTAOrderViewController class];
    NSArray *protocols = @[
                           @protocol(DTAOrderViewInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly viewOrder];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesPresenter {
    // given
    Class targetClass = [DTAOrderPresenter class];
    NSArray *protocols = @[
                           @protocol(DTAOrderModuleInput),
                           @protocol(DTAOrderViewOutput),
                           @protocol(DTAOrderInteractorOutput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(interactor),
                              RamblerSelector(view),
                              RamblerSelector(router)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly presenterOrder];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesInteractor {
    // given
    Class targetClass = [DTAOrderInteractor class];
    NSArray *protocols = @[
                           @protocol(DTAOrderInteractorInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly interactorOrder];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesRouter {
    // given
    Class targetClass = [DTAOrderRouter class];
    NSArray *protocols = @[
                           @protocol(DTAOrderRouterInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(transitionHandler)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly routerOrder];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

@end
