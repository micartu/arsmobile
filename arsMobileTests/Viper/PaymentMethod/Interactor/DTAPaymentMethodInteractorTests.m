//
//  DTAPaymentMethodInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentMethodInteractor.h"

#import "DTAPaymentMethodInteractorOutput.h"

@interface DTAPaymentMethodInteractorTests : XCTestCase

@property (nonatomic, strong) DTAPaymentMethodInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAPaymentMethodInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTAPaymentMethodInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAPaymentMethodInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAPaymentMethodInteractorInput

@end
