//
//  DTAPaymentMethodPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentMethodPresenter.h"

#import "DTAPaymentMethodViewInput.h"
#import "DTAPaymentMethodInteractorInput.h"
#import "DTAPaymentMethodRouterInput.h"

@interface DTAPaymentMethodPresenterTests : XCTestCase

@property (nonatomic, strong) DTAPaymentMethodPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTAPaymentMethodPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTAPaymentMethodPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTAPaymentMethodInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTAPaymentMethodRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTAPaymentMethodViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAPaymentMethodModuleInput

#pragma mark - Testing of methods DTAPaymentMethodViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTAPaymentMethodInteractorOutput

@end
