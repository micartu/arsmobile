//
//  DTAPaymentMethodAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodAssembly.h"

@class DTAPaymentMethodViewController;
@class DTAPaymentMethodInteractor;
@class DTAPaymentMethodPresenter;
@class DTAPaymentMethodRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTAPaymentMethodAssembly ()

- (DTAPaymentMethodViewController *)viewPaymentMethod;
- (DTAPaymentMethodPresenter *)presenterPaymentMethod;
- (DTAPaymentMethodInteractor *)interactorPaymentMethod;
- (DTAPaymentMethodRouter *)routerPaymentMethod;

@end
