//
//  DTAPaymentMethodAssemblyTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <RamblerTyphoonUtils/AssemblyTesting.h>
#import <Typhoon/Typhoon.h>

#import "DTAPaymentMethodAssembly.h"
#import "DTAPaymentMethodAssembly_Testable.h"

#import "DTAPaymentMethodViewController.h"
#import "DTAPaymentMethodPresenter.h"
#import "DTAPaymentMethodInteractor.h"
#import "DTAPaymentMethodRouter.h"

@interface DTAPaymentMethodAssemblyTests : RamblerTyphoonAssemblyTests

@property (nonatomic, strong) DTAPaymentMethodAssembly *assembly;

@end

@implementation DTAPaymentMethodAssemblyTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.assembly = [[DTAPaymentMethodAssembly alloc] init];
    [self.assembly activate];
}

- (void)tearDown {
    self.assembly = nil;

    [super tearDown];
}

#pragma mark - Testing of elements creation

- (void)testThatAssemblyCreatesViewController {
    // given
    Class targetClass = [DTAPaymentMethodViewController class];
    NSArray *protocols = @[
                           @protocol(DTAPaymentMethodViewInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly viewPaymentMethod];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesPresenter {
    // given
    Class targetClass = [DTAPaymentMethodPresenter class];
    NSArray *protocols = @[
                           @protocol(DTAPaymentMethodModuleInput),
                           @protocol(DTAPaymentMethodViewOutput),
                           @protocol(DTAPaymentMethodInteractorOutput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(interactor),
                              RamblerSelector(view),
                              RamblerSelector(router)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly presenterPaymentMethod];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesInteractor {
    // given
    Class targetClass = [DTAPaymentMethodInteractor class];
    NSArray *protocols = @[
                           @protocol(DTAPaymentMethodInteractorInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly interactorPaymentMethod];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesRouter {
    // given
    Class targetClass = [DTAPaymentMethodRouter class];
    NSArray *protocols = @[
                           @protocol(DTAPaymentMethodRouterInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(transitionHandler)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly routerPaymentMethod];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

@end
