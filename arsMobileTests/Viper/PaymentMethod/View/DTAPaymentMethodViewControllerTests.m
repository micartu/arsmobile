//
//  DTAPaymentMethodViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentMethodViewController.h"

#import "DTAPaymentMethodViewOutput.h"

@interface DTAPaymentMethodViewControllerTests : XCTestCase

@property (nonatomic, strong) DTAPaymentMethodViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAPaymentMethodViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTAPaymentMethodViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAPaymentMethodViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTAPaymentMethodViewInput

@end
