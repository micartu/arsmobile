//
//  DTAPaymentMethodRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentMethodRouter.h"

@interface DTAPaymentMethodRouterTests : XCTestCase

@property (nonatomic, strong) DTAPaymentMethodRouter *router;

@end

@implementation DTAPaymentMethodRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTAPaymentMethodRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
