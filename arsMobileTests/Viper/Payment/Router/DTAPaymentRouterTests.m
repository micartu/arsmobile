//
//  DTAPaymentRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentRouter.h"

@interface DTAPaymentRouterTests : XCTestCase

@property (nonatomic, strong) DTAPaymentRouter *router;

@end

@implementation DTAPaymentRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTAPaymentRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
