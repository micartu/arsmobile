//
//  DTAPaymentInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentInteractor.h"

#import "DTAPaymentInteractorOutput.h"

@interface DTAPaymentInteractorTests : XCTestCase

@property (nonatomic, strong) DTAPaymentInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAPaymentInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTAPaymentInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAPaymentInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAPaymentInteractorInput

@end
