//
//  DTAPaymentPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAPaymentPresenter.h"

#import "DTAPaymentViewInput.h"
#import "DTAPaymentInteractorInput.h"
#import "DTAPaymentRouterInput.h"

@interface DTAPaymentPresenterTests : XCTestCase

@property (nonatomic, strong) DTAPaymentPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTAPaymentPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTAPaymentPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTAPaymentInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTAPaymentRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTAPaymentViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAPaymentModuleInput

#pragma mark - Testing of methods DTAPaymentViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTAPaymentInteractorOutput

@end
