//
//  DTAPaymentAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentAssembly.h"

@class DTAPaymentViewController;
@class DTAPaymentInteractor;
@class DTAPaymentPresenter;
@class DTAPaymentRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTAPaymentAssembly ()

- (DTAPaymentViewController *)viewPayment;
- (DTAPaymentPresenter *)presenterPayment;
- (DTAPaymentInteractor *)interactorPayment;
- (DTAPaymentRouter *)routerPayment;

@end
