//
//  DTAChangerDialogInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAChangerDialogInteractor.h"

#import "DTAChangerDialogInteractorOutput.h"

@interface DTAChangerDialogInteractorTests : XCTestCase

@property (nonatomic, strong) DTAChangerDialogInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAChangerDialogInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTAChangerDialogInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAChangerDialogInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAChangerDialogInteractorInput

@end
