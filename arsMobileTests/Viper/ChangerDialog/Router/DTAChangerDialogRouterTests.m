//
//  DTAChangerDialogRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAChangerDialogRouter.h"

@interface DTAChangerDialogRouterTests : XCTestCase

@property (nonatomic, strong) DTAChangerDialogRouter *router;

@end

@implementation DTAChangerDialogRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTAChangerDialogRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
