//
//  DTAChangerDialogPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAChangerDialogPresenter.h"

#import "DTAChangerDialogViewInput.h"
#import "DTAChangerDialogInteractorInput.h"
#import "DTAChangerDialogRouterInput.h"

@interface DTAChangerDialogPresenterTests : XCTestCase

@property (nonatomic, strong) DTAChangerDialogPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTAChangerDialogPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTAChangerDialogPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTAChangerDialogInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTAChangerDialogRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTAChangerDialogViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTAChangerDialogModuleInput

#pragma mark - Testing of methods DTAChangerDialogViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTAChangerDialogInteractorOutput

@end
