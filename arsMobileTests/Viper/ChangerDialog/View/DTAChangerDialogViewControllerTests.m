//
//  DTAChangerDialogViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTAChangerDialogViewController.h"

#import "DTAChangerDialogViewOutput.h"

@interface DTAChangerDialogViewControllerTests : XCTestCase

@property (nonatomic, strong) DTAChangerDialogViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTAChangerDialogViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTAChangerDialogViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTAChangerDialogViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTAChangerDialogViewInput

@end
