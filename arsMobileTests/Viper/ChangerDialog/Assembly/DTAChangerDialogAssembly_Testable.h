//
//  DTAChangerDialogAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogAssembly.h"

@class DTAChangerDialogViewController;
@class DTAChangerDialogInteractor;
@class DTAChangerDialogPresenter;
@class DTAChangerDialogRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTAChangerDialogAssembly ()

- (DTAChangerDialogViewController *)viewChangerDialog;
- (DTAChangerDialogPresenter *)presenterChangerDialog;
- (DTAChangerDialogInteractor *)interactorChangerDialog;
- (DTAChangerDialogRouter *)routerChangerDialog;

@end
