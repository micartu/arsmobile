//
//  DTARootPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTARootPresenter.h"

#import "DTARootInteractorInput.h"
#import "DTARootRouterInput.h"

@interface DTARootPresenterTests : XCTestCase

@property (nonatomic, strong) DTARootPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTARootPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTARootPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTARootInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTARootRouterInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTARootModuleInput

#pragma mark - Testing of methods DTARootInteractorOutput

@end
