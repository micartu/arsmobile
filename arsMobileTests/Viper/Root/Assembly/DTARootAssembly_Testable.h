//
//  DTARootAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootAssembly.h"

@class DTARootViewController;
@class DTARootInteractor;
@class DTARootPresenter;
@class DTARootRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTARootAssembly ()

- (DTARootPresenter *)presenterRoot;
- (DTARootInteractor *)interactorRoot;
- (DTARootRouter *)routerRoot;

@end
