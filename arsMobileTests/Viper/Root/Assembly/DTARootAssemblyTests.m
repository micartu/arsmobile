//
//  DTARootAssemblyTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <RamblerTyphoonUtils/AssemblyTesting.h>
#import <Typhoon/Typhoon.h>

#import "DTARootAssembly.h"
#import "DTARootAssembly_Testable.h"

#import "DTARootPresenter.h"
#import "DTARootInteractor.h"
#import "DTARootRouter.h"

@interface DTARootAssemblyTests : RamblerTyphoonAssemblyTests

@property (nonatomic, strong) DTARootAssembly *assembly;

@end

@implementation DTARootAssemblyTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.assembly = [[DTARootAssembly alloc] init];
    [self.assembly activate];
}

- (void)tearDown {
    self.assembly = nil;

    [super tearDown];
}

#pragma mark - Testing of elements creation

- (void)testThatAssemblyCreatesPresenter {
    // given
    Class targetClass = [DTARootPresenter class];
    NSArray *protocols = @[
                           @protocol(DTARootModuleInput),
                           @protocol(DTARootInteractorOutput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(interactor),
                              RamblerSelector(router)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly presenterRoot];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesInteractor {
    // given
    Class targetClass = [DTARootInteractor class];
    NSArray *protocols = @[
                           @protocol(DTARootInteractorInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly interactorRoot];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

@end
