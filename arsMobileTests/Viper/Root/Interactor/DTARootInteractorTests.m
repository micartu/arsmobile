//
//  DTARootInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTARootInteractor.h"

#import "DTARootInteractorOutput.h"

@interface DTARootInteractorTests : XCTestCase

@property (nonatomic, strong) DTARootInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTARootInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTARootInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTARootInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTARootInteractorInput

@end
