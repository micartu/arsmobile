//
//  DTASettingsPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTASettingsPresenter.h"

#import "DTASettingsViewInput.h"
#import "DTASettingsInteractorInput.h"
#import "DTASettingsRouterInput.h"

@interface DTASettingsPresenterTests : XCTestCase

@property (nonatomic, strong) DTASettingsPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTASettingsPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTASettingsPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTASettingsInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTASettingsRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTASettingsViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTASettingsModuleInput

#pragma mark - Testing of methods DTASettingsViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTASettingsInteractorOutput

@end
