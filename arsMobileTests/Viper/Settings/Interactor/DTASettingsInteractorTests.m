//
//  DTASettingsInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTASettingsInteractor.h"

#import "DTASettingsInteractorOutput.h"

@interface DTASettingsInteractorTests : XCTestCase

@property (nonatomic, strong) DTASettingsInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTASettingsInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTASettingsInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTASettingsInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTASettingsInteractorInput

@end
