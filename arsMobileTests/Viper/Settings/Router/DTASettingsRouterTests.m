//
//  DTASettingsRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTASettingsRouter.h"

@interface DTASettingsRouterTests : XCTestCase

@property (nonatomic, strong) DTASettingsRouter *router;

@end

@implementation DTASettingsRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTASettingsRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
