//
//  DTASettingsViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTASettingsViewController.h"

#import "DTASettingsViewOutput.h"

@interface DTASettingsViewControllerTests : XCTestCase

@property (nonatomic, strong) DTASettingsViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTASettingsViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTASettingsViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTASettingsViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTASettingsViewInput

@end
