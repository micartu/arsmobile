//
//  DTASettingsAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsAssembly.h"

@class DTASettingsViewController;
@class DTASettingsInteractor;
@class DTASettingsPresenter;
@class DTASettingsRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTASettingsAssembly ()

- (DTASettingsViewController *)viewSettings;
- (DTASettingsPresenter *)presenterSettings;
- (DTASettingsInteractor *)interactorSettings;
- (DTASettingsRouter *)routerSettings;

@end
