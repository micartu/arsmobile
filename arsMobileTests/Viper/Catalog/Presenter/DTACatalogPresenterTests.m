//
//  DTACatalogPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACatalogPresenter.h"

#import "DTACatalogViewInput.h"
#import "DTACatalogInteractorInput.h"
#import "DTACatalogRouterInput.h"

@interface DTACatalogPresenterTests : XCTestCase

@property (nonatomic, strong) DTACatalogPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTACatalogPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTACatalogPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTACatalogInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTACatalogRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTACatalogViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTACatalogModuleInput

#pragma mark - Testing of methods DTACatalogViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTACatalogInteractorOutput

@end
