//
//  DTACatalogRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACatalogRouter.h"

@interface DTACatalogRouterTests : XCTestCase

@property (nonatomic, strong) DTACatalogRouter *router;

@end

@implementation DTACatalogRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTACatalogRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
