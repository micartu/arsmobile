//
//  DTACatalogInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACatalogInteractor.h"

#import "DTACatalogInteractorOutput.h"

@interface DTACatalogInteractorTests : XCTestCase

@property (nonatomic, strong) DTACatalogInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTACatalogInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTACatalogInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTACatalogInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTACatalogInteractorInput

@end
