//
//  DTACatalogAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogAssembly.h"

@class DTACatalogViewController;
@class DTACatalogInteractor;
@class DTACatalogPresenter;
@class DTACatalogRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTACatalogAssembly ()

- (DTACatalogViewController *)viewCatalog;
- (DTACatalogPresenter *)presenterCatalog;
- (DTACatalogInteractor *)interactorCatalog;
- (DTACatalogRouter *)routerCatalog;

@end
