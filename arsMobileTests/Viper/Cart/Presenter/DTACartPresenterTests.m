//
//  DTACartPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACartPresenter.h"

#import "DTACartViewInput.h"
#import "DTACartInteractorInput.h"
#import "DTACartRouterInput.h"

@interface DTACartPresenterTests : XCTestCase

@property (nonatomic, strong) DTACartPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTACartPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTACartPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTACartInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTACartRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTACartViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTACartModuleInput

#pragma mark - Testing of methods DTACartViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTACartInteractorOutput

@end
