//
//  DTACartViewControllerTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACartViewController.h"

#import "DTACartViewOutput.h"

@interface DTACartViewControllerTests : XCTestCase

@property (nonatomic, strong) DTACartViewController *controller;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTACartViewControllerTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.controller = [[DTACartViewController alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTACartViewOutput));

    self.controller.output = self.mockOutput;
}

- (void)tearDown {
    self.controller = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing lifecycle events

- (void)testThatControllerNotifiesPresenterOnDidLoad {
	// given

	// when
	[self.controller viewDidLoad];

	// then
	OCMVerify([self.mockOutput didTriggerViewReadyEvent]);
}

#pragma mark - Testing of interface methods

#pragma mark - Testing of methods DTACartViewInput

@end
