//
//  DTACartInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACartInteractor.h"

#import "DTACartInteractorOutput.h"

@interface DTACartInteractorTests : XCTestCase

@property (nonatomic, strong) DTACartInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTACartInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTACartInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTACartInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTACartInteractorInput

@end
