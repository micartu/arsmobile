//
//  DTACartAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartAssembly.h"

@class DTACartViewController;
@class DTACartInteractor;
@class DTACartPresenter;
@class DTACartRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTACartAssembly ()

- (DTACartViewController *)viewCart;
- (DTACartPresenter *)presenterCart;
- (DTACartInteractor *)interactorCart;
- (DTACartRouter *)routerCart;

@end
