//
//  DTACatBrowserRouterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACatBrowserRouter.h"

@interface DTACatBrowserRouterTests : XCTestCase

@property (nonatomic, strong) DTACatBrowserRouter *router;

@end

@implementation DTACatBrowserRouterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.router = [[DTACatBrowserRouter alloc] init];
}

- (void)tearDown {
    self.router = nil;

    [super tearDown];
}

@end
