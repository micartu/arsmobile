//
//  DTACatBrowserInteractorTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACatBrowserInteractor.h"

#import "DTACatBrowserInteractorOutput.h"

@interface DTACatBrowserInteractorTests : XCTestCase

@property (nonatomic, strong) DTACatBrowserInteractor *interactor;

@property (nonatomic, strong) id mockOutput;

@end

@implementation DTACatBrowserInteractorTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.interactor = [[DTACatBrowserInteractor alloc] init];

    self.mockOutput = OCMProtocolMock(@protocol(DTACatBrowserInteractorOutput));

    self.interactor.output = self.mockOutput;
}

- (void)tearDown {
    self.interactor = nil;

    self.mockOutput = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTACatBrowserInteractorInput

@end
