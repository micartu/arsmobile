//
//  DTACatBrowserPresenterTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DTACatBrowserPresenter.h"

#import "DTACatBrowserViewInput.h"
#import "DTACatBrowserInteractorInput.h"
#import "DTACatBrowserRouterInput.h"

@interface DTACatBrowserPresenterTests : XCTestCase

@property (nonatomic, strong) DTACatBrowserPresenter *presenter;

@property (nonatomic, strong) id mockInteractor;
@property (nonatomic, strong) id mockRouter;
@property (nonatomic, strong) id mockView;

@end

@implementation DTACatBrowserPresenterTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.presenter = [[DTACatBrowserPresenter alloc] init];

    self.mockInteractor = OCMProtocolMock(@protocol(DTACatBrowserInteractorInput));
    self.mockRouter = OCMProtocolMock(@protocol(DTACatBrowserRouterInput));
    self.mockView = OCMProtocolMock(@protocol(DTACatBrowserViewInput));

    self.presenter.interactor = self.mockInteractor;
    self.presenter.router = self.mockRouter;
    self.presenter.view = self.mockView;
}

- (void)tearDown {
    self.presenter = nil;

    self.mockView = nil;
    self.mockRouter = nil;
    self.mockInteractor = nil;

    [super tearDown];
}

#pragma mark - Testing of methods DTACatBrowserModuleInput

#pragma mark - Testing of methods DTACatBrowserViewOutput

- (void)testThatPresenterHandlesViewReadyEvent {
    // given


    // when
    [self.presenter didTriggerViewReadyEvent];

    // then
    OCMVerify([self.mockView setupInitialState]);
}

#pragma mark - Testing of methods DTACatBrowserInteractorOutput

@end
