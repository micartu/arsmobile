//
//  DTACatBrowserAssembly_Testable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserAssembly.h"

@class DTACatBrowserViewController;
@class DTACatBrowserInteractor;
@class DTACatBrowserPresenter;
@class DTACatBrowserRouter;

/**
 @author Michael Artuerhof

 Extension, which makes private methods public
 */
@interface DTACatBrowserAssembly ()

- (DTACatBrowserViewController *)viewCatBrowser;
- (DTACatBrowserPresenter *)presenterCatBrowser;
- (DTACatBrowserInteractor *)interactorCatBrowser;
- (DTACatBrowserRouter *)routerCatBrowser;

@end
