//
//  DTACatBrowserAssemblyTests.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <RamblerTyphoonUtils/AssemblyTesting.h>
#import <Typhoon/Typhoon.h>

#import "DTACatBrowserAssembly.h"
#import "DTACatBrowserAssembly_Testable.h"

#import "DTACatBrowserViewController.h"
#import "DTACatBrowserPresenter.h"
#import "DTACatBrowserInteractor.h"
#import "DTACatBrowserRouter.h"

@interface DTACatBrowserAssemblyTests : RamblerTyphoonAssemblyTests

@property (nonatomic, strong) DTACatBrowserAssembly *assembly;

@end

@implementation DTACatBrowserAssemblyTests

#pragma mark - Setup of the environment for testing

- (void)setUp {
    [super setUp];

    self.assembly = [[DTACatBrowserAssembly alloc] init];
    [self.assembly activate];
}

- (void)tearDown {
    self.assembly = nil;

    [super tearDown];
}

#pragma mark - Testing of elements creation

- (void)testThatAssemblyCreatesViewController {
    // given
    Class targetClass = [DTACatBrowserViewController class];
    NSArray *protocols = @[
                           @protocol(DTACatBrowserViewInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly viewCatBrowser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesPresenter {
    // given
    Class targetClass = [DTACatBrowserPresenter class];
    NSArray *protocols = @[
                           @protocol(DTACatBrowserModuleInput),
                           @protocol(DTACatBrowserViewOutput),
                           @protocol(DTACatBrowserInteractorOutput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(interactor),
                              RamblerSelector(view),
                              RamblerSelector(router)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly presenterCatBrowser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesInteractor {
    // given
    Class targetClass = [DTACatBrowserInteractor class];
    NSArray *protocols = @[
                           @protocol(DTACatBrowserInteractorInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(output)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly interactorCatBrowser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

- (void)testThatAssemblyCreatesRouter {
    // given
    Class targetClass = [DTACatBrowserRouter class];
    NSArray *protocols = @[
                           @protocol(DTACatBrowserRouterInput)
                           ];
    NSArray *dependencies = @[
                              RamblerSelector(transitionHandler)
                              ];
    RamblerTyphoonAssemblyTestsTypeDescriptor *descriptor = [RamblerTyphoonAssemblyTestsTypeDescriptor descriptorWithClass:targetClass
                                                                                                              andProtocols:protocols];

    // when
    id result = [self.assembly routerCatBrowser];

    // then
    [self verifyTargetDependency:result
                  withDescriptor:descriptor
                    dependencies:dependencies];
}

@end
