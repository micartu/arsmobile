//
//  DTAMVCAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@class DTAThemeAssembly;
@class DTAMenuTranslator;
@class DTARootAssembly;
@class DTALeftMenuController;
@protocol DTALeftMenuInputProtocol;

@interface DTAMVCAssembly : TyphoonAssembly <RamblerInitialAssembly>

@property(nonatomic, strong, readonly) DTAThemeAssembly *themeProvider;
@property(nonatomic, strong, readonly) DTARootAssembly *rootAssembly;

- (DTALeftMenuController *)mainMenuView;
- (id<DTALeftMenuInputProtocol>)menuTranslator;

@end
