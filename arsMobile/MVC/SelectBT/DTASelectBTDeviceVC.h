//
//  DTASelectBTDeviceVC.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import "DTABaseViewController.h"

@protocol DTASelectBTDeviceVCProtocol <NSObject>
- (void)anotherAccessoryChosen:(EAAccessory*)accessory;
@end

@interface DTASelectBTDeviceVC : DTABaseViewController
@property (weak, nonatomic) id<DTASelectBTDeviceVCProtocol> delegate;
@end
