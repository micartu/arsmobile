//
//  DTASelectBTDeviceVC.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASelectBTDeviceVC.h"
#import "constants.h"

@interface DTASelectBTDeviceVC ()

@property (nonatomic, strong) NSArray *accessoryList;
@property (weak, nonatomic) IBOutlet UIView *noElementsView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DTASelectBTDeviceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.noElementsView.layer.cornerRadius = 8;
    self.title = @"Выбор принтера";
    _accessoryList = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
    if (_accessoryList.count < 1)
        self.noElementsView.hidden = NO;
    else
        self.noElementsView.hidden = YES;
}

#pragma mark UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_accessoryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"deviceCell";
    NSUInteger row = [indexPath row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *eaAccessoryName = [[_accessoryList objectAtIndex:row] name];
    if (!eaAccessoryName || !eaAccessoryName.length)
        eaAccessoryName = @"unknown";
    [cell.textLabel setText:eaAccessoryName];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    if (row >= _accessoryList.count)
        return;
    if (self.delegate)
        [self.delegate anotherAccessoryChosen:[_accessoryList objectAtIndex:row]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *name = [[_accessoryList objectAtIndex:row] name];
    [defaults setObject:name forKey:kDefaultsKeySelectedPrinterName];
    [defaults synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
