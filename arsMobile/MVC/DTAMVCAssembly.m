//
//  DTAMVCAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAMVCAssembly.h"
#import "DTALeftMenuController.h"
#import "DTASelectBTDeviceVC.h"
#import "DTAThemeAssembly.h"
#import "DTARootAssembly.h"
#import "DTAMenuTranslator.h"
#import "DTAAboutViewController.h"

@implementation DTAMVCAssembly

- (DTALeftMenuController *)mainMenuView {
    return [TyphoonDefinition withClass:[DTALeftMenuController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(theme) with:[_themeProvider currentTheme]];
        //[definition injectProperty:@selector(delegate) with:[self menuTranslator]]; // doesn't work!
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (DTAAboutViewController *)aboutView {
    return [TyphoonDefinition withClass:[DTAAboutViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(theme) with:[_themeProvider currentTheme]];
        [definition injectProperty:@selector(root) with:[_rootAssembly presenterRoot]];
    }];
}

- (id<DTALeftMenuInputProtocol>)menuTranslator {
    return [TyphoonDefinition withClass:[DTAMenuTranslator class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(view) with:[self mainMenuView]];
        [definition injectProperty:@selector(output) with:[_rootAssembly presenterRoot]];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (DTASelectBTDeviceVC *)selectDeviceView {
    return [TyphoonDefinition withClass:[DTASelectBTDeviceVC class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(theme) with:[_themeProvider currentTheme]];
    }];
}

@end
