//
//  DTAAboutViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAAboutViewController.h"

@interface DTAAboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@end

@implementation DTAAboutViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
    self.rootMenu = YES;
	[super viewDidLoad];
    [super applyTheme];
    [super setupInitialState];
    
    self.title = @"О программе";
    self.versionLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

@end
