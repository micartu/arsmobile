//
//  DTAMenuTranslator.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTALeftMenuProtocol.h"
#import "DTALeftMenuInputProtocol.h"

@protocol DTALeftViewInputProtocol;
@protocol DTALeftMenuOutputProtocol;

@interface DTAMenuTranslator : NSObject<DTALeftMenuProtocol, DTALeftMenuInputProtocol>

@property (strong, nonatomic) id<DTALeftViewInputProtocol> view;
@property (strong, nonatomic) id<DTALeftMenuOutputProtocol> output;

@end
