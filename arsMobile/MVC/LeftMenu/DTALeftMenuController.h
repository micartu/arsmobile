//
//  DTALeftMenuController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTAThemeable.h"
#import "DTALeftMenuProtocol.h"
#import "DTALeftViewInputProtocol.h"

@interface DTALeftMenuController : UIViewController <DTALeftViewInputProtocol, DTAThemeable>

@property (strong, nonatomic) id<DTALeftMenuProtocol> delegate;

@end
