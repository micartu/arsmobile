//
//  DTAMenuTranslator.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAMenuTranslator.h"
#import "DTALeftViewInputProtocol.h"
#import "DTALeftMenuOutputProtocol.h"

@implementation DTAMenuTranslator

- (void)markPositionSelected:(int)position {
    [self.view markPositionSelected:position];
}

- (void)changeOperatorName:(NSString*)name {
    [self.view changeOperatorName:name];
}

- (void)cartMenuChosen {
    assert(self.output);
    [self.output cartMenuSelected];
}

- (void)catalogMenuChosen {
    [self.output catalogMenuSelected];
}

- (void)settingsMenuChosen {
    [self.output settingsMenuSelected];
}

- (void)aboutMenuChosen {
    [self.output aboutMenuSelected];
}

@end
