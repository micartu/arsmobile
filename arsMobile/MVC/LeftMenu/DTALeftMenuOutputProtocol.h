//
//  DTALeftMenuOutputProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@protocol DTALeftMenuOutputProtocol

- (void)cartMenuSelected;
- (void)catalogMenuSelected;
- (void)settingsMenuSelected;
- (void)aboutMenuSelected;

@end

