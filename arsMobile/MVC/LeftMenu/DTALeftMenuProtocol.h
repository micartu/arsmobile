//
//  DTALeftMenuProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@protocol DTALeftMenuProtocol

- (void)cartMenuChosen;
- (void)catalogMenuChosen;
- (void)settingsMenuChosen;
- (void)aboutMenuChosen;

@end
