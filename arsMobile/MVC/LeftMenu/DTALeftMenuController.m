//
//  DTALeftMenuController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTALeftMenuController.h"
#import "constants.h"

static NSString *const kRowCell = @"menuCell";

typedef NS_ENUM(NSInteger, MenuType) {
    MenuTypeCart = 0,
    MenuTypeCatalog,
    MenuTypeSettings,
    MenuTypeAbout
};

@interface DTALeftMenuController () <UITableViewDelegate, UITableViewDataSource> {
    int _selected;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *opName;

@end

@implementation DTALeftMenuController

@synthesize theme;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self markPositionSelected:_selected];
}

- (void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *name = [defaults objectForKey:kDefaultsKeyOperatorName];
    if (name.length)
        [self changeOperatorName:name];
}

- (void)dealloc {
    NSLog(@"dealloc of DTALeftMenuController");
}

#pragma mark - DTAThemeable

- (void)applyTheme {
}

#pragma mark - Table View source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case MenuTypeCart:
            cell.textLabel.text = @"Корзина";
            break;
            
        case MenuTypeCatalog:
            cell.textLabel.text = @"Каталог";
            break;
            
        case MenuTypeSettings:
            cell.textLabel.text = @"Настройки ФН";
            break;
            
        case MenuTypeAbout:
            cell.textLabel.text = @"О программе";
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRowCell];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case MenuTypeCart:
            [self.delegate cartMenuChosen];
            break;
            
        case MenuTypeCatalog:
            [self.delegate catalogMenuChosen];
            break;
            
        case MenuTypeSettings:
            [self.delegate settingsMenuChosen];
            break;
            
        case MenuTypeAbout:
            [self.delegate aboutMenuChosen];
            break;
    }
}

#pragma mark - DTALeftViewInputProtocol

- (void)markPositionSelected:(int)position {
    NSIndexPath *path = [NSIndexPath indexPathForRow:position inSection:0];
    if (self.tableView)
        [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:NO];
    _selected = position;
}

- (void)changeOperatorName:(NSString*)name {
    self.opName.text = name;
}

@end
