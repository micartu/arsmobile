//
//  DTALeftMenuInputProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@protocol DTALeftMenuInputProtocol

- (void)markPositionSelected:(int)position;
- (void)changeOperatorName:(NSString*)name;

@end
