//
//  DTACartAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

/**
 @author Michael Artuerhof

 Cart module
 */
@interface DTACartAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@end
