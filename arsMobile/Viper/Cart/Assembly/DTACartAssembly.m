//
//  DTACartAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartAssembly.h"

#import "DTAServiceAssembly.h"
#import "DTACartViewController.h"
#import "DTACartInteractor.h"
#import "DTACartPresenter.h"
#import "DTACartRouter.h"
#import "DTARootAssembly.h"
#import "DTAAlertAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTACartAssembly

- (DTACartViewController *)viewCart {
    return [TyphoonDefinition withClass:[DTACartViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCart]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCart]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(root)
                                                    with:[self.rootAssembly presenterRoot]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTACartInteractor *)interactorCart {
    return [TyphoonDefinition withClass:[DTACartInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCart]];
                              [definition injectProperty:@selector(storage)
                                                    with:[self.servicesAssembly dataStorageService]];
                          }];
}

- (DTACartPresenter *)presenterCart{
    return [TyphoonDefinition withClass:[DTACartPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCart]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCart]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCart]];
                          }];
}

- (DTACartRouter *)routerCart{
    return [TyphoonDefinition withClass:[DTACartRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCart]];
                              [definition injectProperty:@selector(root)
                                                    with:[self.rootAssembly presenterRoot]];
                          }];
}

@end
