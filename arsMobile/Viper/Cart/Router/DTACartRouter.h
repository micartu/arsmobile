//
//  DTACartRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartRouterInput.h"
#import "DTACommonRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTACartRouter : DTACommonRouter <DTACartRouterInput>

@end
