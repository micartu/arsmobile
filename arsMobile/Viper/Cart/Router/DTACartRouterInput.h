//
//  DTACartRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTACartRouterInput <DTACommonRouterInput>

- (void)openOrder;

@end
