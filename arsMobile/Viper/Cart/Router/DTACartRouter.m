//
//  DTACartRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "storyboard_constants.h"
#import "DTAOrderModuleInput.h"

@implementation DTACartRouter

#pragma mark - Methods of DTACartRouterInput

- (void)openOrder {
    [[self.transitionHandler openModuleUsingSegue:kSegueOrder] thenChainUsingBlock:
     ^id<RamblerViperModuleOutput>(id<DTAOrderModuleInput> moduleInput) {
         [moduleInput configureModule];
         return nil;
     }];
}

@end
