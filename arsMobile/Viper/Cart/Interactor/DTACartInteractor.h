//
//  DTACartInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartInteractorInput.h"

@protocol DTACartInteractorOutput;
@protocol DTAStorageProtocol;

@interface DTACartInteractor : NSObject <DTACartInteractorInput>

@property (nonatomic, weak) id<DTACartInteractorOutput> output;
@property (nonatomic, weak) id<DTAStorageProtocol> storage;

@end
