//
//  DTACartInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTACartItem;

@protocol DTACartInteractorInput <NSObject>

- (void)fetchCartItemsWithCompletion:(void(^)(NSArray<DTACartItem*> *items))complete;
- (void)removeItemFromCart:(DTACartItem*)item;
- (void)clearCartWithCompletion:(void (^)(void))completion;
- (void)updateCartItem:(DTACartItem*)item withCompletion:(void (^)(void))completion;

@end
