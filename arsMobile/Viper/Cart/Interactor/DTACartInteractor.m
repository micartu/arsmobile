//
//  DTACartInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartInteractor.h"

#import "DTACartInteractorOutput.h"
#import "DTAStorageProtocol.h"
#import "DTACartItem.h"

@implementation DTACartInteractor

#pragma mark - Methods of DTACartInteractorInput

- (void)fetchCartItemsWithCompletion:(void(^)(NSArray<DTACartItem*> *items))complete {
    [self.storage fetchAllCartItemsWithCompletion:complete];
}

- (void)removeItemFromCart:(DTACartItem*)item {
    [self.storage deleteCartItem:item andCompletion:nil];
}

- (void)clearCartWithCompletion:(void (^)(void))completion {
    [self.storage deleteAllCartItemsWithCompletion:completion];
}

- (void)updateCartItem:(DTACartItem*)item withCompletion:(void (^)(void))completion {
    [self.storage modifyCartItem:item andCompletion:completion];
}

@end
