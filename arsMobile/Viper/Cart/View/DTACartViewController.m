//
//  DTACartViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartViewController.h"

#import "DTACartViewOutput.h"
#import "DTACartCell.h"
#import "DTACartItem.h"
#import "DTACategory.h"
#import "constants.h"
#import "DTAItem.h"

static NSString *const kRowCell = @"cartCell";
static const float kRowHeigth = 87;
static const float kCartBottomViewHeigth = 140;

@interface DTACartViewController() <UITableViewDataSource, UITableViewDelegate> {
    NSArray *_records;
}
@property (weak, nonatomic) IBOutlet UIView *emptyCartView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayoutConstraint;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *clearCartButton;

@property (strong, nonatomic) NSNumberFormatter *priceFormatter;
@property (strong, nonatomic) NSNumberFormatter *countFormatter;
@property (strong, nonatomic) NSMutableArray<DTACartItem*> *positions;
@end

@implementation DTACartViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
    self.rootMenu = YES;
	[super viewDidLoad];

    self.title = @"Корзина";

    self.positions = [[NSMutableArray alloc] init];
    self.priceFormatter = [[NSNumberFormatter alloc] init];
    self.priceFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    self.priceFormatter.currencySymbol = @"₽";
    self.priceFormatter.maximumFractionDigits = 2;
    self.priceFormatter.minimumFractionDigits = 0;

    self.countFormatter = [[NSNumberFormatter alloc] init];
    self.countFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.countFormatter.currencySymbol = @"";
    self.countFormatter.maximumFractionDigits = 2;
    self.countFormatter.minimumFractionDigits = 0;

    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = kLongPressTimeSec;
    [self.tableView addGestureRecognizer:lpgr];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTACartViewInput

- (void)setupInitialState {
	// initial setup of the view
    [super setupInitialState];
    [self applyTheme];
}

- (void)setItems:(NSArray<DTACartItem*>*)items {
    runOnMainThread(^{
        _records = items;
        if (items.count) {
            [self hideEmptyCartPlaceholderAnimated:NO];
            [self.tableView reloadData];
        }
        else
            [self showEmptyCartPlaceholderAnimated:YES];
    });
}

- (void)enableContinueButton {
    runOnMainThread(^{
        self.continueButton.enabled = YES;
    });
}

- (void)disableContinueButton {
    runOnMainThread(^{
        self.continueButton.enabled = NO;
    });
}

- (void)enableClearCartButton {
    runOnMainThread(^{
        self.clearCartButton.enabled = YES;
    });
}

- (void)disableClearCartButton {
    runOnMainThread(^{
        self.clearCartButton.enabled = NO;
    });
}

- (void)removeAllOrderPositions {
    runOnMainThread(^{
        _records = nil;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        [self showEmptyCartPlaceholder];
    });
}

- (void)showEmptyCartPlaceholder {
    // need a little delay for a smooth animation
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showEmptyCartPlaceholderAnimated:YES];
    });
}

- (void)updateTotalLabelWithSum:(double)sum {
    self.totalLabel.text = [self.priceFormatter stringFromNumber:@(sum)];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DTACartCell *cell = [tableView dequeueReusableCellWithIdentifier:kRowCell forIndexPath:indexPath];
    DTACartItem *orderPosition = _records[indexPath.row];
    DTAItem *item = orderPosition.item;

    cell.title.text = item.title;
    cell.category.text = item.category.title;
    cell.price.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithFloat:item.price]];
    cell.totalPrice.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithFloat:item.price * orderPosition.count]];
    NSString *measureUnit = [NSString stringWithFormat:@"%@", item.measureUnit.length > 0 ?
                             [NSString stringWithFormat:@" %@", item.measureUnit] :
                             @"x"];
    cell.count.text = [NSString stringWithFormat:@"%@%@", [self stringFromCount:orderPosition.count], measureUnit];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRowHeigth;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.tableView setEditing:NO animated:YES];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.output removeItemAtIndex:indexPath.row];
        });
    }
}

#pragma mark - Private

- (void)showEmptyCartPlaceholderAnimated:(BOOL)animated {
    if (animated) {
        self.bottomLayoutConstraint.constant = -kCartBottomViewHeigth;
        [UIView animateWithDuration:0.4 animations:^{
            self.tableView.alpha = 0.0;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                self.emptyCartView.alpha = 1.0;
            }];
        }];
    }
    else {
        self.emptyCartView.alpha = 1.0;
        self.tableView.alpha = 0.0;
        self.bottomLayoutConstraint.constant = -kCartBottomViewHeigth;
        [self.view layoutIfNeeded];
    }
}

- (void)hideEmptyCartPlaceholderAnimated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:0.2 animations:^{
            self.emptyCartView.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.bottomLayoutConstraint.constant = 0.0;
            [UIView animateWithDuration:0.4 animations:^{
                self.tableView.alpha = 1.0;
                [self.view layoutIfNeeded];
            }];
        }];
    }
    else {
        self.emptyCartView.alpha = 0.0;
        self.tableView.alpha = 1.0;
        self.bottomLayoutConstraint.constant = 0.0;
        [self.view layoutIfNeeded];
    }
}

- (NSString*)stringFromCount:(float)count {
    return [self.countFormatter stringFromNumber:[NSNumber numberWithFloat:count]];
}

#pragma mark - Actions

- (IBAction)unwindIntoCart:(UIStoryboardSegue *)segue {
    [self.output didTriggerViewReadyEvent];
}

- (IBAction)addToCartButtonPressed:(id)sender {
    [self.output addToCartButtonPressed];
}

- (IBAction)continueButtonPressed:(id)sender {
    [self.output continueButtonPressed];
}

- (IBAction)clearCartButtonPressed:(id)sender {
    [self.output clearCartButtonPressed];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath && gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self.output longPressOnCell:indexPath.row];
    }
}

@end
