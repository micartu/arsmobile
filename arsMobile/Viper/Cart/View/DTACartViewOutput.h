//
//  DTACartViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTACartItem;

@protocol DTACartViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)removeItemAtIndex:(NSInteger)index;
- (void)clearCartButtonPressed;
- (void)continueButtonPressed;
- (void)addToCartButtonPressed;
- (void)longPressOnCell:(NSInteger)index;

@end
