//
//  DTACartViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@class DTACartItem;

@protocol DTACartViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;

- (void)enableContinueButton;
- (void)disableContinueButton;
- (void)enableClearCartButton;
- (void)disableClearCartButton;

- (void)setItems:(NSArray<DTACartItem*>*)items;
- (void)updateTotalLabelWithSum:(double)sum;
- (void)removeAllOrderPositions;
- (void)showEmptyCartPlaceholder;

@end
