//
//  DTACartViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTACartViewInput.h"
#import "DTABaseViewController.h"

@protocol DTACartViewOutput;

@interface DTACartViewController : DTABaseViewController <DTACartViewInput>

@property (nonatomic, strong) id<DTACartViewOutput> output;

@end
