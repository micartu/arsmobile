//
//  DTACartPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartPresenter.h"

#import "DTACartViewInput.h"
#import "DTACartInteractorInput.h"
#import "DTACartRouterInput.h"
#import "DTACartItem.h"
#import "DTAItem.h"

@implementation DTACartPresenter {
    NSMutableArray<DTACartItem*> *_items;
}

#pragma mark - Methods of DTACartModuleInput

- (void)configureModule {
    // starting configuration of the module
}

#pragma mark - Methods of DTACartViewOutput

- (void)didTriggerViewReadyEvent {
    __weak typeof (self) wself = self;
	[self.view setupInitialState];
    [wself.interactor fetchCartItemsWithCompletion:^(NSArray<DTACartItem*>* items) {
        _items = [NSMutableArray arrayWithArray:items];
        if (items.count)
            [wself.view setItems:_items];
        else
            [wself.view showEmptyCartPlaceholder];
        [wself updateTotal];
    }];
}

- (void)removeItemAtIndex:(NSInteger)index {
    assert(index < _items.count);
    [self.interactor removeItemFromCart:_items[index]];
    [_items removeObjectAtIndex:index];
    [self.view setItems:_items];
    [self updateTotal];
}

- (void)clearCartButtonPressed {
    __weak typeof (self) wself = self;
    [wself.view showYesNoDialogWithMessage:@"Вы уверены, что хотите очистить корзину?"
                              andYesAction:^(UIAlertAction *a) {
                                  [wself.interactor clearCartWithCompletion:^{
                                      [wself.view removeAllOrderPositions];
                                  }];
                              } andNoAction:nil];
}

- (void)continueButtonPressed {
    [self.router openOrder];
}

- (void)addToCartButtonPressed {
    [self.router changeToMenu:CatalogSelected];
}

- (void)longPressOnCell:(NSInteger)index {
    __weak typeof (self) wself = self;
    NSArray *titles = @[@"Изменить количество", @"Удалить товар", @"Отмена"];
    NSArray *actions = @[^(UIAlertAction *action) {
        assert (index < _items.count);
        [wself.view inputDialogWithTitle:@"Внимание"
                             withMessage:@"Введите количество"
                             andOkAction:^(UIAlertAction *action, UIAlertController *alertController) {
                                 NSArray *textfields = alertController.textFields;
                                 UITextField *ifield = textfields[0];
                                 NSString *dnum = ifield.text;
                                 NSScanner *scanner = [NSScanner scannerWithString:dnum];
                                 double found_double;
                                 if ([scanner scanDouble:&found_double]) {
                                     DTACartItem *ci = _items[index];
                                     ci.count = found_double;
                                     [wself.interactor updateCartItem:ci withCompletion:^{
                                         [wself.view setItems:_items];
                                         [wself updateTotal];
                                     }];
                                     return;
                                 }
                                 [wself.view showErrorWithMessage:@"Неправильный ввод!"];
                             }];
    },
    ^(UIAlertAction *action) {
        assert (index < _items.count);
        [wself removeItemAtIndex:index];
    },
    ^(UIAlertAction *action) {
        // do nothing for cancel
    }];
    NSArray *styles = @[[NSNumber numberWithInteger:UIAlertActionStyleDefault],
                        [NSNumber numberWithInteger:UIAlertActionStyleDestructive],
                        [NSNumber numberWithInteger:UIAlertActionStyleCancel]];
    [self.view chooseDialogWithTitle:@"Выберите действие"
                         withMessage:@"Что нужно сделать с элементом"
                    withActionTitles:titles
                          andActions:actions
                           andStyles:styles];
}

#pragma mark - Methods of DTACartInteractorOutput

#pragma mark - Private

- (void)updateTotal {
    double sum = 0;
    for (DTACartItem *i in _items)
        sum += i.item.price * i.count;
    [self.view updateTotalLabelWithSum:sum];
}

@end
