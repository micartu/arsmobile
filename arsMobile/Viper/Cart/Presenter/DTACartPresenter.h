//
//  DTACartPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartViewOutput.h"
#import "DTACartInteractorOutput.h"
#import "DTACartModuleInput.h"

@protocol DTACartViewInput;
@protocol DTACartInteractorInput;
@protocol DTACartRouterInput;

@interface DTACartPresenter : NSObject <DTACartModuleInput, DTACartViewOutput, DTACartInteractorOutput>

@property (nonatomic, weak) id<DTACartViewInput> view;
@property (nonatomic, strong) id<DTACartInteractorInput> interactor;
@property (nonatomic, strong) id<DTACartRouterInput> router;

@end
