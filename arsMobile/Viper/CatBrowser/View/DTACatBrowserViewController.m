//
//  DTACatBrowserViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserViewController.h"

#import "DTACatBrowserViewOutput.h"
#import "DTACatBrowserCell.h"
#import "DTACornedButton.h"
#import "DTAItem.h"
#import "DTATheme.h"
#import "DTACartButton.h"
#import "CMPopTipView.h"
#import "constants.h"

static NSString *const kRowCell = @"catItemCell";
static CGFloat kItemRowHeight = 44;

@interface DTACatBrowserViewController () <UITableViewDataSource, UITableViewDelegate, DTACatBrowserCellProtocol> {
    NSArray<DTAItem*> *_records;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewNoItems;
@property (weak, nonatomic) IBOutlet DTACornedButton *btnCreateItem;

@end

@implementation DTACatBrowserViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];
    [self applyTheme];

    [self showHideWarnMessage];

    __weak typeof(self) wself = self;
    [wself addCartButtonWithAction:^{
        [wself.output moveToCart];
    }];

    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = kLongPressTimeSec;
    [self.tableView addGestureRecognizer:lpgr];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTACatBrowserViewInput

- (void)setupInitialState {
    [self.cartButton setBadgeCount:0];
}

- (void)setWindowTitle:(NSString*)title {
    self.title = title;
}

- (void)setItems:(NSArray*)items {
    _records = items;
    [self showHideWarnMessage];
    [self.tableView reloadData];
}

- (void)showTooltipAtCartWithMessage:(NSString*)mes {
    CMPopTipView *popTipView = [[CMPopTipView alloc] initWithMessage:mes];
    popTipView.dismissTapAnywhere = YES;
    popTipView.hasGradientBackground = NO;
    popTipView.hasShadow = NO;
    popTipView.has3DStyle = NO;
    popTipView.backgroundColor = self.theme.btnFrontColor;
    popTipView.textColor = self.theme.btnForegroundColor;
    popTipView.borderColor = self.theme.btnForegroundColor;
    [popTipView autoDismissAnimated:YES atTimeInterval:1.0];
    [popTipView presentPointingAtView:self.cartButton inView:self.view animated:YES];
}

#pragma mark - DTAThemeable

- (void)applyTheme {
    [super applyTheme];

    [self.btnCreateItem setTintColor:self.theme.btnFrontColor];
    self.btnCreateItem.backColor = self.theme.btnForegroundColor;
    self.btnCreateItem.frontColor = self.theme.btnForegroundColor;
    [self.btnCreateItem.titleLabel sizeToFit];
}

#pragma mark - Table View source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _records.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    DTACatBrowserCell *c = (DTACatBrowserCell *)cell;
    DTAItem *i = _records[indexPath.row];
    [c applyTheme:self.theme];
    c.tag = indexPath.row;
    c.name.text = i.title;
    c.price.text = [NSString stringWithFormat:@"%.2f ₽", i.price];
    c.delegate = self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRowCell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kItemRowHeight;
}

#pragma mark - UITableViewDelegate


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.output userWantsDeleteElementWithIndex:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - DTACatBrowserCellProtocol

- (void)plusTouched:(id)sender {
    [self.output addToCartItemWithIndex:[sender tag]];
}

#pragma mark - Actions

- (IBAction)createItemAction:(id)sender {
    [self.output createItem];
}

#pragma mark - Inner methods

- (void)showHideWarnMessage {
    if (!_records.count) {
        self.tableView.hidden = YES;
        self.viewNoItems.hidden = NO;
    }
    else {
        self.tableView.hidden = NO;
        self.viewNoItems.hidden = YES;
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath && gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self.output longPressOnCell:indexPath.row];
    }
}

@end
