//
//  DTACatBrowserCell.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DTACatBrowserCellProtocol
- (void)plusTouched:(id)sender;
@end

@class DTATheme;

@interface DTACatBrowserCell : UITableViewCell

@property (weak, nonatomic) id<DTACatBrowserCellProtocol> delegate;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *price;

- (void)applyTheme:(DTATheme*)theme;

@end
