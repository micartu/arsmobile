//
//  DTACatBrowserCell.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserCell.h"
#import "DTATheme.h"

@interface DTACatBrowserCell ()
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@end

@implementation DTACatBrowserCell

- (IBAction)plusAction:(id)sender {
    if (self.delegate)
        [self.delegate plusTouched:self];
}

- (void)applyTheme:(DTATheme*)theme {
    [self.btnPlus setTintColor:theme.btnForegroundColor];
}

@end
