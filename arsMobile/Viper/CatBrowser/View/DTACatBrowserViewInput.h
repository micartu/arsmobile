//
//  DTACatBrowserViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@protocol DTACatBrowserViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setWindowTitle:(NSString*)title;
- (void)setItems:(NSArray*)items;
- (void)showTooltipAtCartWithMessage:(NSString*)mes;

@end
