//
//  DTACatBrowserViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTACatBrowserViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)createItem;
- (void)addToCartItemWithIndex:(NSInteger)index;
- (void)userWantsDeleteElementWithIndex:(NSInteger)index;
- (void)moveToCart;
- (void)longPressOnCell:(NSInteger)index;

@end
