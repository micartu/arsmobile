//
//  DTACatBrowserViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTACatBrowserViewInput.h"

@protocol DTACatBrowserViewOutput;

@interface DTACatBrowserViewController : DTABaseViewController <DTACatBrowserViewInput>

@property (nonatomic, strong) id<DTACatBrowserViewOutput> output;

@end
