//
//  DTACatBrowserPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserPresenter.h"

#import "DTACatBrowserViewInput.h"
#import "DTACatBrowserInteractorInput.h"
#import "DTACatBrowserRouterInput.h"
#import "DTAChangeRequest.h"
#import "DTAChangeResponse.h"
#import "DTAChangerDialogModuleOutput.h"
#import "DTACategory.h"
#import "DTACartItem.h"
#import "DTAItem.h"

static NSString *const kName = @"name";
static NSString *const kPrice = @"price";
static NSString *const kCat = @"category";
static NSString *const kTaxes = @"taxes";
static NSString *const kUnits = @"units";

@interface DTACatBrowserPresenter () <DTAChangerDialogModuleOutput> {
    NSInteger _cid;
    NSMutableArray *_items;
    DTAItem* _item2update;
    NSArray<DTACategory*> *_categories;
}

@end

@implementation DTACatBrowserPresenter

#pragma mark - Methods of DTACatBrowserModuleInput

- (void)configureModuleWithCategoryId:(NSInteger)cid {
    _cid = cid;
    [self.interactor getTitleForCategoryId:cid];
    [self.interactor fetchItemsForCategoryId:cid];
    [self.interactor fetchAllCategorieswithCompletion:^(NSArray<DTACategory*> *categories) {
        _categories = categories;
    }];
}

#pragma mark - Methods of DTACatBrowserViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self updateBadge];
}

- (void)createItem {
    [self updateOrCreateItem:nil];
}

- (void)addToCartItemWithIndex:(NSInteger)index {
    __weak typeof (self) wself = self;
    void(^add2cart)(double count) = ^(double count){
        [wself.interactor addItem:index toCartWithCount:count withCompletion:^{
            // update badge for cart
            [wself updateBadge];
            DTACartItem *i = [wself.interactor findCartItemForIndex:index];
            assert(i);
            NSString *mes = [NSString stringWithFormat:@"В корзине содержится %g %@ %@",
                             i.count,
                             i.item.measureUnit.length > 0 ? i.item.measureUnit : @"x",
                             i.item.title];
            [wself.view showTooltipAtCartWithMessage:mes];
        }];
    };
    if ([wself.interactor askForAmountForObjectWithIndex:index]) {
        [wself.view inputDialogWithTitle:@"Внимание"
                             withMessage:@"Введите количество"
                             andOkAction:^(UIAlertAction *action, UIAlertController *alertController) {
                                 NSArray *textfields = alertController.textFields;
                                 UITextField *ifield = textfields[0];
                                 NSString *dnum = ifield.text;
                                 NSScanner *scanner = [NSScanner scannerWithString:dnum];
                                 double found_double;
                                 if ([scanner scanDouble:&found_double]) {
                                     add2cart(found_double);
                                     return;
                                 }
                                 [wself.view showErrorWithMessage:@"Неправильный ввод!"];
                             }];
    }
    else
        add2cart(1);
}

- (void)userWantsDeleteElementWithIndex:(NSInteger)index {
    assert (index < _items.count);
    __weak typeof (self) wself = self;
    DTAItem *i = [_items objectAtIndex:index];
    [wself.interactor deleteItem:i withCompletion:^{
        [_items removeObjectAtIndex:index];
        [wself.view setItems:_items];
    }];
}

- (void)moveToCart {
    [self.router changeToMenu:CartSelected];
}

- (void)longPressOnCell:(NSInteger)index {
    __weak typeof (self) wself = self;
    NSArray *titles = @[@"Изменить товар", @"Удалить товар", @"Отмена"];
    NSArray *actions = @[^(UIAlertAction *action) {
        assert (index < _items.count);
        _item2update = _items[index];
        [wself updateOrCreateItem:_item2update];
    },
    ^(UIAlertAction *action) {
        [wself userWantsDeleteElementWithIndex:index];
    },
    ^(UIAlertAction *action) {
        // do nothing for cancel
    }];
    NSArray *styles = @[[NSNumber numberWithInteger:UIAlertActionStyleDefault],
                        [NSNumber numberWithInteger:UIAlertActionStyleDestructive],
                        [NSNumber numberWithInteger:UIAlertActionStyleCancel]];
    [self.view chooseDialogWithTitle:@"Выберите действие"
                         withMessage:@"Что нужно сделать с элементом"
                    withActionTitles:titles
                          andActions:actions
                           andStyles:styles];
}

#pragma mark - Methods of DTACatBrowserInteractorOutput

- (void)titleForWindow:(NSString*)title {
    [self.view setWindowTitle:title];
}

- (void)itemsFetched:(NSArray*)items {
    _items = [NSMutableArray arrayWithArray:items];
    [self.view setItems:items];
}

#pragma mark - Methods of DTAChangerDialogModuleOutput

- (void)changerResponses:(NSArray<DTAChangeResponse *> *)responses {
    DTAItem *item;
    if (_item2update)
        item = _item2update;
    else
        item = [DTAItem new];
    for (DTAChangeResponse *r in responses) {
        if ([r.request.identifier isEqualToString:kName]) {
            item.title = r.changedValue;
        }
        else if ([r.request.identifier isEqualToString:kPrice]) {
            item.price = [r.changedValue floatValue];
        }
        else if ([r.request.identifier isEqualToString:kCat]) {
            item.category = _categories[r.selectedItem];
        }
        else if ([r.request.identifier isEqualToString:kTaxes]) {
            item.taxType = r.selectedItem + 1;
        }
        else if ([r.request.identifier isEqualToString:kUnits]) {
            item.measureUnit = r.changedValue;
        }
    }
    if (item.title.length > 0) {
        __weak typeof (self) wself = self;
        if (_item2update) {
            _item2update = nil;
            [wself.interactor updateItem:item withCompletion:^{
                [wself.interactor fetchItemsForCategoryId:_cid];
            }];
        }
        else {
            [wself.interactor createItem:item withCompletion:^{
                [wself.interactor fetchItemsForCategoryId:_cid];
            }];
        }
    }
}

#pragma mark - Private

- (void)updateBadge {
    __weak typeof (self) wself = self;
    [wself.view setBadgeCount:[wself.interactor countOfItemsInCart]];
}

- (void)updateOrCreateItem:(DTAItem*)item {
    NSMutableArray *p = [NSMutableArray arrayWithCapacity:5];
    // title of item
    DTAChangeRequest *req = [DTAChangeRequest new];
    req.identifier = kName;
    req.visibleTitle = @"Наименование товара:";
    req.hintTitle = @"Введите имя";
    if (item)
        req.defaultValue = item.title;
    [p addObject:req];

    // its price
    req = [DTAChangeRequest new];
    req.type = DTAChangeRequestTypeDigit;
    req.identifier = kPrice;
    req.visibleTitle = @"Цена:";
    req.hintTitle = @"Введите стоимость";
    if (item)
        req.defaultValue = [@(item.price) stringValue];
    [p addObject:req];

    // its category
    req = [DTAChangeRequest new];
    req.type = DTAChangeRequestTypeChooseList;
    req.identifier = kCat;
    req.visibleTitle = @"Категория товара:";
    NSMutableArray *catTitles = [NSMutableArray arrayWithCapacity:_categories.count];
    for (int i = 0; i < _categories.count; i++) {
        DTACategory *c = _categories[i];
        [catTitles addObject:c.title];
        if (c.cid == _cid)
            req.selectedItem = i;
    }
    req.titleList = catTitles;
    [p addObject:req];

    // tax of the item
    req = [DTAChangeRequest new];
    req.type = DTAChangeRequestTypeChooseList;
    req.identifier = kTaxes;
    req.visibleTitle = @"Тип налога:";
    req.titleList = [self.interactor giveDescriptionOfTaxes];
    if (item && item.taxType > 0)
        req.selectedItem = item.taxType - 1;
    else
        req.selectedItem = 0;
    [p addObject:req];

    // measurement units of the item
    req = [DTAChangeRequest new];
    req.type = DTAChangeRequestTypeTextWithChooseList;
    req.identifier = kUnits;
    req.visibleTitle = @"Единицы измерения:";
    req.titleList = [self.interactor givePossibleMeasureUnits];
    if (item)
        req.defaultValue = item.measureUnit;
    req.selectedItem = 0;
    [p addObject:req];

    [self.router openChangerDialogWithTitle:item ? @"Обновить товар" : @"Новый товар"
                             andButtonTitle:item ? @"Обновить" : @"Добавить"
                                andRequests:p
                          andOutputDelegate:self];
}

@end
