//
//  DTACatBrowserPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserViewOutput.h"
#import "DTACatBrowserInteractorOutput.h"
#import "DTACatBrowserModuleInput.h"

@protocol DTACatBrowserViewInput;
@protocol DTACatBrowserInteractorInput;
@protocol DTACatBrowserRouterInput;

@interface DTACatBrowserPresenter : NSObject <DTACatBrowserModuleInput, DTACatBrowserViewOutput, DTACatBrowserInteractorOutput>

@property (nonatomic, weak) id<DTACatBrowserViewInput> view;
@property (nonatomic, strong) id<DTACatBrowserInteractorInput> interactor;
@property (nonatomic, strong) id<DTACatBrowserRouterInput> router;

@end
