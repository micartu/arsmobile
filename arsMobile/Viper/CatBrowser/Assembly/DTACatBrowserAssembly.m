//
//  DTACatBrowserAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserAssembly.h"

#import "DTAServiceAssembly.h"
#import "DTACatBrowserViewController.h"
#import "DTACatBrowserInteractor.h"
#import "DTACatBrowserPresenter.h"
#import "DTACatBrowserRouter.h"
#import "DTARootAssembly.h"
#import "DTAAlertAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTACatBrowserAssembly

- (DTACatBrowserViewController *)viewCatBrowser {
    return [TyphoonDefinition withClass:[DTACatBrowserViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCatBrowser]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCatBrowser]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTACatBrowserInteractor *)interactorCatBrowser {
    return [TyphoonDefinition withClass:[DTACatBrowserInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCatBrowser]];
                              [definition injectProperty:@selector(storage)
                                                    with:[self.servicesAssembly dataStorageService]];
                          }];
}

- (DTACatBrowserPresenter *)presenterCatBrowser{
    return [TyphoonDefinition withClass:[DTACatBrowserPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCatBrowser]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCatBrowser]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCatBrowser]];
                          }];
}

- (DTACatBrowserRouter *)routerCatBrowser{
    return [TyphoonDefinition withClass:[DTACatBrowserRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCatBrowser]];
                              [definition injectProperty:@selector(storyboard)
                                                    with:[self.appAssembly storyboard]];
                              [definition injectProperty:@selector(root)
                                                    with:[self.rootAssembly presenterRoot]];
                          }];
}

@end
