//
//  DTACatBrowserRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "DTAChangerDialogModuleOutput.h"

@implementation DTACatBrowserRouter

#pragma mark - Methods of DTACatBrowserRouterInput

- (void)openChangerDialogWithTitle:(NSString*)title
                    andButtonTitle:(NSString*)btitle
                       andRequests:(NSArray*)req
                 andOutputDelegate:(id<DTAChangerDialogModuleOutput>)output {
    float width = [UIScreen mainScreen].bounds.size.width;
    float height = [UIScreen mainScreen].bounds.size.height;
    width *= 0.8;
    height *= 0.6;
    [self openChangerDialogWithTitle:title
                      andButtonTitle:btitle
                         andRequests:req
                            andWidth:width
                           andHeight:height
                   andOutputDelegate:output];
}

@end
