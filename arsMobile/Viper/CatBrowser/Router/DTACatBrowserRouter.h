//
//  DTACatBrowserRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserRouterInput.h"
#import "DTACommonRouter.h"


@interface DTACatBrowserRouter : DTACommonRouter <DTACatBrowserRouterInput>

@end
