//
//  DTACatBrowserRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTAChangerDialogModuleOutput;

@protocol DTACatBrowserRouterInput <DTACommonRouterInput>

- (void)openChangerDialogWithTitle:(NSString*)title
                    andButtonTitle:(NSString*)btitle
                       andRequests:(NSArray*)req
                 andOutputDelegate:(id<DTAChangerDialogModuleOutput>)output;

@end
