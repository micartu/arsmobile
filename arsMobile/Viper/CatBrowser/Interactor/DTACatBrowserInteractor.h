//
//  DTACatBrowserInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserInteractorInput.h"

@protocol DTACatBrowserInteractorOutput;
@protocol DTAStorageProtocol;

@interface DTACatBrowserInteractor : NSObject <DTACatBrowserInteractorInput>

@property (nonatomic, weak) id<DTACatBrowserInteractorOutput> output;
@property (nonatomic, weak) id<DTAStorageProtocol> storage;

@end
