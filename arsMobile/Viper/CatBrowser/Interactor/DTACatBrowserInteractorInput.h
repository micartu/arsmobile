//
//  DTACatBrowserInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTAItem;
@class DTACartItem;
@class DTACategory;

@protocol DTACatBrowserInteractorInput <NSObject>

- (void)getTitleForCategoryId:(NSInteger)cid;
- (void)fetchItemsForCategoryId:(NSInteger)cid;
- (void)createItem:(DTAItem*)item withCompletion:(void (^)(void))completion;
- (void)addItem:(NSInteger)index toCartWithCount:(float)count withCompletion:(void (^)(void))completion;
- (void)updateItem:(DTAItem*)item withCompletion:(void (^)(void))completion;
- (void)deleteItem:(DTAItem*)item withCompletion:(void (^)(void))completion;
- (void)fetchAllCategorieswithCompletion:(void (^)(NSArray<DTACategory*> *categories))completion;
- (BOOL)askForAmountForObjectWithIndex:(NSInteger)index;
- (DTACartItem*)findCartItemForIndex:(NSInteger)index;
- (NSInteger)countOfItemsInCart;
- (NSArray*)giveDescriptionOfTaxes;
- (NSArray*)givePossibleMeasureUnits;

@end
