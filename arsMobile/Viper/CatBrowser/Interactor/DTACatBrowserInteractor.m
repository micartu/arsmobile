//
//  DTACatBrowserInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatBrowserInteractor.h"

#import "DTACatBrowserInteractorOutput.h"
#import "DTAStorageProtocol.h"
#import "DTACategory.h"
#import "DTAItem.h"

@implementation DTACatBrowserInteractor {
    DTACategory *_category;
    NSArray<DTAItem*> *_items;
}

#pragma mark - Methods of DTACatBrowserInteractorInput

- (void)getTitleForCategoryId:(NSInteger)cid {
    [self.storage getCategoryWithId:cid andCompletion:^(DTACategory *cat) {
        _category = cat;
        if (cat)
            [self.output titleForWindow:cat.title];
    }];
}

- (void)fetchAllCategorieswithCompletion:(void (^)(NSArray<DTACategory*> *categories))completion {
    [self.storage fetchAllCategoriesWithCompletion:completion];
}

- (void)fetchItemsForCategoryId:(NSInteger)cid {
    [self.storage fetchAllItemsForCategoryID:cid withCompletion:^(NSArray<DTAItem*> *items) {
        _items = items;
        [self.output itemsFetched:items];
    }];
}

- (void)createItem:(DTAItem*)item withCompletion:(void (^)(void))completion {
    if (item.category == nil)
        item.category = _category;
    [self.storage createItem:item andCompletion:completion];
}

- (void)updateItem:(DTAItem*)item withCompletion:(void (^)(void))completion {
    [self.storage modifyItem:item andCompletion:completion];
}

- (void)addItem:(NSInteger)index toCartWithCount:(float)count withCompletion:(void (^)(void))completion {
    if (index < _items.count) {
        [self.storage createOrModifyCartForItem:_items[index]
                                       andCount:count
                                  andCompletion:completion];
    }
}

- (DTACartItem*)findCartItemForIndex:(NSInteger)index {
    if (index < _items.count) {
        return [self.storage findInCartTheItem:_items[index]];
    }
    return nil;
}

- (BOOL)askForAmountForObjectWithIndex:(NSInteger)index {
    if (index < _items.count && _items[index].measureUnit.length > 0) {
        return YES;
    }
    return NO;
}

- (void)deleteItem:(DTAItem*)item withCompletion:(void (^)(void))completion {
    [self.storage deleteItem:item andCompletion:completion];
}

- (NSInteger)countOfItemsInCart {
    return [self.storage countAllCartItems];
}

- (NSArray*)giveDescriptionOfTaxes {
    return @[@"НДС 18%",
              @"НДС 10%",
              @"НДС 0%",
              @"Без НДС",
              @"НДС 18/118",
              @"НДС 10/110"];
}

- (NSArray*)givePossibleMeasureUnits {
    return @[@"кг.",
             @"г.",
             @"л.",
             @"мл.",
             @"м.",
             @"см."];
}

@end
