//
//  DTACatBrowserInteractorOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTACatBrowserInteractorOutput <NSObject>

- (void)titleForWindow:(NSString*)title;
- (void)itemsFetched:(NSArray*)items;

@end
