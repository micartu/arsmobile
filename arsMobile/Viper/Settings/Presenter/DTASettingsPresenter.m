//
//  DTASettingsPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsPresenter.h"

#import "DTASettingsViewInput.h"
#import "DTASettingsInteractorInput.h"
#import "DTASettingsRouterInput.h"

@implementation DTASettingsPresenter

#pragma mark - Methods of DTASettingsModuleInput

- (void)configureModule {
}

#pragma mark - Methods of DTASettingsViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)openShift {
    [self.interactor openDay];
}

- (void)performXReport {
    [self.interactor xReport];
}

- (void)performZReport {
    [self.interactor zReport];
}

- (void)changeOperator {
    [self.router changeOperator];
}

- (void)changeDevice {
    [self.router changeDevice];
}

- (void)cancelCheque {
    [self.interactor cancelCheque];
}

#pragma mark - Methods of DTASettingsInteractorOutput

- (void)errorOccured:(NSError*)error {
    NSLog(@"an error occured: %@", error);
    if (error)
        [self.view showErrorWithMessage:error.userInfo[NSLocalizedDescriptionKey]];
    else
        [self.view showErrorWithMessage:@"Произошла неизвестная ошибка при печати чека"];
}

@end
