//
//  DTASettingsPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsViewOutput.h"
#import "DTASettingsInteractorOutput.h"
#import "DTASettingsModuleInput.h"

@protocol DTASettingsViewInput;
@protocol DTASettingsInteractorInput;
@protocol DTASettingsRouterInput;

@interface DTASettingsPresenter : NSObject <DTASettingsModuleInput, DTASettingsViewOutput, DTASettingsInteractorOutput>

@property (nonatomic, weak) id<DTASettingsViewInput> view;
@property (nonatomic, strong) id<DTASettingsInteractorInput> interactor;
@property (nonatomic, strong) id<DTASettingsRouterInput> router;

@end
