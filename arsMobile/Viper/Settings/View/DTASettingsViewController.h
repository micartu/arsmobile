//
//  DTASettingsViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTASettingsViewInput.h"

@protocol DTASettingsViewOutput;

@interface DTASettingsViewController : DTABaseViewController <DTASettingsViewInput>

@property (nonatomic, strong) id<DTASettingsViewOutput> output;

@end
