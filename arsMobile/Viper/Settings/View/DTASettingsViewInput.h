//
//  DTASettingsViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@protocol DTASettingsViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;

@end
