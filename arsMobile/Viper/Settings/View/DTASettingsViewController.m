//
//  DTASettingsViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsViewController.h"

#import "DTASettingsViewOutput.h"
#import "DTAOperatorCell.h"
#import "DTATheme.h"
#import "constants.h"

static NSString *const kSettingsCell = @"settingsCell";
static NSString *const kOperatorCell = @"operatorNameCell";
static CGFloat kHeaderViewActions = 50;
static CGFloat kHeaderViewCassier = 200;
static CGFloat kHeaderViewDevice = 20;
static CGFloat kCassierHeight = 78;
static CGFloat kSettingsRowHeight = 44;

@interface DTASettingsViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation DTASettingsViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
    self.rootMenu = YES;
	[super viewDidLoad];

    self.title = @"Настройки ФН";

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

#pragma mark - Methods of DTASettingsViewInput

- (void)setupInitialState {
    [super setupInitialState];
    [self applyTheme];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return 4;
    else if (section == 1)
        return 2;
    // (section == 2)
    return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = self.theme.btnForegroundColor;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"Открыть смену";
                break;

            case 1:
                cell.textLabel.text = @"Отчет без закрытия смены";
                break;

            case 2:
                cell.textLabel.text = @"Закрыть смену";
                break;

            case 3:
                cell.textLabel.text = @"Закрыть чек";
                break;
        }
    }
    else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"Сменить кассира";
                break;

            case 1:
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                DTAOperatorCell *ocell = (DTAOperatorCell *)cell;
                NSString *operator = [defaults objectForKey:kDefaultsKeyOperatorName];
                ocell.name.text = operator.length ? operator : @"?";
            }
                break;
        }
    }
    else if (indexPath.section == 2) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"Сменить устройство";
                break;

            case 1:
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                DTAOperatorCell *pcell = (DTAOperatorCell *)cell;
                NSString *printer = [defaults objectForKey:kDefaultsKeySelectedPrinterName];
                pcell.name.text = printer.length ? printer : @"?";
                pcell.explanation.text = @"Устройство:";
            }
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!section)
        return kHeaderViewActions;
    else if (section == 1)
        return kHeaderViewCassier;
    return kHeaderViewDevice;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if ((indexPath.section == 1 && indexPath.row == 1) ||
        (indexPath.section == 2 && indexPath.row == 1)) {
        cell = [tableView dequeueReusableCellWithIdentifier:kOperatorCell];
    }
    else
        cell = [tableView dequeueReusableCellWithIdentifier:kSettingsCell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((indexPath.section == 1 && indexPath.row == 1) ||
        (indexPath.section == 2 && indexPath.row == 1)) {
        return kCassierHeight;
    }
    return kSettingsRowHeight;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                [self.output openShift];
                break;
            case 1:
                [self.output performXReport];
                break;
            case 2:
                [self.output performZReport];
                break;
            case 3:
                [self.output cancelCheque];
                break;
        }
    }
    else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
                [self.output changeOperator];
                break;
        }
    }
    else if (indexPath.section == 2) {
        switch (indexPath.row) {
            case 0:
                [self.output changeDevice];
                break;
        }
    }
}

@end
