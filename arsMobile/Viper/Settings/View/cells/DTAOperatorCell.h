//
//  DTAOperatorCell.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTAOperatorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *explanation;
@property (weak, nonatomic) IBOutlet UILabel *name;
@end
