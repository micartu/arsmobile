//
//  DTASettingsRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTASettingsRouterInput <DTACommonRouterInput>

- (void)changeDevice;
- (void)changeOperator;

@end
