//
//  DTASettingsRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsRouterInput.h"
#import "DTACommonRouter.h"

@protocol DTALeftMenuInputProtocol;

@interface DTASettingsRouter : DTACommonRouter <DTASettingsRouterInput>
@property (nonatomic, strong) id<DTALeftMenuInputProtocol> leftMenu;
@end
