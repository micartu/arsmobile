//
//  DTASettingsRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import <STPopup/STPopup.h>
#import "storyboard_constants.h"
#import "constants.h"
#import "DTALeftMenuInputProtocol.h"
#import "DTAChangeRequest.h"
#import "DTAChangeResponse.h"
#import "DTAChangerDialogModuleInput.h"
#import "DTAChangerDialogModuleOutput.h"

@interface DTASettingsRouter () <DTAChangerDialogModuleOutput> {
    void(^_handler)(NSString *categoryName);
}
@end

@implementation DTASettingsRouter

#pragma mark - Methods of DTASettingsRouterInput

- (void)changeDevice {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kChangeDeviceVC];
    vc.contentSizeInPopup = CGSizeMake(kPopUpWidth, kPopUpHeight);
    STPopupController *popup = [[STPopupController alloc] initWithRootViewController:vc];
    popup.containerView.layer.cornerRadius = kCornerRadius;
    [popup presentInViewController:(id)self.transitionHandler];
}

- (void)changeOperator {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    __weak typeof (self) wself = self;
    _handler = ^(NSString *name) {
        if (name.length) {
            [defaults setObject:name forKey:kDefaultsKeyOperatorName];
            [defaults synchronize];
            [wself.leftMenu changeOperatorName:name];
        }
    };
    DTAChangeRequest *r = [DTAChangeRequest new];
    r.identifier = @"name";
    r.visibleTitle = @"Имя оператора:";
    r.hintTitle = @"Введите имя оператора";
    NSString *name = [defaults objectForKey:kDefaultsKeyOperatorName];
    if (name.length)
        r.defaultValue = name;
    [self openChangerDialogWithTitle:@"Оператор"
                      andButtonTitle:@"Применить"
                         andRequests:@[r]
                            andWidth:kPopUpWidth
                           andHeight:kPopUpHeight
                   andOutputDelegate:self];
}

#pragma mark - DTAChangerDialogModuleOutput

- (void)changerResponses:(NSArray<DTAChangeResponse *>*)responses {
    DTAChangeResponse *r = responses.firstObject;
    if (_handler)
        _handler(r.changedValue);
}

@end
