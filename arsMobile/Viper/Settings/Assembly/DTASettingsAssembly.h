//
//  DTASettingsAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

@class DTAMVCAssembly;

/**
 @author Michael Artuerhof

 Settings module
 */
@interface DTASettingsAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@property(nonatomic, strong, readonly) DTAMVCAssembly *mvc;

@end
