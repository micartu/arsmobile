//
//  DTASettingsAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsAssembly.h"

#import "DTASettingsViewController.h"
#import "DTASettingsInteractor.h"
#import "DTASettingsPresenter.h"
#import "DTASettingsRouter.h"
#import "DTARootAssembly.h"
#import "DTAServiceAssembly.h"
#import "DTAAlertAssembly.h"
#import "DTAMVCAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTASettingsAssembly

- (DTASettingsViewController *)viewSettings {
    return [TyphoonDefinition withClass:[DTASettingsViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSettings]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSettings]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(root)
                                                    with:[self.rootAssembly presenterRoot]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTASettingsInteractor *)interactorSettings {
    return [TyphoonDefinition withClass:[DTASettingsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSettings]];
                              [definition injectProperty:@selector(printer)
                                                    with:[self.servicesAssembly fiscalPrinterService]];
                          }];
}

- (DTASettingsPresenter *)presenterSettings{
    return [TyphoonDefinition withClass:[DTASettingsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSettings]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSettings]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSettings]];
                          }];
}

- (DTASettingsRouter *)routerSettings{
    return [TyphoonDefinition withClass:[DTASettingsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSettings]];
                              [definition injectProperty:@selector(storyboard)
                                                    with:[self.appAssembly storyboard]];
                              [definition injectProperty:@selector(leftMenu)
                                                    with:[self.mvc menuTranslator]];
                          }];
}

@end
