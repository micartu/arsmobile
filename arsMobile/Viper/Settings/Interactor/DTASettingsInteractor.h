//
//  DTASettingsInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsInteractorInput.h"

@protocol DTASettingsInteractorOutput;
@protocol DTAFPrinterProtocol;

@interface DTASettingsInteractor : NSObject <DTASettingsInteractorInput>

@property (nonatomic, weak) id<DTASettingsInteractorOutput> output;
@property (nonatomic, strong) id<DTAFPrinterProtocol> printer;

@end
