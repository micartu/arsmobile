//
//  DTASettingsInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTASettingsInteractor.h"

#import "DTASettingsInteractorOutput.h"
#import "DTAFPrinterProtocol.h"
#import "constants.h"

@implementation DTASettingsInteractor

#pragma mark - Methods of DTASettingsInteractorInput

- (void)openDay {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *op = [defaults objectForKey:kDefaultsKeyOperatorName];
    if (!op) op = @"";
    [self.printer openShiftForOperator:op withCompletion:^(BOOL success, NSError *er) {
        if (er || !success)
            [self.output errorOccured:er];
    }];
}

- (void)xReport {
    [self.printer performXReportWithCompletion:^(BOOL success, NSError *er) {
        if (er || !success)
            [self.output errorOccured:er];
    }];
}

- (void)zReport {
    [self.printer performZReportWithCompletion:^(BOOL success, NSError *er) {
        if (er || !success)
            [self.output errorOccured:er];
    }];
}

- (void)cancelCheque {
    [self.printer cancelChequeWithCompletion:^(BOOL success, NSError *er) {
        if (er || !success)
            [self.output errorOccured:er];
    }];
}

@end
