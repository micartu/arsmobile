//
//  DTASettingsInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTASettingsInteractorInput <NSObject>

- (void)openDay;
- (void)xReport;
- (void)zReport;
- (void)cancelCheque;

@end
