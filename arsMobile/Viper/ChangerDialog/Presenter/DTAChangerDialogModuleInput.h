//
//  DTAChangerDialogModuleInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DTAChangerDialogModuleInput <RamblerViperModuleInput>

/**
 @author Michael Artuerhof

 Method inializes the basic configuration of the module
 */
- (void)configureModuleWithTitle:(NSString*)title andButtonTitle:(NSString*)btitle andChangeRequests:(NSArray *)requests;

@end
