//
//  DTAChangerDialogModuleOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@class DTAChangeResponse;

@protocol DTAChangerDialogModuleOutput <RamblerViperModuleOutput>

- (void)changerResponses:(NSArray<DTAChangeResponse*>*)responses;

@end
