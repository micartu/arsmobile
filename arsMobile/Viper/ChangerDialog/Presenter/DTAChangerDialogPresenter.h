//
//  DTAChangerDialogPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogViewOutput.h"
#import "DTAChangerDialogInteractorOutput.h"
#import "DTAChangerDialogModuleInput.h"

@protocol DTAChangerDialogViewInput;
@protocol DTAChangerDialogInteractorInput;
@protocol DTAChangerDialogRouterInput;
@protocol DTAChangerDialogModuleOutput;

@interface DTAChangerDialogPresenter : NSObject <DTAChangerDialogModuleInput, DTAChangerDialogViewOutput, DTAChangerDialogInteractorOutput>

@property (nonatomic, weak) id<DTAChangerDialogViewInput> view;
@property (nonatomic, strong) id<DTAChangerDialogInteractorInput> interactor;
@property (nonatomic, strong) id<DTAChangerDialogRouterInput> router;
@property (nonatomic, weak) id<DTAChangerDialogModuleOutput> moduleOutput;

@end
