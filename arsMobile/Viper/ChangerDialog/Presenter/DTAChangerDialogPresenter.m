//
//  DTAChangerDialogPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogPresenter.h"

#import "DTAChangerDialogViewInput.h"
#import "DTAChangeRequest.h"
#import "DTAChangeResponse.h"
#import "DTAChangerDialogInteractorInput.h"
#import "DTAChangerDialogRouterInput.h"
#import "DTAChangerDialogModuleOutput.h"
#import "DTAListChooserModuleOutput.h"

@interface DTAChangerDialogPresenter () <DTAListChooserModuleOutput> {
    DTAChangeResponse *_list2change;
    BOOL _copy2editField;
    NSInteger _selectedIndex;
}
@property (nonatomic, strong) NSArray<DTAChangeRequest*> *paramRequests;
@property (nonatomic, strong) NSArray<DTAChangeResponse*> *requestsResponses;
@property (nonatomic, copy) NSString *btitle;
@end

@implementation DTAChangerDialogPresenter

#pragma mark - Methods of DTAChangerDialogModuleInput

- (void)configureModuleWithTitle:(NSString*)title andButtonTitle:(NSString*)btitle andChangeRequests:(NSArray *)requests {
    NSMutableArray *requestsResponses = [NSMutableArray new];
    for (DTAChangeRequest *request in requests) {
        DTAChangeResponse *response = [DTAChangeResponse new];
        response.request = request;
        response.changedValue = request.defaultValue;
        response.selectedItem = request.selectedItem;
        [requestsResponses addObject:response];
    }
    self.paramRequests = requests;
    self.requestsResponses = requestsResponses;
    self.btitle = btitle;
    [self.view setWindowTitle:title];
}

#pragma mark - Methods of DTAChangerDialogViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.view setButtonTitle:self.btitle];
    [self.view showParamRequests:self.paramRequests];
}

- (void)valueDidChange:(NSString*)value forParamRequest:(DTAChangeRequest*)request {
    for (DTAChangeResponse *response in self.requestsResponses) {
        if ([response.request isEqual:request]) {
            response.changedValue = value;
            break;
        }
    }
}

- (void)elementTouchedWithIndex:(NSInteger)index {
    assert(index < self.requestsResponses.count);
    if (self.paramRequests[index].type == DTAChangeRequestTypeChooseList ||
        self.paramRequests[index].type == DTAChangeRequestTypeTextWithChooseList) {
        _list2change = self.requestsResponses[index];
        if (self.paramRequests[index].type == DTAChangeRequestTypeTextWithChooseList) {
            _copy2editField = YES;
            _list2change.selectedItem = 0;
        }
        else
            _copy2editField = NO;
        _selectedIndex = index;
        [self.router openListChooserWithItemsToChoose:_list2change.request.titleList
                                          andSelected:_list2change.selectedItem
                                            withTitle:@"Выберите элемент из списка"
                                            andOutput:self];
    }
}


- (void)applyTouched {
    [self.moduleOutput changerResponses:self.requestsResponses];
    [self.router closeModule];
}

#pragma mark - Methods of DTAListChooserModuleOutput

- (void)elementWasChosenWithIndex:(NSInteger)index {
    if (_list2change) {
        _list2change.request.selectedItem = index;
        _list2change.selectedItem = index;
        if (_copy2editField) {
            assert(index < _list2change.request.titleList.count);
            NSString *val = _list2change.request.titleList[index];
            [self.view changeValue:val
                    forItemAtIndex:_selectedIndex];
            self.requestsResponses[_selectedIndex].changedValue = val;
        }
        _list2change = nil;
        [self.view refresh];
    }
}

#pragma mark - Methods of DTAChangerDialogInteractorOutput

@end
