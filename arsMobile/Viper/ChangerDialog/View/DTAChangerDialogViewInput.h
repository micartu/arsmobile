//
//  DTAChangerDialogViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTAChangeRequest;

@protocol DTAChangerDialogViewInput <NSObject>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setWindowTitle:(NSString*)title;
- (void)setButtonTitle:(NSString*)title;
- (void)showParamRequests:(NSArray<DTAChangeRequest*>*)paramRequests;
- (void)changeValue:(NSString*)value forItemAtIndex:(NSInteger)index;
- (void)refresh;

@end
