//
//  DTAChangerDialogChooseCell.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11.01.18.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTAChangerDialogChooseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedLabel;

@end
