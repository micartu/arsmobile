//
//  DTAChangerDialogViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTAChangerDialogViewInput.h"

@protocol DTAChangerDialogViewOutput;

@interface DTAChangerDialogViewController : DTABaseViewController <DTAChangerDialogViewInput>

@property (nonatomic, strong) id<DTAChangerDialogViewOutput> output;

@end
