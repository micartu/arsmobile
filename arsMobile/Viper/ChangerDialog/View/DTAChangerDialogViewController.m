//
//  DTAChangerDialogViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogViewController.h"

#import "DTAChangeRequest.h"
#import "DTAChangerDialogChooseCell.h"
#import "DTAChangerDialogTextFieldCell.h"
#import "DTAChangerDialogViewOutput.h"
#import "DTACornedButton.h"
#import "DTATheme.h"

@interface DTAChangerDialogViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet DTACornedButton *btnApply;

@property (nonatomic, strong) NSArray<DTAChangeRequest*> *requests;
@property (nonatomic, strong) NSMutableArray<NSString*> *values;

@end

@implementation DTAChangerDialogViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DTAChangerDialogTextFieldCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([DTAChangerDialogTextFieldCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DTAChangerDialogChooseCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([DTAChangerDialogChooseCell class])];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight:60];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];

    [self applyTheme];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTAChangerDialogViewInput

- (void)setupInitialState {
}

- (void)setWindowTitle:(NSString*)title {
    self.title = title;
}

- (void)setButtonTitle:(NSString*)title {
    [self.btnApply setTitle:title forState:UIControlStateNormal];
}

- (void)showParamRequests:(NSArray<DTAChangeRequest*>*)paramRequests {
    self.requests = paramRequests;
    self.values = [NSMutableArray new];

    for (DTAChangeRequest *paramRequest in self.requests)
        [self.values addObject:paramRequest.defaultValue ? paramRequest.defaultValue : @""];

    [self.tableView reloadData];
}

- (void)changeValue:(NSString*)value forItemAtIndex:(NSInteger)index {
    assert(index < self.values.count);
    self.values[index] = value;
}

- (void)refresh {
    [self.tableView reloadData];
}

#pragma mark - DTAThemeable

- (void)applyTheme {
    [super applyTheme];

    [self.btnApply setTintColor:self.theme.btnFrontColor];
    self.btnApply.backColor = self.theme.btnForegroundColor;
    self.btnApply.frontColor = self.theme.btnForegroundColor;
    [self.btnApply.titleLabel sizeToFit];
}

#pragma mark - Table View source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.requests.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    DTAChangeRequest *r = self.requests[indexPath.row];
    if (r.type == DTAChangeRequestTypeText ||
        r.type == DTAChangeRequestTypeTextWithChooseList ||
        r.type == DTAChangeRequestTypeDigit) {
        DTAChangerDialogTextFieldCell *c = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DTAChangerDialogTextFieldCell class])
                                                                           forIndexPath:indexPath];
        c.titleLabel.text = r.visibleTitle;
        c.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        c.textField.text = self.values[indexPath.row];
        c.textField.tag = indexPath.row;
        c.textField.placeholder = r.hintTitle;
        if (r.type == DTAChangeRequestTypeDigit)
            c.textField.keyboardType = UIKeyboardTypeDecimalPad;
        c.textField.delegate = self;
        [c.textField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        if (r.type == DTAChangeRequestTypeTextWithChooseList)
            [c setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        else
            [c setAccessoryType:UITableViewCellAccessoryNone];
        cell = c;
    }
    else if (r.type == DTAChangeRequestTypeChooseList) {
        DTAChangerDialogChooseCell *c = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DTAChangerDialogChooseCell class])
                                                                           forIndexPath:indexPath];
        c.titleLabel.text = r.visibleTitle;
        if (r.selectedItem < r.titleList.count) {
            c.selectedLabel.text = r.titleList[r.selectedItem];
        }
        [c setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        cell = c;
    }
    else
        cell = nil;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.output elementTouchedWithIndex:indexPath.row];
}

#pragma mark - Actions

- (void)textFieldTextChanged:(UITextField *)sender {
    if (sender.tag < self.values.count) {
        [self.values replaceObjectAtIndex:sender.tag withObject:sender.text];
        [self.output valueDidChange:sender.text forParamRequest:self.requests[sender.tag]];
    }
}

- (IBAction)applyAction:(id)sender {
    [self.output applyTouched];
}

@end
