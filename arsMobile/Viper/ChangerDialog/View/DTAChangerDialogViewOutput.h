//
//  DTAChangerDialogViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTAChangeRequest;

@protocol DTAChangerDialogViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)valueDidChange:(NSString*)value forParamRequest:(DTAChangeRequest*)request;
- (void)elementTouchedWithIndex:(NSInteger)index;
- (void)applyTouched;

@end
