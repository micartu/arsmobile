//
//  DTAChangerDialogAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogAssembly.h"

#import "DTAChangerDialogViewController.h"
#import "DTAChangerDialogInteractor.h"
#import "DTAChangerDialogPresenter.h"
#import "DTAChangerDialogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTAChangerDialogAssembly

- (DTAChangerDialogViewController *)viewChangerDialog {
    return [TyphoonDefinition withClass:[DTAChangerDialogViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChangerDialog]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterChangerDialog]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                          }];
}

- (DTAChangerDialogInteractor *)interactorChangerDialog {
    return [TyphoonDefinition withClass:[DTAChangerDialogInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChangerDialog]];
                          }];
}

- (DTAChangerDialogPresenter *)presenterChangerDialog{
    return [TyphoonDefinition withClass:[DTAChangerDialogPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewChangerDialog]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorChangerDialog]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerChangerDialog]];
                          }];
}

- (DTAChangerDialogRouter *)routerChangerDialog{
    return [TyphoonDefinition withClass:[DTAChangerDialogRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewChangerDialog]];
                          }];
}

@end
