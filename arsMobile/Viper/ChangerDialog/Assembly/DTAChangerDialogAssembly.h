//
//  DTAChangerDialogAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

/**
 @author Michael Artuerhof

 ChangerDialog module
 */
@interface DTAChangerDialogAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@end
