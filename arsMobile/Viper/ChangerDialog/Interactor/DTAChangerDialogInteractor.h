//
//  DTAChangerDialogInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogInteractorInput.h"

@protocol DTAChangerDialogInteractorOutput;

@interface DTAChangerDialogInteractor : NSObject <DTAChangerDialogInteractorInput>

@property (nonatomic, weak) id<DTAChangerDialogInteractorOutput> output;

@end
