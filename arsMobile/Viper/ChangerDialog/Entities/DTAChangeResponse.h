//
//  DTAChangeResponse.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTAChangeRequest;

@interface DTAChangeResponse : NSObject

@property (nonatomic, strong) DTAChangeRequest *request;
@property (nonatomic, strong) NSString *changedValue;
@property (nonatomic, assign) NSInteger selectedItem;

@end
