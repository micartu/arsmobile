//
//  DTAChangeRequest.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DTAChangeRequestType) {
    DTAChangeRequestTypeText = 0,
    DTAChangeRequestTypeDigit,
    DTAChangeRequestTypeTextWithChooseList,
    DTAChangeRequestTypeChooseList,
};

@interface DTAChangeRequest : NSObject

@property (nonatomic, assign) NSInteger type;
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *visibleTitle;
@property (nonatomic, strong) NSString *hintTitle;
@property (nonatomic, strong) NSString *defaultValue;
@property (nonatomic, strong) NSArray<NSString*> *titleList;
@property (nonatomic, assign) NSInteger selectedItem;

@end
