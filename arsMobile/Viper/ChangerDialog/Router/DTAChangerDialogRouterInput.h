//
//  DTAChangerDialogRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAListChooserModuleOutput;

@protocol DTAChangerDialogRouterInput <NSObject>

- (void)closeModule;
- (void)openListChooserWithItemsToChoose:(NSArray*)items
                             andSelected:(NSInteger)selected
                               withTitle:(NSString*)title
                               andOutput:(id<DTAListChooserModuleOutput>)output;

@end
