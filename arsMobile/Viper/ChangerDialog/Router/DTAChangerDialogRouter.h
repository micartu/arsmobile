//
//  DTAChangerDialogRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTAChangerDialogRouter : NSObject <DTAChangerDialogRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
