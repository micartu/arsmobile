//
//  DTAChangerDialogRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAChangerDialogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "DTAListChooserModuleInput.h"
#import "DTAListChooserModuleOutput.h"
#import "storyboard_constants.h"

@implementation DTAChangerDialogRouter

#pragma mark - Methods of DTAChangerDialogRouterInput

- (void)closeModule {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openListChooserWithItemsToChoose:(NSArray*)items
                             andSelected:(NSInteger)selected
                               withTitle:(NSString*)title
                               andOutput:(id<DTAListChooserModuleOutput>)output {
    [[self.transitionHandler openModuleUsingSegue:kSegueListChooser] thenChainUsingBlock:
     ^id<RamblerViperModuleOutput>(id<DTAListChooserModuleInput> moduleInput) {
         [moduleInput configureModuleWithItems:items andSelected:selected withTitle:title];
         return output;
     }];
}

@end
