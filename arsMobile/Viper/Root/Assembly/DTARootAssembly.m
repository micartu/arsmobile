//
//  DTARootAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootAssembly.h"

#import "DTARootInteractor.h"
#import "DTARootPresenter.h"
#import "DTARootRouter.h"
#import "DTAMVCAssembly.h"
#import "DTAFPrinterProtocol.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTARootAssembly

- (DTARootInteractor *)interactorRoot {
    return [TyphoonDefinition withClass:[DTARootInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRoot]];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (id<DTARootModuleInput>)presenterRoot{
    return [TyphoonDefinition withClass:[DTARootPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorRoot]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerRoot]];
                              [definition injectProperty:@selector(leftMenu)
                                                    with:[_mvcAssembly menuTranslator]];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (DTARootRouter *)routerRoot{
    return [TyphoonDefinition withClass:[DTARootRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(storyboard)
                                                    with:[self.appAssembly storyboard]];
                              [definition injectProperty:@selector(leftMenu)
                                                    with:[_mvcAssembly menuTranslator]];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

@end
