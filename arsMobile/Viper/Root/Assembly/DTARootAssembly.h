//
//  DTARootAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

@class DTAMVCAssembly;
@class DTARootPresenter;
@protocol DTARootModuleInput;

/**
 @author Michael Artuerhof

 Root module
 */
@interface DTARootAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@property(nonatomic, strong, readonly) DTAMVCAssembly *mvcAssembly;

- (id<DTARootModuleInput>)presenterRoot;

@end
