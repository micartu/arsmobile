//
//  DTARootPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootInteractorOutput.h"
#import "DTARootModuleInput.h"
#import "DTALeftMenuOutputProtocol.h"

@protocol DTARootInteractorInput;
@protocol DTARootRouterInput;
@protocol DTALeftMenuInputProtocol;

@interface DTARootPresenter : NSObject <DTARootModuleInput,
DTARootInteractorOutput,
DTALeftMenuOutputProtocol>

@property (nonatomic, strong) id<DTARootInteractorInput> interactor;
@property (nonatomic, strong) id<DTARootRouterInput> router;
@property (nonatomic, strong) id<DTALeftMenuInputProtocol> leftMenu;

@end
