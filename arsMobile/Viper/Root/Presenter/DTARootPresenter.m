//
//  DTARootPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootPresenter.h"

#import "DTARootInteractorInput.h"
#import "DTARootRouterInput.h"
#import "LGSideMenuController.h"
#import "DTALeftMenuInputProtocol.h"



@interface DTARootPresenter () {
    MenuSelected _mselected;
}

@property (nonatomic, strong) LGSideMenuController *sideMenu;

@end

@implementation DTARootPresenter

- (void)dealloc {
    NSLog(@"DTARootPresenter Deallocated!");
}

#pragma mark - Methods of DTARootModuleInput

- (UIViewController*)configureInitialVC {
    _mselected = [self.interactor getSavedController];
    UIViewController *menu = [self.router menuController];
    UIViewController *root = [self getViewController:_mselected];
    self.sideMenu = [LGSideMenuController sideMenuControllerWithRootViewController:root
                                                                leftViewController:menu
                                                               rightViewController:nil];
    self.sideMenu.leftViewWidth = 260.0;
    self.sideMenu.leftViewPresentationStyle = LGSideMenuPresentationStyleSlideBelow;

    return self.sideMenu;
}

- (void)showMenu:(BOOL)show {
    if (self.sideMenu) {
        BOOL curState = self.sideMenu.isLeftViewShowing;
        if (curState != show) {
            if (self.sideMenu.isLeftViewHidden)
                [self.sideMenu showLeftViewAnimated];
            else
                [self.sideMenu hideLeftViewAnimated];
        }
    }
}

- (void)changeSelectedMenuTo:(MenuSelected)menu {
    _mselected = menu;
    [self activateController];
}

#pragma mark - Methods of DTARootInteractorOutput

#pragma mark - Methods of DTALeftMenuProtocol

- (void)cartMenuSelected {
    if (_mselected != CartSelected) {
        _mselected = CartSelected;
        [self activateController];
    }
}

- (void)catalogMenuSelected {
    if (_mselected != CatalogSelected) {
        _mselected = CatalogSelected;
        [self activateController];
    }
}

- (void)settingsMenuSelected {
    if (_mselected != SettingsSelected) {
        _mselected = SettingsSelected;
        [self activateController];
    }
}

- (void)aboutMenuSelected {
    if (_mselected != AboutSelected) {
        _mselected = AboutSelected;
        [self activateController];
    }
}

#pragma mark - Inner functions

- (void)activateController {
    [self showViewController:[self getViewController:_mselected]];
    [self.interactor saveActiveController:_mselected];
}

- (UIViewController*)getViewController:(int)selected {
    [self.leftMenu markPositionSelected:selected];
    switch (selected) {
        case CartSelected:
            return [self.router cartController];
        case CatalogSelected:
            return [self.router catalogController];
        case SettingsSelected:
            return [self.router settingsController];
        case AboutSelected:
            return [self.router aboutController];
    }
    return [self.router cartController];
}

- (void)showViewController:(id)viewController {
    self.sideMenu.rootViewController = viewController;

    if (self.sideMenu.isLeftViewVisible)
        [self.sideMenu hideLeftViewAnimated];
}

@end
