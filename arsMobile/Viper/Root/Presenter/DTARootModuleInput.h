//
//  DTARootModuleInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

typedef NS_ENUM(NSInteger, MenuSelected) {
    CartSelected = 0,
    CatalogSelected,
    SettingsSelected,
    AboutSelected,
};

@protocol DTARootModuleInput <RamblerViperModuleInput>

- (UIViewController*)configureInitialVC;

- (void)showMenu:(BOOL)show;
- (void)changeSelectedMenuTo:(MenuSelected)menu;

@end
