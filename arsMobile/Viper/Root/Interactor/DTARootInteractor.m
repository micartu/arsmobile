//
//  DTARootInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootInteractor.h"

#import "DTARootInteractorOutput.h"
#import "constants.h"

@implementation DTARootInteractor

#pragma mark - Methods of DTARootInteractorInput

- (int)getSavedController {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *num = [defaults objectForKey:kLastUsedController];
    if (num)
        return (int)[num integerValue];
    return 0;
}

- (void)saveActiveController:(int)controller {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:controller] forKey:kLastUsedController];
    [defaults synchronize];
}

@end
