//
//  DTARootInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootInteractorInput.h"

@protocol DTARootInteractorOutput;

@interface DTARootInteractor : NSObject <DTARootInteractorInput>

@property (nonatomic, weak) id<DTARootInteractorOutput> output;

@end
