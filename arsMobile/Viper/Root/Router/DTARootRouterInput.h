//
//  DTARootRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTALeftMenuModuleOutput;

@protocol DTARootRouterInput <NSObject>

- (id)menuController;
- (id)cartController;
- (id)catalogController;
- (id)settingsController;
- (id)aboutController;

@end
