//
//  DTARootRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "DTACartModuleInput.h"
#import "DTACatalogModuleInput.h"
#import "DTASettingsModuleInput.h"
#import "DTALeftMenuController.h"
#import "DTAMenuTranslator.h"
#import "storyboard_constants.h"

@implementation DTARootRouter

#pragma mark - Methods of DTARootRouterInput

- (id)menuController {
    DTALeftMenuController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kMenuVCId];
    vc.delegate = self.leftMenu; // BUG in Typhoon?
    ((DTAMenuTranslator*)self.leftMenu).view = (id)vc; // Another stuff was, I had to reinstall it manually
    return vc;
}

- (id)elementWithId:(NSString*)eid {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:eid];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    return nav;
}

- (id)cartController {
    return [self elementWithId:kCartVCId];
}

- (id)catalogController {
    return [self elementWithId:kCatalogVCId];
}

- (id)settingsController {
    return [self elementWithId:kSettingsVCId];
}

- (id)aboutController {
    return [self elementWithId:kAboutVCId];
}

@end
