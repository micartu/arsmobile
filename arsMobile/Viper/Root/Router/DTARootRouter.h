//
//  DTARootRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTARootRouterInput.h"
#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTARootRouter : NSObject <DTARootRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;
@property (nonatomic, strong) UIStoryboard *storyboard;
@property (nonatomic, strong) id leftMenu;

@end
