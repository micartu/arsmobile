//
//  DTABaseModuleAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import "DTAThemeAssembly.h"
#import "ApplicationAssembly.h"

@class DTARootAssembly;
@class DTAServiceAssembly;
@class DTAAlertAssembly;

@interface DTABaseModuleAssembly : TyphoonAssembly

@property(nonatomic, strong, readonly) DTAThemeAssembly *themeProvider;
@property(nonatomic, strong, readonly) ApplicationAssembly *appAssembly;
@property(nonatomic, strong, readonly) DTAServiceAssembly *servicesAssembly;
@property(nonatomic, strong, readonly) DTAAlertAssembly *alertsAssembly;
@property(nonatomic, strong, readonly) DTARootAssembly *rootAssembly;

@end
