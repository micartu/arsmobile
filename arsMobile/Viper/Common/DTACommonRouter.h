//
//  DTACommonRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@class UIStoryboard;
@protocol RamblerViperModuleTransitionHandlerProtocol;
@protocol DTARootModuleInput;

@interface DTACommonRouter : NSObject <DTACommonRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;
@property (nonatomic, strong) UIStoryboard *storyboard;
@property (strong, nonatomic) id<DTARootModuleInput> root;

@end
