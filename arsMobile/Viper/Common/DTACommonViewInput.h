//
//  DTACommonViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAAlertProtocol.h"

@protocol DTACommonViewInput <DTAAlertProtocol>

- (void)setBadgeCount:(NSInteger)badgeCount;

@end

