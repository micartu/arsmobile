//
//  DTABaseViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTAThemeable.h"
#import "DTAAlertProtocol.h"

@class DTACartButton;
@protocol DTARootModuleInput;
@protocol DTAFabricAlertProtocol;

void runOnMainThread(void (^block)(void));

@interface DTABaseViewController : UIViewController <DTAThemeable, DTAAlertProtocol>

@property (assign, nonatomic) BOOL rootMenu;
@property (strong, nonatomic) id<DTARootModuleInput> root;
@property (strong, nonatomic) id<DTAFabricAlertProtocol> alertFabric;
@property (nonatomic, strong) DTACartButton *cartButton;

- (void)setupInitialState;
- (void)addCartButtonWithAction:(void (^)(void))action;
- (void)setBadgeCount:(NSInteger)badgeCount;

@end
