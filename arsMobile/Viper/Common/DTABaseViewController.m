//
//  DTABaseViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTABaseViewController.h"
#import <STPopup/STPopup.h>
#import "DTARootModuleInput.h"
#import "DTAFabricAlertProtocol.h"
#import "DTACartButton.h"
#import "DTATheme.h"

@interface DTABaseViewController () {
    void (^_actionForCart)(void);
}
@end

@implementation DTABaseViewController

@synthesize theme;

#pragma mark - Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupInitialState {
    if (self.rootMenu) {
        UIBarButtonItem *menu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"]
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(leftMenuAction)];
        self.navigationItem.leftBarButtonItem = menu;
    }
}

- (void)addCartButtonWithAction:(void (^)(void))action {
    runOnMainThread(^{
        self.cartButton = [[DTACartButton alloc] init];
        _actionForCart = action;
        UIBarButtonItem *cartButton = [[UIBarButtonItem alloc] initWithCustomView:self.cartButton];
        self.navigationItem.rightBarButtonItem = cartButton;
        [self.cartButton addTarget:self action:@selector(cartButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    });
}

- (void)setBadgeCount:(NSInteger)badgeCount {
    [self.cartButton setBadgeCount:badgeCount];
}

#pragma mark - Methods of DTMThemeable

- (void)applyTheme {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.navigationController) {
            self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
            [self.navigationController.navigationBar setTintColor:self.theme.btnForegroundColor];
            [self.navigationController.navigationBar
             setTitleTextAttributes:@{NSForegroundColorAttributeName:self.theme.btnForegroundColor}];
            self.navigationController.navigationBar.translucent = NO;

            // STPopup style
            [STPopupNavigationBar appearance].tintColor = self.theme.btnForegroundColor;
        }
    });
}

#pragma mark - DTAAlertProtocol

- (void)showMessage:(NSString*)message
          withTitle:(NSString*)title
          andAction:(void(^)(UIAlertAction *action))handler {
    assert(self.alertFabric);
    UIAlertController *ac = [self.alertFabric acWithMessage:message andTitle:title andAction:handler];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)showErrorWithMessage:(NSString*)message {
    assert(self.alertFabric);
    UIAlertController *ac = [self.alertFabric acErrorWithMessage:message];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)showWarningWithMessage:(NSString*)message {
    assert(self.alertFabric);
    UIAlertController *ac = [self.alertFabric acWarningWithMessage:message];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)showYesNoDialogWithMessage:(NSString *)message
                      andYesAction:(void(^)(UIAlertAction *action))yesHandler
                       andNoAction:(void(^)(UIAlertAction *action))noHandler {
    assert(self.alertFabric);
    UIAlertController *ac = [self.alertFabric acYesNoDialogWithMessage:message
                                                          andYesAction:yesHandler
                                                           andNoAction:noHandler];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)inputDialogWithTitle:(NSString*)title
                 withMessage:(NSString*)message
                 andOkAction:(void(^)(UIAlertAction *action, UIAlertController *alertController))yesHandler {
    assert(self.alertFabric);
    UIAlertController *ac = [self.alertFabric acInputDialogWithTitle:title
                                                         withMessage:message
                                                         andOkAction:yesHandler];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)chooseDialogWithTitle:(NSString*)title
                  withMessage:(NSString*)message
             withActionTitles:(NSArray*)titles
                   andActions:(NSArray*)actions
                    andStyles:(NSArray*)styles {
    assert(self.alertFabric);
    UIAlertController *ac = [self.alertFabric acChooseDialogWithTitle:title
                                                          withMessage:message
                                                     withActionTitles:titles
                                                           andActions:actions
                                                            andStyles:styles];
    [self presentViewController:ac animated:YES completion:nil];
}

#pragma mark - Actions

- (void)leftMenuAction {
    [self.root showMenu:YES];
}

- (void)cartButtonPressed {
    if (_actionForCart)
        _actionForCart();
}

void runOnMainThread(void (^block)(void)) {
    if ([NSThread isMainThread])
        block();
    else
        dispatch_sync(dispatch_get_main_queue(), block);
}

@end
