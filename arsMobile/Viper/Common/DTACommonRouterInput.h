//
//  DTACommonRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTARootModuleInput.h"

@protocol DTAChangerDialogModuleOutput;

@protocol DTACommonRouterInput <NSObject>

- (void)openChangerDialogWithTitle:(NSString*)title
                    andButtonTitle:(NSString*)btitle
                       andRequests:(NSArray*)req
                          andWidth:(CGFloat)width
                         andHeight:(CGFloat)height
                 andOutputDelegate:(id<DTAChangerDialogModuleOutput>)output;
- (void)changeToMenu:(MenuSelected)menu;

@end
