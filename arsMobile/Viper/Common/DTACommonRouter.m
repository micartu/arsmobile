//
//  DTACommonRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACommonRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import <STPopup/STPopup.h>
#import "storyboard_constants.h"
#import "constants.h"
#import "DTAChangerDialogModuleInput.h"
#import "DTAChangerDialogModuleOutput.h"

@implementation DTACommonRouter

#pragma mark - Methods of DTACommonRouterInput

- (void)openChangerDialogWithTitle:(NSString*)title
                    andButtonTitle:(NSString*)btitle
                       andRequests:(NSArray*)req
                          andWidth:(CGFloat)width
                          andHeight:(CGFloat)height
                 andOutputDelegate:(id<DTAChangerDialogModuleOutput>)output {
    assert(self.storyboard != nil);
    NSString *const msgId = @"changerDialog";
    RamblerViperModuleFactory *factory = [[RamblerViperModuleFactory alloc] initWithStoryboard:self.storyboard
                                                                              andRestorationId:msgId];
    [[self.transitionHandler openModuleUsingFactory:factory
                                withTransitionBlock:^(id<RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler,
                                                      id<RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {
                                    UIViewController *vc = (id)destinationModuleTransitionHandler;
                                    vc.contentSizeInPopup = CGSizeMake(width, height);
                                    STPopupController *popup = [[STPopupController alloc] initWithRootViewController:vc];
                                    popup.containerView.layer.cornerRadius = kCornerRadius;
                                    [popup presentInViewController:(id)self.transitionHandler];
                                }]
     thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<DTAChangerDialogModuleInput> moduleInput) {
         [moduleInput configureModuleWithTitle:title andButtonTitle:btitle andChangeRequests:req];
         return output;
     }];
}

- (void)changeToMenu:(MenuSelected)menu {
    [self.root changeSelectedMenuTo:menu];
}

@end
