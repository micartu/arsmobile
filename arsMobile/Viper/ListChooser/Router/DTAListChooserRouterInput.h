//
//  DTAListChooserRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTAListChooserRouterInput <DTACommonRouterInput>

- (void)closeModule;

@end
