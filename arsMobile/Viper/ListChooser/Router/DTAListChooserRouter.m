//
//  DTAListChooserRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTAListChooserRouter

#pragma mark - Methods of DTAListChooserRouterInput

- (void)closeModule {
    [self.transitionHandler closeCurrentModule:YES];
}

@end
