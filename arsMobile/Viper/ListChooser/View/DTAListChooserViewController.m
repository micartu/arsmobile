//
//  DTAListChooserViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserViewController.h"

#import "DTAListChooserViewOutput.h"
#import "DTACornedButton.h"
#import "DTATheme.h"

static NSString *const kCellId = @"listChooserCell";

@interface DTAListChooserViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray<NSString*> *_records;
    NSInteger _selected;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet DTACornedButton *btnChooseItem;

@end

@implementation DTAListChooserViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];

    [self applyTheme];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTAListChooserViewInput

- (void)setupInitialState {
	// initial setup of the view
}

- (void)setWindowTitle:(NSString*)title {
    self.title = title;
}

- (void)setItems:(NSArray*)items {
    _records = items;
    [self.tableView reloadData];
}

- (void)setSelected:(NSInteger)selected {
    _selected = selected;
    [self.tableView reloadData];
}

#pragma mark - DTAThemeable

- (void)applyTheme {
    [super applyTheme];

    [self.btnChooseItem setTintColor:self.theme.btnFrontColor];
    self.btnChooseItem.backColor = self.theme.btnForegroundColor;
    self.btnChooseItem.frontColor = self.theme.btnForegroundColor;
    [self.btnChooseItem.titleLabel sizeToFit];
    self.tableView.tintColor = self.theme.btnForegroundColor;
}

#pragma mark - Table View source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _records.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.text = _records[indexPath.row];
    if (indexPath.row == _selected)
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    else
        [cell setAccessoryType:UITableViewCellAccessoryNone];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.output chosenItemChangedTo:indexPath.row];
}

#pragma mark - Actions

- (IBAction)chooseItemAction:(id)sender {
    [self.output chooseItem];
}

@end
