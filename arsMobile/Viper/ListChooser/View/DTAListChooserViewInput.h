//
//  DTAListChooserViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@protocol DTAListChooserViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setWindowTitle:(NSString*)title;
- (void)setItems:(NSArray*)items;
- (void)setSelected:(NSInteger)selected;

@end
