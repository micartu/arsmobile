//
//  DTAListChooserViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAListChooserViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)chosenItemChangedTo:(NSInteger)index;
- (void)chooseItem;

@end
