//
//  DTAListChooserViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTAListChooserViewInput.h"

@protocol DTAListChooserViewOutput;

@interface DTAListChooserViewController : DTABaseViewController <DTAListChooserViewInput>

@property (nonatomic, strong) id<DTAListChooserViewOutput> output;

@end
