//
//  DTAListChooserPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserPresenter.h"

#import "DTAListChooserViewInput.h"
#import "DTAListChooserInteractorInput.h"
#import "DTAListChooserRouterInput.h"
#import "DTAListChooserModuleOutput.h"

@implementation DTAListChooserPresenter {
    NSInteger _curSelected;
    NSArray *_items;
}

#pragma mark - Methods of DTAListChooserModuleInput

- (void)configureModuleWithItems:(NSArray*)items andSelected:(NSInteger)selected withTitle:(NSString*)title {
    if (selected < items.count)
        _curSelected = selected;
    _items = items;
    [self.view setWindowTitle:title];
}

#pragma mark - Methods of DTAListChooserViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.view setItems:_items];
    [self.view setSelected:_curSelected];
}

- (void)chosenItemChangedTo:(NSInteger)index {
    if (index < _items.count) {
        _curSelected = index;
        [self.view setSelected:index];
    }
}

- (void)chooseItem {
    [self.moduleOutput elementWasChosenWithIndex:_curSelected];
    [self.router closeModule];
}

#pragma mark - Methods of DTAListChooserInteractorOutput

@end
