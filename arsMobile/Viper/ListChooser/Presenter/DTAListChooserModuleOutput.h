//
//  DTAListChooserModuleOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11.01.18.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DTAListChooserModuleOutput <RamblerViperModuleOutput>

- (void)elementWasChosenWithIndex:(NSInteger)index;

@end
