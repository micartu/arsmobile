//
//  DTAListChooserPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserViewOutput.h"
#import "DTAListChooserInteractorOutput.h"
#import "DTAListChooserModuleInput.h"

@protocol DTAListChooserViewInput;
@protocol DTAListChooserInteractorInput;
@protocol DTAListChooserRouterInput;
@protocol DTAListChooserModuleOutput;

@interface DTAListChooserPresenter : NSObject <DTAListChooserModuleInput, DTAListChooserViewOutput, DTAListChooserInteractorOutput>

@property (nonatomic, weak) id<DTAListChooserViewInput> view;
@property (nonatomic, strong) id<DTAListChooserInteractorInput> interactor;
@property (nonatomic, strong) id<DTAListChooserRouterInput> router;
@property (nonatomic, weak) id<DTAListChooserModuleOutput> moduleOutput;

@end
