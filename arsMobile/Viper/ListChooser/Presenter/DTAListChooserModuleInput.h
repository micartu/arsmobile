//
//  DTAListChooserModuleInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DTAListChooserModuleInput <RamblerViperModuleInput>

/**
 @author Michael Artuerhof

 Method inializes the basic configuration of the module
 */
- (void)configureModuleWithItems:(NSArray*)items andSelected:(NSInteger)selected withTitle:(NSString*)title;

@end
