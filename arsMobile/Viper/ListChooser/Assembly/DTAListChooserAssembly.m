//
//  DTAListChooserAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserAssembly.h"

#import "DTAListChooserViewController.h"
#import "DTAListChooserInteractor.h"
#import "DTAListChooserPresenter.h"
#import "DTAListChooserRouter.h"
#import "DTAAlertAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTAListChooserAssembly

- (DTAListChooserViewController *)viewListChooser {
    return [TyphoonDefinition withClass:[DTAListChooserViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterListChooser]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterListChooser]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTAListChooserInteractor *)interactorListChooser {
    return [TyphoonDefinition withClass:[DTAListChooserInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterListChooser]];
                          }];
}

- (DTAListChooserPresenter *)presenterListChooser{
    return [TyphoonDefinition withClass:[DTAListChooserPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewListChooser]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorListChooser]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerListChooser]];
                          }];
}

- (DTAListChooserRouter *)routerListChooser{
    return [TyphoonDefinition withClass:[DTAListChooserRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewListChooser]];
                          }];
}

@end
