//
//  DTAListChooserInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAListChooserInteractorInput.h"

@protocol DTAListChooserInteractorOutput;

@interface DTAListChooserInteractor : NSObject <DTAListChooserInteractorInput>

@property (nonatomic, weak) id<DTAListChooserInteractorOutput> output;

@end
