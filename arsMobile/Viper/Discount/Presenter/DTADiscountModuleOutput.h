//
//  DTADiscountModuleOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.01.18.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DTADiscountModuleOutput <RamblerViperModuleOutput>

- (void)discountAmountApplied:(double)discount;

@end
