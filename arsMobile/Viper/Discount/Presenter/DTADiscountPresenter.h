//
//  DTADiscountPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountViewOutput.h"
#import "DTADiscountInteractorOutput.h"
#import "DTADiscountModuleInput.h"

@protocol DTADiscountViewInput;
@protocol DTADiscountInteractorInput;
@protocol DTADiscountRouterInput;
@protocol DTADiscountModuleOutput;

@interface DTADiscountPresenter : NSObject <DTADiscountModuleInput, DTADiscountViewOutput, DTADiscountInteractorOutput>

@property (nonatomic, weak) id<DTADiscountViewInput> view;
@property (nonatomic, strong) id<DTADiscountInteractorInput> interactor;
@property (nonatomic, strong) id<DTADiscountRouterInput> router;
@property (nonatomic, weak) id<DTADiscountModuleOutput> moduleOutput;

@end
