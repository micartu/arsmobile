//
//  DTADiscountPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountPresenter.h"

#import "DTADiscountViewInput.h"
#import "DTADiscountInteractorInput.h"
#import "DTADiscountRouterInput.h"
#import "DTADiscountModuleOutput.h"

@implementation DTADiscountPresenter {
    double _discount;
}

#pragma mark - Methods of DTADiscountModuleInput

- (void)configureModule {
    [self.view setItems:[self.interactor grabDiscountItems]];
}

#pragma mark - Methods of DTADiscountViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)itemSelectedWithIndex:(NSInteger)index {
    [self.interactor convertIntoDiscountIndexOfItem:index];
}

- (void)applyTouched {
    [self.moduleOutput discountAmountApplied:_discount];
    [self.router closeModule];
}

#pragma mark - Methods of DTADiscountInteractorOutput

- (void)discountPercentage:(double)discount {
    _discount = discount;
    [self.view showDiscount:discount * 100.0f];
}

- (void)openDialogForManualInputOfPercentage {
    __weak typeof(self) wself = self;
    [wself.view inputDialogWithTitle:@"Введите скидку вручную (0-100%)"
                         withMessage:@"Другая скидка"
                         andOkAction:^(UIAlertAction *action, UIAlertController *alertController) {
                             NSArray *textfields = alertController.textFields;
                             UITextField *ifield = textfields[0];
                             NSString *dnum = ifield.text;
                             NSScanner *scanner = [NSScanner scannerWithString:dnum];
                             double found_double;
                             if ([scanner scanDouble:&found_double]) {
                                 if (found_double > 0 && found_double < 100) {
                                     [wself discountPercentage:found_double / 100];
                                     return;
                                 }
                             }
                             [wself.view showErrorWithMessage:@"Неправильный ввод!"];
                         }];
}

@end
