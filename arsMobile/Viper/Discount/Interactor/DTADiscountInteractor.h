//
//  DTADiscountInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountInteractorInput.h"

@protocol DTADiscountInteractorOutput;

@interface DTADiscountInteractor : NSObject <DTADiscountInteractorInput>

@property (nonatomic, weak) id<DTADiscountInteractorOutput> output;

@end
