//
//  DTADiscountInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTADiscountInteractorInput <NSObject>

- (NSArray*)grabDiscountItems;
- (void)convertIntoDiscountIndexOfItem:(NSInteger)index;

@end
