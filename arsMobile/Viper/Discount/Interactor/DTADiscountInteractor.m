//
//  DTADiscountInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountInteractor.h"

#import "DTADiscountInteractorOutput.h"

@interface DTADiscountInteractor() {
    NSArray *_items;
}
@end

@implementation DTADiscountInteractor

#pragma mark - Methods of DTADiscountInteractorInput

- (NSArray*)grabDiscountItems {
    _items = @[@"0 %",
               @"5 %",
               @"10 %",
               @"15 %",
               @"20 %",
               @"Другая"];
    return _items;
}

- (void)convertIntoDiscountIndexOfItem:(NSInteger)index {
    if (index < _items.count - 1) {
        NSString *element = _items[index];
        NSArray<NSString*> *a = [element componentsSeparatedByString:@" "];
        if (a.count)
            [self.output discountPercentage:[a[0] doubleValue]/100.0f];
    }
    else
        [self.output openDialogForManualInputOfPercentage];
}

@end
