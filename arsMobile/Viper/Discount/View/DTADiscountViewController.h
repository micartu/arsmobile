//
//  DTADiscountViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTADiscountViewInput.h"

@protocol DTADiscountViewOutput;

@interface DTADiscountViewController : DTABaseViewController <DTADiscountViewInput>

@property (nonatomic, strong) id<DTADiscountViewOutput> output;

@end
