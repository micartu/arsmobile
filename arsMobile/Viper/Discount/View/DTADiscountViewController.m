//
//  DTADiscountViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountViewController.h"

#import "DTADiscountViewOutput.h"
#import "DTACornedButton.h"
#import "DTATheme.h"
#import "DTADiscountCell.h"
#import "constants.h"

static NSString *const kDiscountCell = @"discountCell";

@interface DTADiscountViewController () <UICollectionViewDataSource, UICollectionViewDelegate> {
    NSArray *_records;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DTACornedButton *btnApply;
@property (weak, nonatomic) IBOutlet UILabel *labelDiscount;
@property (strong, nonatomic) NSNumberFormatter *discountFormatter;

@end

@implementation DTADiscountViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];

    self.title = @"Скидка";

    self.discountFormatter = [[NSNumberFormatter alloc] init];
    self.discountFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    self.discountFormatter.currencySymbol = @"%";
    self.discountFormatter.maximumFractionDigits = 2;
    self.discountFormatter.minimumFractionDigits = 0;

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTADiscountViewInput

- (void)setupInitialState {
	// initial setup of the view
}

- (void)setItems:(NSArray*)items {
    _records = items;
    [self.collectionView reloadData];
}

- (void)showDiscount:(double)discount {
    self.labelDiscount.text = [self.discountFormatter stringFromNumber:[NSNumber numberWithDouble:discount]];
}

#pragma mark - DTAThemeable

- (void)applyTheme {
    [super applyTheme];

    [self.btnApply setTintColor:self.theme.btnFrontColor];
    self.btnApply.backColor = self.theme.btnForegroundColor;
    self.btnApply.frontColor = self.theme.btnForegroundColor;
    [self.btnApply.titleLabel sizeToFit];
}

#pragma mark - Methods of UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _records.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDiscountCell forIndexPath:indexPath];
    return cell;
}

#pragma mark - Methods of UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    DTADiscountCell *cat = (DTADiscountCell*)cell;
    NSString *title = _records[indexPath.row];
    cat.name.text = title;

    cell.contentView.layer.cornerRadius = kBorderRadius;
    cell.contentView.layer.borderWidth = kBorderWidth;
    cell.contentView.layer.borderColor = self.theme.btnForegroundColor.CGColor;
    cell.contentView.layer.masksToBounds = YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.output itemSelectedWithIndex:indexPath.row];
}

#pragma mark - Actions

- (IBAction)applyButtonAction:(id)sender {
    [self.output applyTouched];
}

@end
