//
//  DTADiscountCell.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.01.18.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTADiscountCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;

@end
