//
//  DTADiscountViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@protocol DTADiscountViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setItems:(NSArray*)items;
- (void)showDiscount:(double)discount;

@end
