//
//  DTADiscountAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountAssembly.h"

#import "DTADiscountViewController.h"
#import "DTADiscountInteractor.h"
#import "DTADiscountPresenter.h"
#import "DTADiscountRouter.h"
#import "DTAAlertAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTADiscountAssembly

- (DTADiscountViewController *)viewDiscount {
    return [TyphoonDefinition withClass:[DTADiscountViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterDiscount]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterDiscount]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTADiscountInteractor *)interactorDiscount {
    return [TyphoonDefinition withClass:[DTADiscountInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterDiscount]];
                          }];
}

- (DTADiscountPresenter *)presenterDiscount{
    return [TyphoonDefinition withClass:[DTADiscountPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewDiscount]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorDiscount]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerDiscount]];
                          }];
}

- (DTADiscountRouter *)routerDiscount{
    return [TyphoonDefinition withClass:[DTADiscountRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewDiscount]];
                          }];
}

@end
