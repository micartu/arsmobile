//
//  DTADiscountAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

/**
 @author Michael Artuerhof

 Discount module
 */
@interface DTADiscountAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@end
