//
//  DTADiscountRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTADiscountRouterInput <DTACommonRouterInput>

- (void)closeModule;

@end
