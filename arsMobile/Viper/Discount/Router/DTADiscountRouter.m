//
//  DTADiscountRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTADiscountRouter

#pragma mark - Methods of DTADiscountRouterInput

- (void)closeModule {
    [self.transitionHandler closeCurrentModule:YES];
}

@end
