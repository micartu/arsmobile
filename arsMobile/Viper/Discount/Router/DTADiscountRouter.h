//
//  DTADiscountRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTADiscountRouterInput.h"
#import "DTACommonRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTADiscountRouter : DTACommonRouter <DTADiscountRouterInput>

@end
