//
//  DTAPaymentPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentViewOutput.h"
#import "DTAPaymentInteractorOutput.h"
#import "DTAPaymentModuleInput.h"

@protocol DTAPaymentViewInput;
@protocol DTAPaymentInteractorInput;
@protocol DTAPaymentRouterInput;

@interface DTAPaymentPresenter : NSObject <DTAPaymentModuleInput, DTAPaymentViewOutput, DTAPaymentInteractorOutput>

@property (nonatomic, weak) id<DTAPaymentViewInput> view;
@property (nonatomic, strong) id<DTAPaymentInteractorInput> interactor;
@property (nonatomic, strong) id<DTAPaymentRouterInput> router;

@end
