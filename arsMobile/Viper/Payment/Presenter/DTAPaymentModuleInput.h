//
//  DTAPaymentModuleInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DTAPaymentModuleInput <RamblerViperModuleInput>

/**
 @author Michael Artuerhof

 Method inializes the basic configuration of the module
 */
- (void)configureModuleWithPaymentInfo:(NSDictionary*)info;

@end
