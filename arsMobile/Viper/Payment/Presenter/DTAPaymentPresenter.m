//
//  DTAPaymentPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentPresenter.h"

#import "DTAPaymentViewInput.h"
#import "DTAPaymentInteractorInput.h"
#import "DTAPaymentRouterInput.h"
#import "DTACartItem.h"
#import "constants.h"
#import "DTAItem.h"

enum printerState {
    STATE_INACTIVE = 0,
    STATE_EMAIL_TELEPHONE,
    STATE_OPEN_CHEQUE,
    STATE_ADD_ITEMS,
    STATE_PAYMENT_CALC,
    STATE_CLOSE_CHEQUE,
    STATE_FINISHED,
    STATE_ERROR,
};

@interface DTAPaymentPresenter () {
    enum printerState _pstate;
    NSString *_op;
    NSString *_buyer;
    BOOL _isReturnCheque;
    BOOL _isCash;
    int _nCurItem2print;
    double _enteredSum;
    double _changeAmount;
    NSArray<DTACartItem*> *_items;
}

@end

@implementation DTAPaymentPresenter

#pragma mark - Methods of DTAPaymentModuleInput

- (void)configureModuleWithPaymentInfo:(NSDictionary*)info {
    NSNumber *payMethod = [info objectForKey:kPaymentMethodKey];
    NSNumber *returnCheque = [info objectForKey:kReturnChequeKey];
    NSNumber *discount = [info objectForKey:kDiscountOfItemsKey];
    double sum2pay = [[info objectForKey:kSumOfItemsKey] doubleValue];
    if (discount)
        sum2pay -= sum2pay * [discount doubleValue];
    _changeAmount = [[info objectForKey:kChangeAmount] doubleValue];
    _enteredSum = _changeAmount + sum2pay;
    _op = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsKeyOperatorName];
    _buyer = [info objectForKey:kSendAddressKey];
    if ([_buyer containsString:@"+"]) {
        // remove symbols which an ars fiscal printer doesn't like:
        _buyer = [_buyer stringByReplacingOccurrencesOfString:@"+" withString:@""];
        _buyer = [_buyer stringByReplacingOccurrencesOfString:@"-" withString:@""];
        _buyer = [_buyer stringByReplacingOccurrencesOfString:@"(" withString:@""];
        _buyer = [_buyer stringByReplacingOccurrencesOfString:@")" withString:@""];
        _buyer = [_buyer stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    if (returnCheque)
        _isReturnCheque = [returnCheque boolValue];
    else
        _isReturnCheque = NO;
    if (payMethod)
        _isCash = [payMethod boolValue];
    else
        _isCash = YES;
    [self.interactor fetchCartItemsWithCompletion:^(NSArray<DTACartItem*> *items) {
        _items = items;
        if (discount) {
            double d = [discount doubleValue] * 100;
            for (DTACartItem *i in _items)
                i.discountPercentage = d;
        }
    }];
}

#pragma mark - Methods of DTAPaymentViewOutput

- (void)didTriggerViewReadyEvent {
    _nCurItem2print = 0;
    _pstate = STATE_INACTIVE;
    if (_isCash)
        [self.view setWindowTitle:@"Оплата наличными"];
    else
        [self.view setWindowTitle:@"Оплата картой"];
    [self.view showCashAmount:_enteredSum - _changeAmount
              andChangeAmount:_changeAmount];
	[self.view setupInitialState];
    [self.view showStatus:@"Идет печать..."];
    [self printTaskFinishedWithError:nil];
}

- (void)tryAgainButtonPressed {
    [self didTriggerViewReadyEvent];
}

- (void)doneButtonPressed {
    if (_pstate == STATE_FINISHED) {
        __weak typeof(self) wself = self;
        [wself.interactor clearCartWithCompletion:^{
            [wself.router done];
        }];
    }
    else
        [self.router done];
}

#pragma mark - Methods of DTAPaymentInteractorOutput

- (void)printTaskFinishedWithError:(NSError*)err {
    if (err == nil) {
        [self move2nextState];
        if (_pstate != STATE_FINISHED)
            [self executeNextPrinterStep];
        else {
            [self.view showSuccess:@"Оплата успешна!"];
            [self.view showBackToCartButton];
        }
    }
    else {
        _pstate = STATE_ERROR;
        [self.view showFailed:err.userInfo[NSLocalizedDescriptionKey]];
        [self.view showTryAgainButton];
        [self.view showBackToCartButton];
    }
}

#pragma mark - State Machine of Printer

- (void)executeNextPrinterStep {
    NSLog(@"STEP: %ld", (long)_pstate); // TODO: delete me
    switch (_pstate) {
        case STATE_EMAIL_TELEPHONE:
            if (_buyer.length == 0) { // skip the step
                [self move2nextState];
                [self executeNextPrinterStep];
                return;
            }
            [self.interactor sendCopyToAddress:_buyer];
            break;

        case STATE_OPEN_CHEQUE:
            [self.interactor openChequeForOperator:_op
                                     withBuyerInfo:_buyer
                                    isReturnCheque:_isReturnCheque];
            break;

        case STATE_ADD_ITEMS:
            if (_nCurItem2print < _items.count) {
                [self.interactor printItem:_items[_nCurItem2print]];
            }
            _nCurItem2print++;
            break;

        case STATE_PAYMENT_CALC:
            [self.interactor calculatePaymentOnPrinterWithSum:_enteredSum isCash:_isCash];
            break;

        case STATE_CLOSE_CHEQUE:
            [self.interactor closeCheque];
            break;

        default:
            break;
    }
}

- (void)move2nextState {
    if (_pstate != STATE_ERROR && _pstate != STATE_ADD_ITEMS) {
        _pstate++;
    }
    else if (_pstate == STATE_ADD_ITEMS) {
        if (_nCurItem2print >= _items.count)
            _pstate++;
    }
}

@end
