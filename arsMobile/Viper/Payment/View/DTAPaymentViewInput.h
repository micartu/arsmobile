//
//  DTAPaymentViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAPaymentViewInput <NSObject>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setWindowTitle:(NSString*)title;
- (void)showCashAmount:(float)cashAmount
       andChangeAmount:(float)changeAmount;
- (void)showStatus:(NSString *)status;
- (void)showSuccess:(NSString *)status;
- (void)showFailed:(NSString *)status;

- (void)showTryAgainButton;
- (void)hideTryAgainButton;

- (void)showBackToCartButton;
- (void)hideBackToCartButton;

@end
