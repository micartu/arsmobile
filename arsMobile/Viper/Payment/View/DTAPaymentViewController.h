//
//  DTAPaymentViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTAPaymentViewInput.h"

@protocol DTAPaymentViewOutput;

@interface DTAPaymentViewController : DTABaseViewController <DTAPaymentViewInput>

@property (nonatomic, strong) id<DTAPaymentViewOutput> output;

@end
