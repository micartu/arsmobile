//
//  DTAPaymentViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAPaymentViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;

- (void)tryAgainButtonPressed;
- (void)doneButtonPressed;

@end
