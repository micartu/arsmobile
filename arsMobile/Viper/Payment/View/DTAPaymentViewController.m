//
//  DTAPaymentViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentViewController.h"

#import "DTAPaymentViewOutput.h"
#import "DTATheme.h"

static float kPaymentStatusLabelTopMarginWithImage = 102;

@interface DTAPaymentViewController()

@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusLabelTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *changeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIButton *tryAgainButton;
@property (weak, nonatomic) IBOutlet UIButton *backToCartButton;

@property (strong, nonatomic) NSNumberFormatter *priceFormatter;
@end

@implementation DTAPaymentViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];

    self.priceFormatter = [[NSNumberFormatter alloc] init];
    self.priceFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    self.priceFormatter.currencySymbol = @"₽";

    [self.navigationItem setHidesBackButton:YES animated:NO];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTAPaymentViewInput

- (void)setupInitialState {
    [self showStatus:@""];
    [self hideTryAgainButton];
    [self hideBackToCartButton];
}

- (void)setWindowTitle:(NSString*)title {
    self.title = title;
}

- (void)showCashAmount:(float)cashAmount andChangeAmount:(float)changeAmount {
    NSString *cashString = [self.priceFormatter stringFromNumber:[NSNumber numberWithFloat:cashAmount]];
    NSString *changeString = [self.priceFormatter stringFromNumber:[NSNumber numberWithFloat:changeAmount]];

    runOnMainThread(^{
        self.sumLabel.text = cashString;
        self.changeLabel.text = changeString;
    });
}

- (void)showStatus:(NSString *)status {
    runOnMainThread(^{

        if (self.statusLabelTopConstraint.constant != 0) {
            self.statusLabelTopConstraint.constant = 0;
            [self.view layoutIfNeeded];
        }

        self.statusImageView.alpha = 0;
        self.statusLabel.text = status;
        self.statusLabel.textColor = [UIColor blackColor];
    });
}

- (void)showSuccess:(NSString *)status {
    [self showStatus:status
           withImage:@"successBig"
        andTextColor:self.theme.btnFrontColor
    andAnimationTime:0.4];
}

- (void)showFailed:(NSString *)status {
    [self showStatus:status
           withImage:@"failedBig"
        andTextColor:self.theme.errColor
    andAnimationTime:0.2];
}

- (void)showTryAgainButton {
    runOnMainThread(^{
        self.tryAgainButton.enabled = YES;
        [UIView animateWithDuration:0.5 animations:^{
             self.tryAgainButton.alpha = 1.0;
         }];
    });
}

- (void)hideTryAgainButton {
    runOnMainThread(^{
        self.tryAgainButton.alpha = 0.0;
        self.tryAgainButton.enabled = NO;
    });
}

- (void)showBackToCartButton {
    runOnMainThread(^{
        self.backToCartButton.enabled = YES;
        [UIView animateWithDuration:0.5 animations:^{
             self.backToCartButton.alpha = 1.0;
         }];
    });
}

- (void)hideBackToCartButton {
    runOnMainThread(^{
        self.backToCartButton.alpha = 0.0;
        self.backToCartButton.enabled = NO;
    });
}

#pragma mark - Actions

- (IBAction)tryAgainAction:(id)sender {
    [self.output tryAgainButtonPressed];
}

- (IBAction)doneButtonAction:(id)sender {
    [self.output doneButtonPressed];
}

#pragma mark - Inner Methods

- (void)showStatus:(NSString *)status withImage:(NSString*)nameOfImage andTextColor:(UIColor*)color andAnimationTime:(CGFloat)time {
    runOnMainThread(^{
        [UIView animateWithDuration:time animations: ^{
            self.statusLabel.alpha = 0;
            self.statusImageView.alpha = 0;
        } completion:^(BOOL finished) {
            if (self.statusLabelTopConstraint.constant != kPaymentStatusLabelTopMarginWithImage) {
                self.statusLabelTopConstraint.constant = kPaymentStatusLabelTopMarginWithImage;
                [self.view layoutIfNeeded];
            }

            self.statusImageView.image = [UIImage imageNamed:nameOfImage];
            self.statusLabel.text = status;
            self.statusLabel.textColor = color;

            [UIView animateWithDuration:time animations:^{
                self.statusLabel.alpha = 1;
                self.statusImageView.alpha = 1;
            }];
        }];
    });
}

@end
