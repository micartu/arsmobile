//
//  DTAPaymentInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentInteractor.h"

#import "DTAPaymentInteractorOutput.h"
#import "DTAStorageProtocol.h"
#import "DTAFPrinterProtocol.h"

@implementation DTAPaymentInteractor

#pragma mark - Methods of DTAPaymentInteractorInput

- (void)openChequeForOperator:(NSString*)opName withBuyerInfo:(NSString*)buyer isReturnCheque:(BOOL)retCheque {
    __weak typeof(self) wself = self;
    [wself.printer openChequeForOperatorName:opName
                                    andBuyer:buyer
                              isReturnCheque:retCheque
                              withCompletion:^(BOOL success, NSError *er) {
                                  [wself.output printTaskFinishedWithError:er];
                             }];
}

- (void)printItem:(DTACartItem*)item {
    __weak typeof(self) wself = self;
    [wself.printer addItemToCheque:item withCompletion:^(BOOL success, NSError *er) {
        [wself.output printTaskFinishedWithError:er];
    }];
}

- (void)calculatePaymentOnPrinterWithSum:(double)sum isCash:(BOOL)isCash {
    __weak typeof(self) wself = self;
    [wself.printer payCheque:sum paymentByCash:isCash withCompletion:^(BOOL success, NSError *er) {
        [wself.output printTaskFinishedWithError:er];
    }];
}

- (void)closeCheque {
    __weak typeof(self) wself = self;
    [wself.printer closeChequeWithCompletion:^(BOOL success, NSError *er) {
        [wself.output printTaskFinishedWithError:er];
    }];
}

- (void)sendCopyToAddress:(NSString*)address {
    __weak typeof(self) wself = self;
    [wself.printer sendCopyOfChequeToAddress:address withCompletion:^(BOOL success, NSError *er) {
        [wself.output printTaskFinishedWithError:er];
    }];
}

- (void)fetchCartItemsWithCompletion:(void(^)(NSArray<DTACartItem*> *items))complete {
    [self.storage fetchAllCartItemsWithCompletion:complete];
}

- (void)clearCartWithCompletion:(void (^)(void))completion {
    [self.storage deleteAllCartItemsWithCompletion:completion];
}

@end
