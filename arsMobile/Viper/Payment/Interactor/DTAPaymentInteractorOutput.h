//
//  DTAPaymentInteractorOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAPaymentInteractorOutput <NSObject>

- (void)printTaskFinishedWithError:(NSError*)err;

@end
