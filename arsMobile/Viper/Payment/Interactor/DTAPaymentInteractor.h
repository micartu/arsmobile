//
//  DTAPaymentInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentInteractorInput.h"

@protocol DTAPaymentInteractorOutput;
@protocol DTAStorageProtocol;
@protocol DTAFPrinterProtocol;

@interface DTAPaymentInteractor : NSObject <DTAPaymentInteractorInput>

@property (nonatomic, weak) id<DTAPaymentInteractorOutput> output;
@property (nonatomic, strong) id<DTAStorageProtocol> storage;
@property (nonatomic, strong) id<DTAFPrinterProtocol> printer;

@end
