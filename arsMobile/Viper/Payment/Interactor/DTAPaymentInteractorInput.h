//
//  DTAPaymentInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTACartItem;

@protocol DTAPaymentInteractorInput <NSObject>

- (void)fetchCartItemsWithCompletion:(void(^)(NSArray<DTACartItem*> *items))complete;
- (void)openChequeForOperator:(NSString*)opName withBuyerInfo:(NSString*)buyer isReturnCheque:(BOOL)retCheque;
- (void)calculatePaymentOnPrinterWithSum:(double)sum isCash:(BOOL)isCash;
- (void)printItem:(DTACartItem*)item;
- (void)sendCopyToAddress:(NSString*)address;
- (void)closeCheque;
- (void)clearCartWithCompletion:(void (^)(void))completion;

@end
