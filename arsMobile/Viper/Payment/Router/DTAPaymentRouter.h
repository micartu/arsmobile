//
//  DTAPaymentRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentRouterInput.h"
#import "DTACommonRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTAPaymentRouter : DTACommonRouter <DTAPaymentRouterInput>

@end
