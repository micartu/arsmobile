//
//  DTAPaymentRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "storyboard_constants.h"

@implementation DTAPaymentRouter

#pragma mark - Methods of DTAPaymentRouterInput

- (void)done {
    [(id)self.transitionHandler performSegueWithIdentifier:kSegueUnwindToCart
                                                    sender:self.transitionHandler];
}

@end
