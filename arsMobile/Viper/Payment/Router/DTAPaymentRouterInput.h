//
//  DTAPaymentRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTAPaymentRouterInput <DTACommonRouterInput>

- (void)done;

@end
