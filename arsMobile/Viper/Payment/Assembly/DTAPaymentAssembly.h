//
//  DTAPaymentAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

/**
 @author Michael Artuerhof

 Payment module
 */
@interface DTAPaymentAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@end
