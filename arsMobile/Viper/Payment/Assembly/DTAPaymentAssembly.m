//
//  DTAPaymentAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 28/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAPaymentAssembly.h"

#import "DTAServiceAssembly.h"
#import "DTAPaymentViewController.h"
#import "DTAPaymentInteractor.h"
#import "DTAPaymentPresenter.h"
#import "DTAPaymentRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTAPaymentAssembly

- (DTAPaymentViewController *)viewPayment {
    return [TyphoonDefinition withClass:[DTAPaymentViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPayment]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterPayment]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                          }];
}

- (DTAPaymentInteractor *)interactorPayment {
    return [TyphoonDefinition withClass:[DTAPaymentInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPayment]];
                              [definition injectProperty:@selector(storage)
                                                    with:[self.servicesAssembly dataStorageService]];
                              [definition injectProperty:@selector(printer)
                                                    with:[self.servicesAssembly fiscalPrinterService]];
                          }];
}

- (DTAPaymentPresenter *)presenterPayment{
    return [TyphoonDefinition withClass:[DTAPaymentPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewPayment]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorPayment]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerPayment]];
                          }];
}

- (DTAPaymentRouter *)routerPayment{
    return [TyphoonDefinition withClass:[DTAPaymentRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewPayment]];
                          }];
}

@end
