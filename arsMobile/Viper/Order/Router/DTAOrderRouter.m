//
//  DTAOrderRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "storyboard_constants.h"
#import "DTAPaymentMethodModuleInput.h"
#import "DTADiscountModuleInput.h"
#import "DTADiscountModuleOutput.h"

@implementation DTAOrderRouter

#pragma mark - Methods of DTAOrderRouterInput

- (void)openPaymentMethodWithPaymentInfo:(NSDictionary*)info {
    [[self.transitionHandler openModuleUsingSegue:kSeguePaymentMethod] thenChainUsingBlock:
     ^id<RamblerViperModuleOutput>(id<DTAPaymentMethodModuleInput> moduleInput) {
         [moduleInput configureModuleWithPaymentInfo:info];
         return nil;
     }];
}

- (void)openDiscountWithDelegate:(id<DTADiscountModuleOutput>)output {
    [[self.transitionHandler openModuleUsingSegue:kSegueDiscount] thenChainUsingBlock:
     ^id<DTADiscountModuleOutput>(id<DTADiscountModuleInput> moduleInput) {
         [moduleInput configureModule];
         return output;
     }];
}

@end
