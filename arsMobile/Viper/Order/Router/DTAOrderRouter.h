//
//  DTAOrderRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderRouterInput.h"
#import "DTACommonRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTAOrderRouter : DTACommonRouter <DTAOrderRouterInput>

@end
