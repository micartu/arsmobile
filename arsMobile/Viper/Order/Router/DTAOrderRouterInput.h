//
//  DTAOrderRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@protocol DTADiscountModuleOutput;

@protocol DTAOrderRouterInput <DTACommonRouterInput>

- (void)openPaymentMethodWithPaymentInfo:(NSDictionary*)info;
- (void)openDiscountWithDelegate:(id<DTADiscountModuleOutput>)output;

@end
