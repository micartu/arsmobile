//
//  DTAOrderPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderPresenter.h"

#import "constants.h"
#import "DTAOrderViewInput.h"
#import "DTAOrderInteractorInput.h"
#import "DTAOrderRouterInput.h"
#import "DTADiscountModuleOutput.h"
#import "DTACartItem.h"
#import "DTAItem.h"

@interface DTAOrderPresenter() <DTADiscountModuleOutput> {
    double _sum;
    double _discount;
    BOOL _retCheq;
    NSString *_address;
}
@end

@implementation DTAOrderPresenter

#pragma mark - Methods of DTAOrderModuleInput

- (void)configureModule {
    _address = @"";
}

#pragma mark - Methods of DTADiscountViewOutput

- (void)discountAmountApplied:(double)discount {
    _discount = discount;
    [self.view setDiscount:_discount];
}

#pragma mark - Methods of DTAOrderViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    __weak typeof(self) wself = self;
    [wself.interactor fetchCartItemsWithCompletion:^(NSArray<DTACartItem*> *items) {
        double sum = 0;
        for (DTACartItem *i in items) {
            sum += i.count * i.item.price;
        }
        _sum = sum;
        [wself.view setItems:items];
        [wself.view setSum:sum];
        [wself.view setDiscount:0];
    }];
}

- (void)tagButtonPressed {
    [self.router openDiscountWithDelegate:self];
}

- (void)continueButtonPressed {
    NSDictionary *paymentInfo = @{ kSumOfItemsKey : [NSNumber numberWithDouble:_sum],
                                   kReturnChequeKey : [NSNumber numberWithBool:_retCheq],
                                   kSendAddressKey : _address,
                                   kDiscountOfItemsKey: [NSNumber numberWithDouble:_discount]};
    [self.router openPaymentMethodWithPaymentInfo:paymentInfo];
}

- (void)didSelectAddressForSendingReceipt:(NSString *)address {
    _address = address;
}

- (void)returnCheque:(BOOL)retCheq {
    _retCheq = retCheq;
}

#pragma mark - Methods of DTAOrderInteractorOutput

@end
