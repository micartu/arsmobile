//
//  DTAOrderPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderViewOutput.h"
#import "DTAOrderInteractorOutput.h"
#import "DTAOrderModuleInput.h"

@protocol DTAOrderViewInput;
@protocol DTAOrderInteractorInput;
@protocol DTAOrderRouterInput;

@interface DTAOrderPresenter : NSObject <DTAOrderModuleInput, DTAOrderViewOutput, DTAOrderInteractorOutput>

@property (nonatomic, weak) id<DTAOrderViewInput> view;
@property (nonatomic, strong) id<DTAOrderInteractorInput> interactor;
@property (nonatomic, strong) id<DTAOrderRouterInput> router;

@end
