//
//  DTAOrderInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderInteractorInput.h"

@protocol DTAOrderInteractorOutput;
@protocol DTAStorageProtocol;

@interface DTAOrderInteractor : NSObject <DTAOrderInteractorInput>

@property (nonatomic, weak) id<DTAOrderInteractorOutput> output;
@property (nonatomic, weak) id<DTAStorageProtocol> storage;

@end
