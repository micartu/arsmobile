//
//  DTAOrderInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderInteractor.h"

#import "DTAOrderInteractorOutput.h"
#import "DTAStorageProtocol.h"

@implementation DTAOrderInteractor

#pragma mark - Methods of DTAOrderInteractorInput

- (void)fetchCartItemsWithCompletion:(void(^)(NSArray<DTACartItem*> *items))complete {
    [self.storage fetchAllCartItemsWithCompletion:complete];
}

@end
