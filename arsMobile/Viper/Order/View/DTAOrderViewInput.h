//
//  DTAOrderViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@class DTACartItem;

@protocol DTAOrderViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setItems:(NSArray<DTACartItem*>*)items;
- (void)setSum:(double)sum;
- (void)setDiscount:(double)discount;

@end
