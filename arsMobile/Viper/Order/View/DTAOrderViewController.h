//
//  DTAOrderViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTAOrderViewInput.h"

@protocol DTAOrderViewOutput;

@interface DTAOrderViewController : DTABaseViewController <DTAOrderViewInput>

@property (nonatomic, strong) id<DTAOrderViewOutput> output;

@end
