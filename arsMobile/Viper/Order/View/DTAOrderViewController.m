//
//  DTAOrderViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderViewController.h"

#import "DTAOrderViewOutput.h"
#import "DTAItem.h"
#import "DTACategory.h"
#import "DTACartItem.h"
#import "DTACartCell.h"
#import <SHSPhoneLibrary.h>

static NSString *const kRowCell = @"cartCell";
static const float kRowHeigth = 87;
static const float kOrderBottomTextFieldsHeigth = 76;

typedef NS_ENUM(NSUInteger, DTASelectedOrderSendReceiptButton) {
    DTASelectedOrderSendReceiptButtonNone,
    DTASelectedOrderSendReceiptButtonPhone,
    DTASelectedOrderSendReceiptButtonEmail
};

@interface DTAOrderViewController() <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate> {
    double _sum;
    double _discount;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayoutConstraint;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;

@property (strong, nonatomic) NSNumberFormatter *priceFormatter;
@property (strong, nonatomic) NSNumberFormatter *countFormatter;
@property (strong, nonatomic) NSArray<DTACartItem*> *positions;

@property (assign, nonatomic) DTASelectedOrderSendReceiptButton selectedRecieptButton;
@property (weak, nonatomic) IBOutlet UIButton *phoneSelectorButton;
@property (weak, nonatomic) IBOutlet UIButton *emailSelectorButton;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end


@implementation DTAOrderViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];

    self.title = @"Заказ";

    [self setCheck:NO forButton:self.phoneSelectorButton];
    [self setCheck:NO forButton:self.emailSelectorButton];

    self.phoneTextField.placeholder = @"+7 (123) 456-78-90";
    [self.phoneTextField.formatter setDefaultOutputPattern:@"+# (###) ###-##-##"];
    self.phoneTextField.delegate = self;
    self.emailTextField.delegate = self;

    self.positions = [[NSMutableArray alloc] init];
    self.priceFormatter = [[NSNumberFormatter alloc] init];
    self.priceFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    self.priceFormatter.currencySymbol = @"₽";
    self.priceFormatter.maximumFractionDigits = 2;
    self.priceFormatter.minimumFractionDigits = 0;

    self.countFormatter = [[NSNumberFormatter alloc] init];
    self.countFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.countFormatter.currencySymbol = @"";
    self.countFormatter.maximumFractionDigits = 2;
    self.countFormatter.minimumFractionDigits = 0;

    UIBarButtonItem *rmenu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"tag"]
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(tagButtonPressed)];
    self.navigationItem.rightBarButtonItem = rmenu;

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTAOrderViewInput

- (void)setupInitialState {
	// initial setup of the view
    [self hideBottomView:YES withAnimation:NO];
}

- (void)setItems:(NSArray<DTACartItem*>*)items {
    runOnMainThread(^{
        self.positions = items;
        [self.tableView reloadData];
    });
}

- (void)setSum:(double)sum {
    runOnMainThread(^{
        _sum = sum;
        self.sumLabel.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithDouble:sum]];
    });
}

- (void)setDiscount:(double)discount {
    runOnMainThread(^{
        _discount = discount * _sum;
        self.discountLabel.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithDouble:_discount]];
        double totalSum = _sum - _discount;
        self.totalLabel.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithDouble:totalSum]];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.positions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DTACartCell *cell = [tableView dequeueReusableCellWithIdentifier:kRowCell forIndexPath:indexPath];
    DTACartItem *orderPosition = self.positions[indexPath.row];
    DTAItem *item = orderPosition.item;

    cell.title.text = item.title;
    cell.category.text = item.category.title;
    cell.price.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithFloat:item.price]];
    cell.totalPrice.text = [self.priceFormatter stringFromNumber:[NSNumber numberWithFloat:item.price * orderPosition.count]];
    NSString *measureUnit = [NSString stringWithFormat:@"%@", item.measureUnit.length > 0 ?
                             [NSString stringWithFormat:@" %@", item.measureUnit] :
                             @"x"];
    cell.count.text = [NSString stringWithFormat:@"%@%@", [self stringFromCount:orderPosition.count], measureUnit];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRowHeigth;
}

#pragma mark - UITableViewDelegate

#pragma mark - Actions

- (IBAction)continueAction:(id)sender {
    [self.output continueButtonPressed];
}

- (void)tagButtonPressed {
    [self.output tagButtonPressed];
}

- (IBAction)returnPaymentToggled:(id)sender {
    [self.output returnCheque:[sender isOn]];
}

- (IBAction)informUserToggled:(id)sender {
    if ([sender isOn])
        [self hideBottomView:NO withAnimation:YES];
    else
        [self hideBottomView:YES withAnimation:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self textFieldEditingChanged:textField finishedEditing:YES];
    return YES;
}

- (void)textFieldEditingChanged:(UITextField *)textField finishedEditing:(BOOL)finished {
    if ([textField isEqual:self.phoneTextField]) {
        if ([self isValidPhone:textField.text]) {
            self.selectedRecieptButton = DTASelectedOrderSendReceiptButtonPhone;
            [self notifyAboutSelectedSendRecieptType];
        }
        else if (textField.text.length > 0) {
            if (finished)
                [self showWarningWithMessage:@"Введён некорректный номер телефона"];

            if (self.selectedRecieptButton == DTASelectedOrderSendReceiptButtonPhone)
                self.selectedRecieptButton = DTASelectedOrderSendReceiptButtonNone;

            [self notifyAboutSelectedSendRecieptType];
        }
    }
    else if ([textField isEqual:self.emailTextField]) {
        if ([self isValidEmail:textField.text]) {
            self.selectedRecieptButton = DTASelectedOrderSendReceiptButtonEmail;
            [self notifyAboutSelectedSendRecieptType];
        }
        else if (textField.text.length > 0) {
            if (finished)
                [self showWarningWithMessage:@"Введён некорректный E-Mail адрес"];

            if (self.selectedRecieptButton == DTASelectedOrderSendReceiptButtonEmail)
                self.selectedRecieptButton = DTASelectedOrderSendReceiptButtonNone;

            [self notifyAboutSelectedSendRecieptType];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self textFieldEditingChanged:textField finishedEditing:NO];
    });
    return YES;
}

#pragma mark - Private methods

- (NSString*)stringFromCount:(float)count {
    return [self.countFormatter stringFromNumber:[NSNumber numberWithFloat:count]];
}

- (void)hideBottomView:(BOOL)hide withAnimation:(BOOL)animated {
    self.bottomLayoutConstraint.constant =  hide ? kOrderBottomTextFieldsHeigth : 0;
    if (animated) {
        [UIView animateWithDuration:0.4 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    else
        [self.view layoutIfNeeded];
}

- (void)setCheck:(BOOL)checked forButton:(UIButton*)button {
    UIImage *im =[UIImage imageNamed:checked ? @"checked" : @"unchecked"];
    [button setImage:im forState:UIControlStateNormal];
    [button setImage:im forState:UIControlStateHighlighted];
}

- (void)notifyAboutSelectedSendRecieptType {
    if (self.selectedRecieptButton == DTASelectedOrderSendReceiptButtonPhone) {
        [self.output didSelectAddressForSendingReceipt:self.phoneTextField.text];

        [self setCheck:YES forButton:self.phoneSelectorButton];
        [self setCheck:NO forButton:self.emailSelectorButton];
    }
    else if (self.selectedRecieptButton == DTASelectedOrderSendReceiptButtonEmail) {
        [self.output didSelectAddressForSendingReceipt:self.emailTextField.text];

        [self setCheck:NO forButton:self.phoneSelectorButton];
        [self setCheck:YES forButton:self.emailSelectorButton];
    }
    else {
        [self.output didSelectAddressForSendingReceipt:nil];

        [self setCheck:NO forButton:self.phoneSelectorButton];
        [self setCheck:NO forButton:self.emailSelectorButton];
    }
}

- (BOOL)isValidPhone:(NSString *)checkString {
    return checkString.length == 18 || checkString.length == 17;
}

- (BOOL)isValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
