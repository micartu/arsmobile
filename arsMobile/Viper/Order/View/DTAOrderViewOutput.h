//
//  DTAOrderViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAOrderViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)continueButtonPressed;
- (void)tagButtonPressed;
- (void)didSelectAddressForSendingReceipt:(NSString *)address;
- (void)returnCheque:(BOOL)retCheq;

@end
