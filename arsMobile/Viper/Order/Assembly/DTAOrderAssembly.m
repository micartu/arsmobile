//
//  DTAOrderAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 05/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAOrderAssembly.h"

#import "DTAServiceAssembly.h"
#import "DTAOrderViewController.h"
#import "DTAOrderInteractor.h"
#import "DTAOrderPresenter.h"
#import "DTAOrderRouter.h"
#import "DTAAlertAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTAOrderAssembly

- (DTAOrderViewController *)viewOrder {
    return [TyphoonDefinition withClass:[DTAOrderViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterOrder]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterOrder]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTAOrderInteractor *)interactorOrder {
    return [TyphoonDefinition withClass:[DTAOrderInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterOrder]];
                              [definition injectProperty:@selector(storage)
                                                    with:[self.servicesAssembly dataStorageService]];
                          }];
}

- (DTAOrderPresenter *)presenterOrder{
    return [TyphoonDefinition withClass:[DTAOrderPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewOrder]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorOrder]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerOrder]];
                          }];
}

- (DTAOrderRouter *)routerOrder{
    return [TyphoonDefinition withClass:[DTAOrderRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewOrder]];
                          }];
}

@end
