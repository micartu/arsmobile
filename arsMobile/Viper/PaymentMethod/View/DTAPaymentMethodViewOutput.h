//
//  DTAPaymentMethodViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAPaymentMethodViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)payButtonTouched;
- (void)noChangeButtonTouched;
- (void)resetButtonTouched;
- (void)itemSelectedWithIndex:(NSInteger)index;
- (void)sumEntered:(NSString*)sum;
- (void)paymentSourceChanged:(BOOL)byCard;

@end
