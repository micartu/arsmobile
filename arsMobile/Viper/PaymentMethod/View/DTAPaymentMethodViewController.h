//
//  DTAPaymentMethodViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTAPaymentMethodViewInput.h"

@protocol DTAPaymentMethodViewOutput;

@interface DTAPaymentMethodViewController : DTABaseViewController <DTAPaymentMethodViewInput>

@property (nonatomic, strong) id<DTAPaymentMethodViewOutput> output;

@end
