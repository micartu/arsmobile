//
//  DTAPaymentMethodViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodViewController.h"

#import "DTAPaymentMethodViewOutput.h"
#import "DTADiscountCell.h"
#import "DTACornedButton.h"
#import "DTATheme.h"
#import "constants.h"

static NSString *const kDiscountCell = @"discountCell";

@interface DTAPaymentMethodViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate> {
    NSArray *_records;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DTACornedButton *btnPay;
@property (weak, nonatomic) IBOutlet UIView *viewInput;
@property (weak, nonatomic) IBOutlet UIView *viewChange;
@property (weak, nonatomic) IBOutlet UILabel *labelSum;
@property (weak, nonatomic) IBOutlet UILabel *labelChange;
@property (weak, nonatomic) IBOutlet UITextField *textSumEntered;
@property (strong, nonatomic) NSNumberFormatter *moneyFormatter;

@end

@implementation DTAPaymentMethodViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
	[super viewDidLoad];

    self.moneyFormatter = [[NSNumberFormatter alloc] init];
    self.moneyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    self.moneyFormatter.currencySymbol = @"₽";
    self.moneyFormatter.maximumFractionDigits = 2;
    self.moneyFormatter.minimumFractionDigits = 0;

    [self applyTheme];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTAPaymentMethodViewInput

- (void)setupInitialState {
	// initial setup of the view
}

- (void)setWindowTitle:(NSString*)title {
    self.title = title;
}

- (void)enablePayButton:(BOOL)enabled {
    [self.btnPay setEnabled:enabled];
}

- (void)setPayItems:(NSArray*)items {
    _records = items;
    [self.collectionView reloadData];
}

- (void)showSumToPay:(NSNumber*)sum {
    self.labelSum.text = [self.moneyFormatter stringFromNumber:sum];
}

- (void)showChange:(NSNumber*)change {
    self.labelChange.text = [self.moneyFormatter stringFromNumber:change];
}

- (void)changeEnteredSum:(NSString*)sum {
    self.textSumEntered.text = sum;
}

- (void)hideInputElements:(BOOL)hide {
    [UIView animateWithDuration:0.4 animations:^{
        CGFloat alfa = hide ? 0 : 1;
        self.viewInput.alpha = alfa;
        self.viewChange.alpha = alfa;
        self.collectionView.alpha = alfa;
    }];
}

#pragma mark - DTAThemeable

- (void)applyTheme {
    [super applyTheme];

    [self.btnPay setTintColor:self.theme.btnFrontColor];
    self.btnPay.backColor = self.theme.btnForegroundColor;
    self.btnPay.frontColor = self.theme.btnForegroundColor;
    self.btnPay.disabledBackColor = self.theme.btnDisabledColor;
    self.btnPay.disabledFrontColor = self.theme.btnDisabledColor;
    [self.btnPay.titleLabel sizeToFit];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self textFieldDidEndEditing:textField];
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.output sumEntered:textField.text];
}

#pragma mark - Methods of UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _records.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDiscountCell forIndexPath:indexPath];
    return cell;
}

#pragma mark - Methods of UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    DTADiscountCell *cat = (DTADiscountCell*)cell;
    NSString *title = _records[indexPath.row];
    cat.name.text = title;

    cell.contentView.layer.cornerRadius = kBorderRadius;
    cell.contentView.layer.borderWidth = kBorderWidth;
    cell.contentView.layer.borderColor = self.theme.btnForegroundColor.CGColor;
    cell.contentView.layer.masksToBounds = YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.output itemSelectedWithIndex:indexPath.row];
}

#pragma mark - Actions

- (IBAction)payButtonAction:(id)sender {
    [self.output payButtonTouched];
}

- (IBAction)noChangeButtonAction:(id)sender {
    [self.output noChangeButtonTouched];
}

- (IBAction)resetButtonAction:(id)sender {
    [self.output resetButtonTouched];
}

- (IBAction)paymentMethodChangedAction:(id)sender {
    [self.output paymentSourceChanged:[sender isOn]];
}

@end
