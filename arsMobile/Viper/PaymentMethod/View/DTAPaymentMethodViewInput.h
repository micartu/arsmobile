//
//  DTAPaymentMethodViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@protocol DTAPaymentMethodViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;
- (void)setWindowTitle:(NSString*)title;
- (void)enablePayButton:(BOOL)enabled;
- (void)setPayItems:(NSArray*)items;
- (void)showSumToPay:(NSNumber*)sum;
- (void)showChange:(NSNumber*)change;
- (void)changeEnteredSum:(NSString*)sum;
- (void)hideInputElements:(BOOL)hide;

@end
