//
//  DTAPaymentMethodInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodInteractor.h"

#import "DTAPaymentMethodInteractorOutput.h"

@implementation DTAPaymentMethodInteractor {
    NSArray *_items;
}

#pragma mark - Methods of DTAPaymentMethodInteractorInput

- (NSArray*)grabPayItems {
    _items = @[@"50 ₽",
               @"100 ₽",
               @"500 ₽",
               @"1000 ₽",
               @"2000 ₽",
               @"5000 ₽"];
    return _items;
}

- (double)convertIntoMoneyIndexOfItem:(NSInteger)index {
    if (index < _items.count)
        return [_items[index] doubleValue];
    return 0;
}

@end
