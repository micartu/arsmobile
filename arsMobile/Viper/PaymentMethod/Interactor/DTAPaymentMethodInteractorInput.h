//
//  DTAPaymentMethodInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTAPaymentMethodInteractorInput <NSObject>

- (NSArray*)grabPayItems;
- (double)convertIntoMoneyIndexOfItem:(NSInteger)index;

@end
