//
//  DTAPaymentMethodInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodInteractorInput.h"

@protocol DTAPaymentMethodInteractorOutput;

@interface DTAPaymentMethodInteractor : NSObject <DTAPaymentMethodInteractorInput>

@property (nonatomic, weak) id<DTAPaymentMethodInteractorOutput> output;

@end
