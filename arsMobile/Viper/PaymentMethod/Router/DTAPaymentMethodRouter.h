//
//  DTAPaymentMethodRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodRouterInput.h"
#import "DTACommonRouter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface DTAPaymentMethodRouter : DTACommonRouter <DTAPaymentMethodRouterInput>

@end
