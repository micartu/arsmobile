//
//  DTAPaymentMethodRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "storyboard_constants.h"
#import "DTAPaymentModuleInput.h"

@implementation DTAPaymentMethodRouter

#pragma mark - Methods of DTAPaymentMethodRouterInput

- (void)openPaymentWithPaymentInfo:(NSDictionary*)info {
    [[self.transitionHandler openModuleUsingSegue:kSeguePayment] thenChainUsingBlock:
     ^id<RamblerViperModuleOutput>(id<DTAPaymentModuleInput> moduleInput) {
         [moduleInput configureModuleWithPaymentInfo:info];
         return nil;
     }];
}

@end
