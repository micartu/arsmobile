//
//  DTAPaymentMethodAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodAssembly.h"

#import "DTAPaymentMethodViewController.h"
#import "DTAPaymentMethodInteractor.h"
#import "DTAPaymentMethodPresenter.h"
#import "DTAPaymentMethodRouter.h"
#import "DTAAlertAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTAPaymentMethodAssembly

- (DTAPaymentMethodViewController *)viewPaymentMethod {
    return [TyphoonDefinition withClass:[DTAPaymentMethodViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPaymentMethod]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterPaymentMethod]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(alertFabric)
                                                    with:[self.alertsAssembly alertFactory]];
                          }];
}

- (DTAPaymentMethodInteractor *)interactorPaymentMethod {
    return [TyphoonDefinition withClass:[DTAPaymentMethodInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterPaymentMethod]];
                          }];
}

- (DTAPaymentMethodPresenter *)presenterPaymentMethod{
    return [TyphoonDefinition withClass:[DTAPaymentMethodPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewPaymentMethod]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorPaymentMethod]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerPaymentMethod]];
                          }];
}

- (DTAPaymentMethodRouter *)routerPaymentMethod{
    return [TyphoonDefinition withClass:[DTAPaymentMethodRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewPaymentMethod]];
                          }];
}

@end
