//
//  DTAPaymentMethodPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodViewOutput.h"
#import "DTAPaymentMethodInteractorOutput.h"
#import "DTAPaymentMethodModuleInput.h"

@protocol DTAPaymentMethodViewInput;
@protocol DTAPaymentMethodInteractorInput;
@protocol DTAPaymentMethodRouterInput;

@interface DTAPaymentMethodPresenter : NSObject <DTAPaymentMethodModuleInput, DTAPaymentMethodViewOutput, DTAPaymentMethodInteractorOutput>

@property (nonatomic, weak) id<DTAPaymentMethodViewInput> view;
@property (nonatomic, strong) id<DTAPaymentMethodInteractorInput> interactor;
@property (nonatomic, strong) id<DTAPaymentMethodRouterInput> router;

@end
