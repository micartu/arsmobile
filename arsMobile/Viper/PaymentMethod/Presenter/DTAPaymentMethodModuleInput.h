//
//  DTAPaymentMethodModuleInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol DTAPaymentMethodModuleInput <RamblerViperModuleInput>

/**
 @author Michael Artuerhof

 Method inializes the basic configuration of the module
 */
- (void)configureModuleWithPaymentInfo:(NSDictionary*)info;

@end
