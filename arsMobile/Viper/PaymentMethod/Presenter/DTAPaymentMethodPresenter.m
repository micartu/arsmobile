//
//  DTAPaymentMethodPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 10/01/2018.
//  Copyright © 2018 Dataphone. All rights reserved.
//

#import "DTAPaymentMethodPresenter.h"

#import "DTAPaymentMethodViewInput.h"
#import "DTAPaymentMethodInteractorInput.h"
#import "DTAPaymentMethodRouterInput.h"
#import "constants.h"

@implementation DTAPaymentMethodPresenter {
    NSMutableDictionary *_finfo;
    double _sum2pay;
    double _sumEntered;
}

#pragma mark - Methods of DTAPaymentMethodModuleInput

- (void)configureModuleWithPaymentInfo:(NSDictionary*)info {
    _finfo = [NSMutableDictionary dictionaryWithDictionary:info];
    double s = 0;
    NSNumber *sum = [_finfo objectForKey:kSumOfItemsKey];
    NSNumber *discount = [_finfo objectForKey:kDiscountOfItemsKey];
    if (sum)
        s = [sum doubleValue];
    if (discount)
        s -= s * [discount doubleValue];
    _sum2pay = s;
}

#pragma mark - Methods of DTAPaymentMethodViewOutput

- (void)didTriggerViewReadyEvent {
    __weak typeof(self) wself = self;
	[self.view setupInitialState];
    [wself.view setPayItems:[wself.interactor grabPayItems]];
    [self.view showSumToPay:[NSNumber numberWithDouble:_sum2pay]];
    [self.view enablePayButton:NO];
    [self windowTitleForCashPayment:YES];
}

- (void)sumEntered:(NSString*)sum {
    _sumEntered = [sum doubleValue];
    [self handleSumEntered];
}

- (void)payButtonTouched {
    [self.router openPaymentWithPaymentInfo:_finfo];
}

- (void)paymentSourceChanged:(BOOL)byCard {
    // in finfo byCash is default, so we have to reverse it:
    [_finfo setObject:[NSNumber numberWithBool:!byCard] forKey:kPaymentMethodKey];
    [self handlePaymentByCard:byCard];
    [self.view hideInputElements:byCard];
    if (!byCard) {
        [self.view showChange:[NSNumber numberWithDouble:_sumEntered >= _sum2pay ? _sumEntered - _sum2pay : 0]];
    }
    [self windowTitleForCashPayment:!byCard];
}

- (void)noChangeButtonTouched {
    _sumEntered = _sum2pay;
    [self.view changeEnteredSum:[@(_sumEntered) stringValue]];
    [self.view showChange:[NSNumber numberWithDouble:0]];
    [self handleSumEntered];
}

- (void)resetButtonTouched {
    _sumEntered = 0;
    [self.view changeEnteredSum:@""];
    [self handleSumEntered];
}

- (void)itemSelectedWithIndex:(NSInteger)index {
    double s = [self.interactor convertIntoMoneyIndexOfItem:index];
    _sumEntered += s;
    [self.view changeEnteredSum:[@(_sumEntered) stringValue]];
    [self handleSumEntered];
}

#pragma mark - Methods of DTAPaymentMethodInteractorOutput

#pragma mark - Private Methods

- (void)handleSumEntered {
    NSNumber *change = [NSNumber numberWithDouble:_sumEntered >= _sum2pay ? _sumEntered - _sum2pay : 0];
    [self.view showChange:change];
    [_finfo setObject:change forKey:kChangeAmount];
    if (_sumEntered >= _sum2pay)
        [self.view enablePayButton:YES];
    else
        [self.view enablePayButton:NO];
}

- (void)handlePaymentByCard:(BOOL)byCard {
    if (byCard) {
        [_finfo setObject:[NSNumber numberWithDouble:0] forKey:kChangeAmount];
        [self.view enablePayButton:YES];
    }
    else
        [self handleSumEntered];
}

- (void)windowTitleForCashPayment:(BOOL)byCash {
    [self.view setWindowTitle:byCash ? @"Оплата наличными" : @"Оплата картой"];
}

@end
