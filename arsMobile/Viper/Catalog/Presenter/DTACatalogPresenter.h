//
//  DTACatalogPresenter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogViewOutput.h"
#import "DTACatalogInteractorOutput.h"
#import "DTACatalogModuleInput.h"

@protocol DTACatalogViewInput;
@protocol DTACatalogInteractorInput;
@protocol DTACatalogRouterInput;

@interface DTACatalogPresenter : NSObject <DTACatalogModuleInput, DTACatalogViewOutput, DTACatalogInteractorOutput>

@property (nonatomic, weak) id<DTACatalogViewInput> view;
@property (nonatomic, strong) id<DTACatalogInteractorInput> interactor;
@property (nonatomic, strong) id<DTACatalogRouterInput> router;

@end
