//
//  DTACatalogPresenter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogPresenter.h"

#import "DTACatalogViewInput.h"
#import "DTACatalogInteractorInput.h"
#import "DTACatalogRouterInput.h"

@implementation DTACatalogPresenter

#pragma mark - Methods of DTACatalogModuleInput

- (void)configureModule {
    // starting configuration of the module
}

#pragma mark - Methods of DTACatalogViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.interactor fetchAllCategories];
    [self.view setBadgeCount:[self.interactor countOfItemsInCart]];
}

- (void)itemSelectedWithIndex:(NSInteger)index {
    __weak typeof (self) wself = self;
    [wself.router openCategoryBrowserForCategoryId:[wself.interactor idOfCategoryWithIndex:index]];
}

- (void)longPressOnItem:(NSUInteger)index {
    __weak typeof (self) wself = self;
    [wself.router showActionsDialogWithMessage:@"Выберите действие"
                               andUpdateAction:^(UIAlertAction *ac) {
                                   NSString *name = [wself.interactor nameOfCategoryWithIndex:index];
                                   [wself.router changeOrCreateCategoryWithName:name withHandler:^(NSString *categoryName) {
                                       if (categoryName.length) {
                                           [wself.interactor updateCategoryWithName:categoryName forIndex:index andCompletion:^{
                                               [wself.interactor fetchAllCategories];
                                           }];
                                       }
                                   }];
                               }
                                andDeleteAction:^(UIAlertAction *ac) {
                                    [wself.interactor deleteElementWithIndex:index andCompletion:^{
                                        [wself.interactor fetchAllCategories];
                                    }];
                                }];
}

- (void)createCategory {
    __weak typeof (self) wself = self;
    [wself.router changeOrCreateCategoryWithName:nil withHandler:^(NSString *categoryName) {
        if (categoryName.length) {
            [wself.interactor createCategoryWithName:categoryName andCompletion:^{
                [wself.interactor fetchAllCategories];
            }];
        }
    }];
}

- (void)moveToCart {
    [self.router changeToMenu:CartSelected];
}

#pragma mark - Methods of DTACatalogInteractorOutput

- (void)categoriesDelivered:(NSArray*)categories {
    [self.view setItems:categories];
}

@end
