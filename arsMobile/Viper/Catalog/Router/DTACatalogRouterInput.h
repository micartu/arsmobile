//
//  DTACatalogRouterInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonRouterInput.h"

@class UIAlertAction;

@protocol DTACatalogRouterInput <DTACommonRouterInput>

- (void)changeOrCreateCategoryWithName:(NSString*)change2name
                           withHandler:(void(^)(NSString *categoryName))handler;
- (void)showActionsDialogWithMessage:(NSString*)message
                     andUpdateAction:(void(^)(UIAlertAction *action))updateHandler
                     andDeleteAction:(void(^)(UIAlertAction *action))deleteHandler;
- (void)openCategoryBrowserForCategoryId:(NSInteger)cid;

@end
