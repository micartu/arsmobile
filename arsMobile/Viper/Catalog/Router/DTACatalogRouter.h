//
//  DTACatalogRouter.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogRouterInput.h"
#import "DTACommonRouter.h"

@interface DTACatalogRouter : DTACommonRouter <DTACatalogRouterInput>

@end
