//
//  DTACatalogRouter.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "storyboard_constants.h"
#import "constants.h"
#import "DTALeftMenuInputProtocol.h"
#import "DTACatBrowserModuleInput.h"
#import "DTAChangeRequest.h"
#import "DTAChangeResponse.h"
#import "DTAChangerDialogModuleInput.h"
#import "DTAChangerDialogModuleOutput.h"

@interface DTACatalogRouter () <DTAChangerDialogModuleOutput> {
    void(^_handler)(NSString *categoryName);
}
@end

@implementation DTACatalogRouter

#pragma mark - Methods of DTACatalogRouterInput

- (void)changeOrCreateCategoryWithName:(NSString*)change2name withHandler:(void(^)(NSString *categoryName))handler {
    _handler = handler;
    DTAChangeRequest *r = [DTAChangeRequest new];
    r.identifier = @"name";
    r.visibleTitle = @"Имя категории:";
    r.hintTitle = @"Введите имя категории";
    NSString *btitle;
    if (change2name.length > 0) {
        r.defaultValue = change2name;
        btitle = @"Изменить";
    }
    else {
        r.defaultValue = @"";
        btitle = @"Создать";
    }
    [self openChangerDialogWithTitle:@"Категория"
                      andButtonTitle:btitle
                         andRequests:@[r]
                            andWidth:kPopUpWidth
                           andHeight:kPopUpHeight
                   andOutputDelegate:self];
}

- (void)showActionsDialogWithMessage:(NSString*)message
                     andUpdateAction:(void(^)(UIAlertAction *action))updateHandler
                     andDeleteAction:(void(^)(UIAlertAction *action))deleteHandler {
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Внимание!"
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *updateAction = [UIAlertAction actionWithTitle:@"Изменить элемент" // change
                                                           style:UIAlertActionStyleDefault
                                                         handler:updateHandler];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Удалить элемент" // delete
                                                           style:UIAlertActionStyleDestructive
                                                         handler:deleteHandler];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" // cancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    [ac addAction:updateAction];
    [ac addAction:deleteAction];
    [ac addAction:cancelAction];
    [(id)self.transitionHandler presentViewController:ac animated:YES completion:nil];
}

- (void)openCategoryBrowserForCategoryId:(NSInteger)cid {
    [[self.transitionHandler openModuleUsingSegue:kSegueCatBrowser] thenChainUsingBlock:
     ^id<RamblerViperModuleOutput>(id<DTACatBrowserModuleInput> moduleInput) {
         [moduleInput configureModuleWithCategoryId:cid];
         return nil;
     }];
}

#pragma mark - DTAChangerDialogModuleOutput

- (void)changerResponses:(NSArray<DTAChangeResponse *>*)responses {
    DTAChangeResponse *r = responses.firstObject;
    if (_handler)
        _handler(r.changedValue);
}

@end
