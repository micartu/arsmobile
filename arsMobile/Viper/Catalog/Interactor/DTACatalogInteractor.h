//
//  DTACatalogInteractor.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogInteractorInput.h"

@protocol DTACatalogInteractorOutput;
@protocol DTAStorageProtocol;

@interface DTACatalogInteractor : NSObject <DTACatalogInteractorInput>

@property (nonatomic, weak) id<DTACatalogInteractorOutput> output;
@property (nonatomic, weak) id<DTAStorageProtocol> storage;

@end
