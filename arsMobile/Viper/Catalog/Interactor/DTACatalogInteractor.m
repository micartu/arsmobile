//
//  DTACatalogInteractor.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogInteractor.h"

#import "DTACatalogInteractorOutput.h"
#import "DTAStorageProtocol.h"
#import "DTACategory.h"

@implementation DTACatalogInteractor {
    NSArray<DTACategory*>* _items;
}

#pragma mark - Methods of DTACatalogInteractorInput

- (void)createCategoryWithName:(NSString*)name andCompletion:(void (^)(void))completion {
    [self.storage createCategoryWithName:name andCompletion:completion];
}

- (void)updateCategoryWithName:(NSString*)name forIndex:(NSInteger)index andCompletion:(void (^)(void))completion {
    if (index < _items.count) {
        DTACategory *cat = _items[index];
        cat.title = name;
        [self.storage modifyCategory:cat andCompletion:completion];
    }
}

- (void)fetchAllCategories {
    [self.storage fetchAllCategoriesWithCompletion:^(NSArray<DTACategory*>* items) {
        _items = items;
        [self.output categoriesDelivered:items];
    }];
}

- (void)deleteElementWithIndex:(NSUInteger)index andCompletion:(void (^)(void))completion {
    if (index < _items.count) {
        [self.storage deleteCategory:_items[index] andCompletion:completion];
    }
}

- (NSString*)nameOfCategoryWithIndex:(NSInteger)index {
    if (index < _items.count)
        return _items[index].title;
    return nil;
}

- (NSInteger)idOfCategoryWithIndex:(NSInteger)index {
    if (index < _items.count)
        return _items[index].cid;
    return -1;
}

- (NSInteger)countOfItemsInCart {
    return [self.storage countAllCartItems];
}

@end
