//
//  DTACatalogInteractorInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTACatalogInteractorInput <NSObject>

- (void)createCategoryWithName:(NSString*)name andCompletion:(void (^)(void))completion;
- (void)updateCategoryWithName:(NSString*)name forIndex:(NSInteger)index andCompletion:(void (^)(void))completion;
- (NSString*)nameOfCategoryWithIndex:(NSInteger)index;
- (NSInteger)idOfCategoryWithIndex:(NSInteger)index;
- (NSInteger)countOfItemsInCart;
- (void)deleteElementWithIndex:(NSUInteger)index andCompletion:(void (^)(void))completion;
- (void)fetchAllCategories;

@end
