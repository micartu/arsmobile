//
//  DTACatalogAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogAssembly.h"

#import "DTACatalogViewController.h"
#import "DTACatalogInteractor.h"
#import "DTACatalogPresenter.h"
#import "DTACatalogRouter.h"
#import "DTARootAssembly.h"
#import "DTAServiceAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation DTACatalogAssembly

- (DTACatalogViewController *)viewCatalog {
    return [TyphoonDefinition withClass:[DTACatalogViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCatalog]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCatalog]];
                              [definition injectProperty:@selector(theme)
                                                    with:[self.themeProvider currentTheme]];
                              [definition injectProperty:@selector(root)
                                                    with:[self.rootAssembly presenterRoot]];
                          }];
}

- (DTACatalogInteractor *)interactorCatalog {
    return [TyphoonDefinition withClass:[DTACatalogInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCatalog]];
                              [definition injectProperty:@selector(storage)
                                                    with:[self.servicesAssembly dataStorageService]];
                          }];
}

- (DTACatalogPresenter *)presenterCatalog{
    return [TyphoonDefinition withClass:[DTACatalogPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCatalog]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCatalog]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCatalog]];
                          }];
}

- (DTACatalogRouter *)routerCatalog{
    return [TyphoonDefinition withClass:[DTACatalogRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCatalog]];
                              [definition injectProperty:@selector(storyboard)
                                                    with:[self.appAssembly storyboard]];
                              [definition injectProperty:@selector(root)
                                                    with:[self.rootAssembly presenterRoot]];
                          }];
}

@end
