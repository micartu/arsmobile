//
//  DTACatalogAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "DTABaseModuleAssembly.h"

/**
 @author Michael Artuerhof

 Catalog module
 */
@interface DTACatalogAssembly : DTABaseModuleAssembly <RamblerInitialAssembly>

@end
