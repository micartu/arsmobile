//
//  DTACatalogViewController.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACatalogViewController.h"

#import "DTACatalogViewOutput.h"
#import "DTACatalogCell.h"
#import "DTACornedButton.h"
#import "DTACategory.h"
#import "constants.h"
#import "DTATheme.h"
#import "DTACartButton.h"

static NSString *const kCategoryItemCell = @"categoryItemCell";

@interface DTACatalogViewController () <UICollectionViewDataSource, UICollectionViewDelegate> {
    NSArray<DTACategory*> *_records;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *viewNoCategories;
@property (weak, nonatomic) IBOutlet DTACornedButton *btnCreateCategory;

@end

@implementation DTACatalogViewController

#pragma mark - Live cycle methods

- (void)viewDidLoad {
    self.rootMenu = YES;
	[super viewDidLoad];

    self.title = @"Каталог";
    [self setItems:nil];

    __weak typeof(self) wself = self;
    [wself addCartButtonWithAction:^{
        [wself.output moveToCart];
    }];

    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = kLongPressTimeSec;
    [self.collectionView addGestureRecognizer:lpgr];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.output didTriggerViewReadyEvent];
}

#pragma mark - Methods of DTACatalogViewInput

- (void)setupInitialState {
    [super setupInitialState];
    [self applyTheme];
    [self.cartButton setBadgeCount:0];
}

- (void)setItems:(NSArray*)items {
    _records = items;
    if (_records.count) {
        self.collectionView.hidden = NO;
        [self.collectionView reloadData];
        self.viewNoCategories.hidden = YES;
    }
    else {
        self.viewNoCategories.hidden = NO;
        self.collectionView.hidden = YES;
    }
}

#pragma mark - DTAThemeable

- (void)applyTheme {
    [super applyTheme];

    [self.btnCreateCategory setTintColor:self.theme.btnFrontColor];
    self.btnCreateCategory.backColor = self.theme.btnForegroundColor;
    self.btnCreateCategory.frontColor = self.theme.btnForegroundColor;
    [self.btnCreateCategory.titleLabel sizeToFit];
}

#pragma mark - Methods of UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _records.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCategoryItemCell forIndexPath:indexPath];
    return cell;
}

#pragma mark - Methods of UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    DTACatalogCell *cat = (DTACatalogCell*)cell;
    DTACategory *item = _records[indexPath.row];
    cat.name.text = item.title;
    cat.count.text = [@(item.count) stringValue];

    cell.contentView.layer.cornerRadius = kBorderRadius;
    cell.contentView.layer.borderWidth = kBorderWidth;
    cell.contentView.layer.borderColor = self.theme.btnForegroundColor.CGColor;
    cell.contentView.layer.masksToBounds = YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.output itemSelectedWithIndex:indexPath.row];
}

#pragma mark - Actions

- (IBAction)createCategoryAction:(id)sender {
    [self.output createCategory];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    if (indexPath && gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self.output longPressOnItem:indexPath.row];
    }
}

@end
