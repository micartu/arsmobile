//
//  DTACatalogViewInput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTACommonViewInput.h"

@protocol DTACatalogViewInput <DTACommonViewInput>

/**
 @author Michael Artuerhof

 Method initializes the state
 */
- (void)setupInitialState;

- (void)setItems:(NSArray*)items;

@end
