//
//  DTACatalogViewController.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTABaseViewController.h"

#import "DTACatalogViewInput.h"

@protocol DTACatalogViewOutput;

@interface DTACatalogViewController : DTABaseViewController <DTACatalogViewInput>

@property (nonatomic, strong) id<DTACatalogViewOutput> output;

@end
