//
//  DTACatalogViewOutput.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DTACatalogViewOutput <NSObject>

/**
 @author Michael Artuerhof

 Method says to the presenter what everything's ready for work
 */
- (void)didTriggerViewReadyEvent;
- (void)itemSelectedWithIndex:(NSInteger)index;
- (void)createCategory;
- (void)longPressOnItem:(NSUInteger)index;
- (void)moveToCart;

@end
