//
//  CartItem+CoreDataProperties.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import "CartItem+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CartItem (CoreDataProperties)

+ (NSFetchRequest<CartItem *> *)fetchRequest;

@property (nonatomic) int64_t cid;
@property (nonatomic) float count;
@property (nullable, nonatomic, retain) Item *item;

@end

NS_ASSUME_NONNULL_END
