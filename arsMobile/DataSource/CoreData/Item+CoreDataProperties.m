//
//  Item+CoreDataProperties.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import "Item+CoreDataProperties.h"

@implementation Item (CoreDataProperties)

+ (NSFetchRequest<Item *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Item"];
}

@dynamic iid;
@dynamic name;
@dynamic price;
@dynamic measureUnit;
@dynamic taxType;
@dynamic category;
@dynamic cart;

@end
