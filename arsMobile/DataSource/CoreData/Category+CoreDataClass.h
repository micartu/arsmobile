//
//  Category+CoreDataClass.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

NS_ASSUME_NONNULL_BEGIN

@interface Category : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Category+CoreDataProperties.h"
