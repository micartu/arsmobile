//
//  Item+CoreDataProperties.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import "Item+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Item (CoreDataProperties)

+ (NSFetchRequest<Item *> *)fetchRequest;

@property (nonatomic) int64_t iid;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSDecimalNumber *price;
@property (nullable, nonatomic, copy) NSString *measureUnit;
@property (nonatomic) int16_t taxType;
@property (nullable, nonatomic, retain) Category *category;
@property (nullable, nonatomic, retain) NSSet<CartItem *> *cart;

@end

@interface Item (CoreDataGeneratedAccessors)

- (void)addCartObject:(CartItem *)value;
- (void)removeCartObject:(CartItem *)value;
- (void)addCart:(NSSet<CartItem *> *)values;
- (void)removeCart:(NSSet<CartItem *> *)values;

@end

NS_ASSUME_NONNULL_END
