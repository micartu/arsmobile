//
//  CartItem+CoreDataProperties.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import "CartItem+CoreDataProperties.h"

@implementation CartItem (CoreDataProperties)

+ (NSFetchRequest<CartItem *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CartItem"];
}

@dynamic cid;
@dynamic count;
@dynamic item;

@end
