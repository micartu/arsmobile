//
//  CartItem+CoreDataClass.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

NS_ASSUME_NONNULL_BEGIN

@interface CartItem : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CartItem+CoreDataProperties.h"
