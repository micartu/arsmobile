//
//  Category+CoreDataProperties.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//
//

#import "Category+CoreDataProperties.h"

@implementation Category (CoreDataProperties)

+ (NSFetchRequest<Category *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Category"];
}

@dynamic cid;
@dynamic name;
@dynamic item;

@end
