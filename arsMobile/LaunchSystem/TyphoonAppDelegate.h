//
//  TyphoonAppDelegate.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TyphoonAppDelegate : UIResponder <UIApplicationDelegate>

@end
