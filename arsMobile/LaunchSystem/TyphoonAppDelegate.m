//
//  TyphoonAppDelegate.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "TyphoonAppDelegate.h"
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@implementation TyphoonAppDelegate

- (NSArray *)initialAssemblies {
    RamblerInitialAssemblyCollector *collector = [RamblerInitialAssemblyCollector new];
    return [collector collectInitialAssemblyClasses];
}

@end
