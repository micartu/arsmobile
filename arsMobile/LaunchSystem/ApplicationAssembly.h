//
//  ApplicationAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@class DTARootAssembly;

@interface ApplicationAssembly : TyphoonAssembly <RamblerInitialAssembly>

@property(nonatomic, strong, readonly) DTARootAssembly *rootAssembly;

- (UIStoryboard *)storyboard;

@end
