//
//  DTACoreDataAppDelegate.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACoreDataAppDelegate.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation DTACoreDataAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"arsMobile"];
    return YES;
}

@end
