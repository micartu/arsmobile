//
//  DTADefaultAppDelegate.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DTARootModuleInput;

@interface DTADefaultAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) id<DTARootModuleInput> rootMenu;

@end
