//
//  ApplicationAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "ApplicationAssembly.h"
#import <RamblerAppDelegateProxy/RamblerAppDelegateProxy.h>
#import "DTACoreDataAppDelegate.h"
#import "DTADefaultAppDelegate.h"
#import "DTARootAssembly.h"

@implementation ApplicationAssembly

- (RamblerAppDelegateProxy *)applicationDelegateProxy {
    return [TyphoonDefinition withClass:[RamblerAppDelegateProxy class]
                          configuration:^(TyphoonDefinition *definition){
                              [definition injectMethod:@selector(addAppDelegates:)
                                            parameters:^(TyphoonMethod *method) {
                                                NSArray *appDelegates = @[
                                                                          [self coreDataAppDelegate],
                                                                          [self defaultAppDelegate]
                                                                          ];
                                                [method injectParameterWith:appDelegates];
                                            }];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (DTACoreDataAppDelegate *)coreDataAppDelegate {
    return [TyphoonDefinition withClass:[DTACoreDataAppDelegate class]];
}

- (DTADefaultAppDelegate *)defaultAppDelegate {
    return [TyphoonDefinition withClass:[DTADefaultAppDelegate class] configuration:^(TyphoonDefinition *definition){
        [definition injectProperty:@selector(rootMenu)
                              with:[_rootAssembly presenterRoot]];
    }];
}

- (UIStoryboard *)storyboard {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                        parameters:^(TyphoonMethod *initializer) {
                            [initializer injectParameterWith:@"Main"];
                            [initializer injectParameterWith:self];
                            [initializer injectParameterWith:[NSBundle mainBundle]];
                        }];
    }];
}

@end
