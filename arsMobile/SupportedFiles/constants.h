//
//  constants.h
//  arsMobile
//
//  Created by Michael Artuerhof on 08/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

static NSString* const kLastUsedController = @"lastUsedController";
static NSString* const kDefaultsKeySelectedPrinterName = @"defaultsKeySelectedPrinterName";
static NSString* const kDefaultsKeyOperatorName = @"defaultsKeyOperatorName";

static NSString* const kArsMobileDomain = @"arsMobileDomain";

static NSString* const kDefaultKeyCountForCategory = @"CategoryCounter";
static NSString* const kDefaultKeyCountForItem = @"ItemCounter";
static NSString* const kDefaultKeyCountForCartItem = @"CartItemCounter";

static NSString* const kSumOfItemsKey = @"SumOfItemsKey";
static NSString* const kDiscountOfItemsKey = @"DiscountOfItemsKey";
static NSString* const kChangeAmount = @"ChangeAmountOfItemsKey";
static NSString* const kPaymentMethodKey = @"PaymentMethodKey";
static NSString* const kSendAddressKey = @"SendAddressKey";
static NSString* const kReturnChequeKey = @"ReturnChequeKey";

static const float kPopUpWidth = 280;
static const float kPopUpHeight = 200;
static const float kCornerRadius = 8;
static const float kLongPressTimeSec = 0.3;

static const float kBorderRadius = 8.0f;
static const float kBorderWidth = 2.0f;
