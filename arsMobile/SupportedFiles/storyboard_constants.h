//
//  storyboard_constants.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

static NSString* const kMenuVCId = @"leftMenuController";
static NSString* const kCartVCId = @"cartController";
static NSString* const kCatalogVCId = @"catalogController";
static NSString* const kSettingsVCId = @"settingsController";
static NSString* const kAboutVCId = @"aboutController";

static NSString* const kSegueCatBrowser = @"showCategoryContents";
static NSString* const kSeguePayment = @"paymentSegue";
static NSString* const kSeguePaymentMethod = @"paymentMethodSegue";
static NSString* const kSegueOrder = @"orderSegue";
static NSString* const kSegueUnwindToCart = @"unwindToCartSegue";
static NSString* const kSegueDiscount = @"discountSegue";
static NSString* const kSegueListChooser = @"listChooserSegue";

static NSString* const kChangeDeviceVC = @"changeDeviceVC";
static NSString* const kEnterTextVC = @"enterTextVC";
