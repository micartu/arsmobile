//
//  arsprinter_err_codes.h
//  arsMobile
//
//  Created by Michael Artuerhof on 11/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

static const int ERR_DEVICE_DAY_IS_OPENED = 110108;
static const int ERR_DEVICE_DAY_IS_CLOSED = 110109;
static const int ERR_R_OPEN_BON = 111015;
static const int ERR_R_PAY_NOT_STARTED = 111050;
static const int ERR_R_PAY_STARTED = 111018;
static const int ERR_R_AMOUNT_BIGGER_BILLAMOUNT = 111063;
static const int ERR_R_PAY_NOCASH = 111017;
