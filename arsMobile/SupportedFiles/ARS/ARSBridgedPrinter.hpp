//
//  ARSBridgedPrinter.hpp
//  ARSTest
//
//  Created by Michael Artuerhof on 24/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#ifndef ARSBridgedPrinter_hpp
#define ARSBridgedPrinter_hpp

#include <stdio.h>
#include <ars_printer/arsprinter.h>

using namespace arsp;

class ARSBridgedPrinter : public Printer
{
public:
    ARSBridgedPrinter(void* objc);
    ~ARSBridgedPrinter();

    virtual int write(const uint8_t* data, unsigned int len);

    virtual int read(uint8_t* buf, unsigned int buflen);

private:
    void* _class;
};

#endif /* ARSBridgedPrinter_hpp */
