//
//  ARSFReport.h
//  ARSTest
//
//  Created by Michael Artuerhof on 28/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ARSFReportProtocol <NSObject>

- (void)cameData:(uint8_t*)data withLen:(int)len;
- (void)cameCommand:(int)command withSEQ:(int)seq;

@end

@interface ARSFReport : NSObject

@property (nonatomic, weak) id<ARSFReportProtocol> delegate;

- (void)checkRTask:(void*)task;
- (void)rtaskWithData:(uint8_t*)data andLen:(int)len;
- (void)rtaskWithSEQ:(int)seq andCommand:(int)command;

@end
