#pragma once

namespace arsp {

	class rtask;

	/**
	 * an abstract class which represent the subscriber for printer's answers
	 *
	 * the descendant class must implement checkRTask method
	 */
	class Report
	{
	public:
		/**
		 * Method notifies the client about
		 * printer's answer
		 *
		 * @param ans answer of the printer
		 */
		virtual void checkRTask(rtask* ans) = 0;
		virtual ~Report() {};
	};
};
