#pragma once
#include <stdint.h>
#include <string>
#include "fstructs.h"

// based on document:
// FISCAL PRINTER. ars.mobile Ф.Programmer’s Manual
// ver. 1.19 (March, 2017)
namespace arsp {

	class ARSCommanderImpl;
	class Printer;
	class Report;

	/**
	 * Routes commands for fiscal printer, executes them and
	 * reads the answer(s) from printer back
	 */
	class ARSCommander
	{
		ARSCommanderImpl* _cmd;

	public:
		/**
		 * @param printer a link to printer = interface implementing
		 * read/write commands (almost posix style)
		 */
		ARSCommander(Printer& printer);
		~ARSCommander();

		/**
		 * returns current seq number used by the last packet generation
		 */
		uint8_t getSeq();

		/**
		 * removes all tasks for reading/writing (needed for tests)
		 */
		void removeAllTasks();

		/**
		 * Adds a subscriber for notifications from printer
		 *
		 * @param sub a subscriber which receives notifications
		 * in form of class rtask.
		 *
		 * @details rtask will be delivered to the subscriber twice:
		 *  	1) in the moment when the command to execution was received.
		 *  		In that case data field of the subscriber will be NULL
		 *  		you shouldn't do anything with the object (**do not delete** it)
		 *
		 *  	2) when the answer from the printer was received.
		 *  		In that case the data in rtask'll be not empty (with answer packet
		 *   		but without the command, seq, etc (they are provided through methods of rtask)
		 *   		only with ans string + 9 status bytes)
		 *   		after processing the object **MUST** be deleted with delete operator, because
		 *   		in that moment it's not in the processing queue and if the user won't delete it
		 *   		there would be a memory leak!:) Maybe I should process it the base class (Report)
		 *   		but now it's not the case...
		 */
		void addSubscriber(Report* sub);

		/**
		 * Removes subscriber
		 *
		 * @param sub a subscriber which receives notifications from printer
		 * 	should be called if the subscriber doesn't need notifications
		 * 	any more
		 */
		void removeSubscriber(Report* sub);

		/**
		 * Manually sets sequence number: field seq in packet (one byte)
		 *
		 * @details the seq field will be managed automatically, but if you want to pass
		 * 	the same seq numbers twice or maybe generate command with that seq then
		 * 	you should call that command (e.g. for tests).
		 * 	But in normal mode it's not needed
		 */
		void setSequenceNumber(uint8_t num);

		/**
		 * returns current version of the library
		 */
		const char* version();

		/**
		 * Waits for writer to finish its one job.
		 * Needed for tests.
		 */
		void waitOneOperation();

		/**
		 * Called after one operation was finished (command was sent and answer was received from printer).
		 * Needed for tests.
		 */
		void processingEnded();

		/**
		 * Executes custom user's command
		 *
		 * @param command any possible number for a command which printer would understand
		 *
		 * @param param string with parameters to printer which correspond the command number's syntax
		 *
		 */
		void customCommand(int command, const char* param);

		/**
		 * opens a new non fiscal receipt.
		 *
		 * command's number: 38
		 *
		 * @param param parameter:
		 * 1 - don't print header lines
		 */
		void openNonFiscalReceipt(char param = 0);

		/**
		 * closes an opened non fiscal receipt (must be called after openNonFiscalReceipt)
		 *
		 * command's number: 39
		 *
		 * @param param parameter:
		 * 1 - don't print header lines
		 */
		void closeNonFiscalReceipt(char param = 0);

		/**
		 * shows device's diagnostic
		 *
		 * command's number: 90
		 *
		 * @param type of diagnostic for detailed reference see documentation to printer
		 */
		void showDiagnostic(int type);

		/**
		 * programs serial number of printer
		 *
		 * command's number: 91
		 *
		 * @param serialNumber new serial number of the printer
		 */
		void programSerialNumber(const char* serialNumber);

		/**
		 * rolls printer's paper (feeds paper)
		 *
		 * command's number: 44
		 *
		 * @param amount of paper to be rolled
		 */
		void feedPaper(unsigned int amount);

		/**
                 * Printing of a free non-fiscal text
		 *
		 * command's number: 42
		 *
		 * @param p parameter corresponding to command
                 */
		void printNonFiscalText(struct nonFiscalTextParams& p);

		/**
		 *
		 * checks connection mode
		 *
		 * command's number: 45
		 */
		void checkConnectionMode();

		/**
                 * opens fiscal receipt
		 *
		 * command's number: 48
		 *
		 * @param p parameter corresponding to command
                 */
		void openFiscalReceipt(struct fiscalReceiptParams& p);

		/**
                 * registers a sale
		 *
		 * command's number: 49
		 *
		 * @param p parameter corresponding to command
                 */
		void saleRegistration(struct saleRegistrationParams& p);

		/**
		 * returns the active VAT rates
		 *
		 * command's number: 50
                 */
		void returnVAT();

		/**
		 * shows/prints Subtotal
		 *
		 * command's number: 51
		 *
		 * @param p parameter corresponding to command
		 */
		void subtotal(struct subtotalParams& p);

		/**
		 * calculates payment
		 *
		 * command's number: 53
		 *
		 * @param p parameter corresponding to command
		 */
		void paymentsCalculation(struct paymentsCalculationParams& p);

		/**
		 * prints free fiscal text
		 *
		 * command's number: 54
		 *
		 * @param p parameter corresponding to command
		 */
		void printFreeFiscalText(struct nonFiscalTextParams& p);

		/**
		 * closes an opened fiscal receipt (must be called after openFiscalReceipt)
		 *
		 * command's number: 56
		 */
		void closeFiscalReceipt();

		/**
		 * prints free fiscal text
		 *
		 * command's number: 57
		 *
		 * @param p parameter corresponding to command
		 */
		void writeGSMorEmailForFiscalReceipt(struct gsmEmailParams& p);

		/**
		 * registers a sale of a programmed item
		 *
		 * command's number: 58
		 *
		 * @param p parameter corresponding to command
                 */
		void saleRegistrationProgIt(struct saleRegistrationParams& p);

		/**
		 * makes X- or Z- reports
		 *
		 * command's number: 69
		 *
		 * @param type of the report to be produced
		 */
		void report(const char type);

		/**
		 * cancels fiscal receipt
		 *
		 * command's number: 60
		 */
		void cancelFiscalReceipt();

		/**
		 * sets date and time of the fiscal printer
		 *
		 * command's number: 61
		 *
		 * @param p date and time to be installed
		 */
		void setDateTime(struct dateTime& p);

		/**
		 * reads date and time of the fiscal printer
		 *
		 * command's number: 62
		 *
		 * @param p date and time to be installed
		 */
		void readDateTime();

		/**
		 * asks for information about last fiscal entry
		 *
		 * command's number: 64
		 *
		 * @param p type and record type of the info
		 */
		void lastFiscalEntryInfo(struct lastFiscalEntryParams& p);

		/**
		 * returns info about daily taxation
		 *
		 * command's number: 65
		 *
		 * @param type Type of returned data. Default: 0;
		 * o 0 - Turnovers by TAX groups including CORRECTIONS, accumulated by receipts SALES "Приход";
		 * o 1 - VAT amounts by TAX groups including CORRECTIONS, accumulated by receipts SALES "Приход";
		 * o 2 - Turnovers by TAX groups including CORRECTIONS, accumulated by receipts RETURN_SALES "Возврат прихода";
		 * o 3 - VAT amounts by TAX groups including CORRECTIONS, accumulated by receipts RETURN_SALES "Возврат прихода";
		 * o 4 - Turnovers by TAX groups including CORRECTIONS, accumulated by receipts PURCHASES "Расход";
		 * o 5 - VAT amounts by TAX groups including CORRECTIONS, accumulated by receipts PURCHASES "Расход";
		 * o 6 - Turnovers by TAX groups including CORRECTIONS, accumulated by receipts RETURN_PURCHASES "Возврат расхода";
		 * o 7 - VAT amounts by TAX groups including CORRECTIONS, accumulated by receipts RETURN_PURCHASES "Возврат расхода";
		 * o 8 - Turnovers by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF SALES "Коррекция Приход";
		 * o 9 - VAT amounts by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF SALES "Коррекция Приход";
		 * o 10 - Turnovers by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF RETURN_SALES "Коррекция Возврат прихода";
		 * o 11 - VAT amounts by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF RETURN_SALES "Коррекция Возврат прихода";
		 * o 12 - Turnovers by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF PURCHASES "Коррекция Расход";
		 * o 13 - VAT amounts by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF PURCHASES "Коррекция Расход";
		 * o 14 - Turnovers by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF RETURN_PURCHASES "Коррекция Возврат расхода";
		 * o 15 - VAT amounts by TAX groups from CORRECTIONS only, accumulated by receipts CORRECTION OF RETURN_PURCHASES "Коррекция Возврат расхода";
		 */
		void dailyTaxationInfo(char type);

		/**
		 * returns info about remaining entries for Z-Reports
		 *
		 * command's number: 68
		 */
		void remainingEntriesZReportsFM();


		/**
		 * cash in and out operations
		 *
		 * command's number: 70
		 *
		 * @param type of operation:
		 * o '0' - cash in;
		 * o '1' - cash out;
		 * o '2' - cash in - (foreign currency);
		 * o '3' - cash out - (foreign currency);
		 *
		 * @param amount the sum (0.00÷9999999.99); If Amount=0, the only Answer is returned,
		 * and receipt does not print.
		 */
		void cashInOut(char type, double amount);

		/**
		 * prints diagnostic information on printer's paper
		 *
		 * command's number: 71
		 *
		 * @param type of the information printed;
		 * Devices with LAN module:
		 * o '0' - general information about the device;
		 * o '1' - test of the modem;
		 * o '2' - reserved;
		 * o '3' - get LAN settings;
		 * o '4' - test of the LAN interface;
		 * o '5' - test of the ФН ("фискальный накопитель");
		 * o '6' - get ФН ("фискальный накопитель") parameters;
		 * Devices with WiFi module:
		 * o '0' - general information about the device;
		 * o '1' - test of the modem;
		 * o '2' - test of the WiFi module;
		 * o '3' - get wireless access points of the WiFi module;
		 * o '31'- print wireless access points of the WiFi module;
		 * o '4' - reserved;
		 * o '5' - test of the ФН ("фискальный накопитель");
		 * o '6' - get ФН ("фискальный накопитель") parameters;
		 */
		void printDiagnostic(char type);

		/**
		 * changes fiscal parameters
		 *
		 * command's number: 72
		 *
		 * @param p fiscalization params
		 */
		void fiscalization(struct fiscalizationParams& p);

		/**
		 * reads the status of the printer
		 *
		 * command's number: 74
		 *
		 */
		void readStatus();

		/**
		 * reads the status of the current or last receipt
		 *
		 * command's number: 76
		 *
		 */
		void readReceiptStatus();

		/**
		 * reads the status of FN and OFD
		 *
		 * command's number: 77
		 *
		 */
		void readFNStatus();

		/**
		 * plays sound
		 *
		 * command's number: 80
		 *
		 */
		void playSound(uint16_t hz, uint16_t msec);

		/**
		 * programs VAT
		 *
		 * command's number: 83
		 *
		 * @param p params to be programmed
		 */
		void programVAT(struct programVATParams& p);

		/**
		 * prints a bar code
		 *
		 * command's number: 84
		 *
		 * @param p bar code parameters
		 */
		void printBarCode(struct barCodeParams& p);

		/**
		 * returns the date of the last fiscal record
		 *
		 * command's number: 86
		 *
		 */
		void lastFiscalDate();

		/**
		 * asks item's groups information
		 *
		 * command's number: 87
		 *
		 * @param group number of item group (1-99) or negative => prints info about all groups
		 */
		void itemGroupInfo(char group);

		/**
		 * asks about department's information
		 *
		 * command's number: 88
		 *
		 * @param dep number of a department (1÷99). If zero or negative - printing of Department report;
		 *
		 * @param rec_type type of receipts. Default: 0;
		 * o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 */
		void departmentInfo(char dep, char rec_type);

		/**
		 * tests fiscal memory (read or write)
		 *
		 * command's number: 89
		 *
		 * @param write type of test
		 * o 0 - Read test.
		 * o 1 - Write and read test;
		 */
		void fiscalMemoryTest(char write);

		/**
		 * prints separate line
		 *
		 * command's number: 92
		 *
		 * @param type of the line to be printed
		 */
		void printSeparateLine(enum separateLineType type);

		/**
		 * fiscal memory report
		 *
		 * command's number: 94
		 *
		 * @param p parameters of report
		 */
		void fiscalMemoryReport(struct fiscalMemoryReportParams& p);

		/**
		 * fiscal memory z-report or shift status
		 *
		 * command's number: 95
		 *
		 * @param p parameters of report
		 */
		void fiscalMemoryZReport(struct fiscalMemoryZReportParams& p);

		/**
		 * programs TAX number
		 *
		 * command's number: 98
		 *
		 * @param tax number to be programmed
		 */
		void programTAX(const char* tax);

		/**
		 * reads TAX number
		 *
		 * command's number: 99
		 */
		void readTAX();

		/**
		 * asks for error explanation
		 *
		 * command's number: 100
		 *
		 * @param err number to be explained
		 */
		void readError(int err);

		/**
		 * sets new password
		 *
		 * command's number: 101
		 *
		 * @param p password parameters
		 */
		void setPassword(struct password& p);

		/**
		 * registers operator with his password
		 *
		 * command's number: 102
		 *
		 * @param opCode Operator number from 1...32;
		 *
		 * @param opPass Operator password, ascii string of digits. Lenght from 1...8;
		 */
		void regOperator(char opCode, const char *opPass);

		/**
		 * asks about current receipt's information
		 *
		 * command's number: 103
		 */
		void receiptInfo();

		/**
		 * reports about operators' actions
		 *
		 * command's number: 105
		 *
		 * @param fistOp Operator's number from 1...30;
		 *
		 * @param lastOp Operator's number from 1...30;
		 *
		 * @param clear Clears registers for operators. Default: 0;
		 * o '0' - Do not clear registers for operators;
		 * o '1' - Clear registers for operators;
		 */
		void reportOperators(char firstOp, char lastOp, char clear);

		/**
		 * sets drawer openning time
		 *
		 * command's number: 106
		 *
		 * @param time
		 */
		void setDrawerOpeningTime(char time);

		/**
		 * gives information about programmed items
		 *
		 * command's number: 107
		 *
		 */
		void itemInfo();

		/**
		 * programs item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be programmed
		 *
		 */
		void programItem(struct itemsInfo& p);

		/**
		 * sets item's quantity
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be programmed
		 *
		 */
		void setAvailabilityOfItem(struct itemsInfo& p);

		/**
		 * deletes items from PluNum till lastPlu
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be programmed
		 */
		void deleteItems(struct itemsInfo& p);

		/**
		 * reads item's info by its number
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be read
		 */
		void readItem(struct itemsInfo& p);

		/**
		 * finds an item info by its number
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void findItem(struct itemsInfo& p);

		/**
		 * retuns a last found item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void lastFoundItem(struct itemsInfo& p);

		/**
		 * retuns a next found item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void nextFoundItem(struct itemsInfo& p);

		/**
		 * retuns a next found item with sales on it
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void nextSalesItem(struct itemsInfo& p);

		/**
		 * data about the last found item with sales on it;
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void lastSalesItem(struct itemsInfo& p);

		/**
		 * data for the next found item with sales on it
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void nextFoundSalesItem(struct itemsInfo& p);

		/**
		 * first not programmed item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void firstNotProgrammedItem(struct itemsInfo& p);

		/**
		 * last not programmed item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void lastNotProgrammedItem(struct itemsInfo& p);

		/**
		 * prints dublicate receipt
		 *
		 * command's number: 109
		 */
		void printDublicateReceipt();

		/**
		 * prints additional Daily Info
		 *
		 * command's number: 110
		 *
		 * @param type  Type of information. Default: 0;
		 * o '0' - Payments in normal receipts "Кассовых чеков";
		 * o '1' - Payments in correction receipts "Кассовых чеков коррекции";
		 * o '2' - Number and sum of sales;
		 * o '3' - Number and sum of discounts and surcharges;
		 * o '4' - Number and sum of corrections "Сторно" in a receipt and of annulled receipts "Отмененных чеков";
		 * o '5' - Number and sum of cash in and cash out operations;
		 *
		 * @param rec_type Type of receipts (for types 0,1,2,3 and 4). By default RecType - 0;
		 * o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 */
		void additionalDInfo(char type, char rec_type);

		/**
		 * PLU report
		 *
		 * command's number: 111
		 *
		 * @param type  Type of report;
		 * o '0' - PLU turnovers;
		 * o '1' - PLU turnovers with clearing; o '2' - PLU parameters;
		 * o '3' - PLU stock;
		 *
		 * @param firstPlu First PLU in the report (1÷3000). Default: 1;
		 *
		 * @param lastPlu Last PLU in the report (1÷3000). Default: 3000;
		 */
		void pluReport(char type, char firstPlu, char lastPlu);

		/**
		 * Information for operator
		 *
		 * command's number: 112
		 *
		 * @param op Operator number (1÷30);
		 *
		 * @param rec_type Type of receipts. Default: 0;
		 * o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 */
		void infoForOperator(char op, char rec_type);

		/**
		 * Reading FM
		 *
		 * command's number: 116
		 *
		 * @param op type of operation = '0';
		 *
		 * @param address Start address 0÷FFFFFF (format ascii-hex).
		 *
		 * @param count Number of bytes (1÷104)
		 */
		void readFM(char op, char address, char count);

		/**
		 * Reading Mem
		 *
		 * command's number: 121
		 *
		 * @param address Start address 0÷FFFFFF (format ascii-hex).
		 *
		 * @param count Number of bytes (1÷64)
		 */
		void readMem(char address, char count);

		/**
		 * Search documents in EJ by date
		 *
		 * command's number: 124
		 *
		 * @param p document parameters
		 */
		void searchDocuments(struct searchDocsParams& p);

		/**
		 * Info EJ
		 *
		 * command's number: 125
		 *
		 * @param op Option:
		 * o '0' - Set document to read;
		 * o '1' - Read one line as text. Must be called multiple time to read whole document;
		 * o '2' - Read as data. Must be called multiple time to read whole document;
		 * o '3' - Print document;
		 *
		 * @param doc_num Number of document (1÷99999999). Needed for Option = 0 and 3.
		 *
		 * @param rec_type Document type. Needed for Option = 0.
		 * o '0' - all types;
		 * o '1' - fiscal receipts (including Z-reports);
		 * o '2' - non fiscal receipts;
		 * o '3' - Z-reports;
		 * o "20" - full EJ content for Z report specified in {doc_num};
		 */
		void infoEJ(char op, char doc_num, char rec_type);

		/**
		 * OFD - FN registration/deregistration/change registration
		 *
		 * command's number: 144
		 *
		 * @param op type of operation;
		 * '1' - Registration;
		 * '2' - Reregistration same FN;
		 * '3' - Deregistration;
		 * '4' - Reregistration new FN;
		 * '5' - Force close current FN ( use when FN is broken or lost ). Require service jumper!;
		 * '9' - Erase FN (Service program only!!!). Only development FN can by erased;
		 *
		 * @param pass Current inspector password;
		 */
		void registerOFD(char op, const char *pass);

		/**
		 * Entering of service parameters
		 *
		 * command's number: 253
		 *
		 * @param op Type of entered parameter;
		 * '0' - Service password (Password of the Service man);
		 * '1' - Command to FN
		 *
		 * @param param Value of entered parameter;
		 * Service password (Password of the Service man). Text up to 8 symbols. The default password
		 * may be blank ( 0 symbols ) or "70";
		 */
		void serviceParameters(char op, const char *param);

		/**
		 * Programming
		 *
		 * command's number: 255
		 *
		 * @param name Variable name;
		 *
		 * @param param used for index if variable is array. For variable that is not array can be left blank. Default: 0;
		 * Note:For example: Header[], Index 0 refer to line 1. Index 9 refer to line 10.
		 *
		 * @param value If this parameter is blank ECR will return current value (Answer(2)). If the value is set, then
		 * ECR will program this value (Answer(1));
		 */
		void programming(const char *name, const char *param, const char *value);
	};
};
