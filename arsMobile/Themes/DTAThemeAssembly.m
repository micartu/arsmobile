//
//  DTAThemeAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAThemeAssembly.h"
#import "DTAThemeFactory.h"
#import "DTATheme.h"

@implementation DTAThemeAssembly

- (DTATheme *)currentTheme {
    return [TyphoonDefinition withFactory:[self themeFactory] selector:@selector(curTheme)];
}

- (DTAThemeFactory *)themeFactory {
    return [TyphoonDefinition withClass:[DTAThemeFactory class] configuration:^(TyphoonDefinition *definition) {
                [definition useInitializer:@selector(initWithThemes:) parameters:^(TyphoonMethod *initializer) {
                    [initializer injectParameterWith:@[
                                                       [self initialTheme]
                                                        ]];
                }];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (DTATheme*)initialTheme {
    return [TyphoonDefinition withClass:[DTATheme class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(btnForegroundColor) with:[UIColor colorWithRed:2.0/255.0f
                                                                                      green:142.0/255.0f
                                                                                       blue:1.0/255.0f
                                                                                      alpha:1.0f]];
        [definition injectProperty:@selector(btnFrontColor) with:[UIColor whiteColor]];
        [definition injectProperty:@selector(btnDisabledColor) with:[UIColor lightGrayColor]];
        [definition injectProperty:@selector(errColor) with:[UIColor colorWithRed:208.0/255.0
                                                                            green:2.0/255.0
                                                                             blue:27.0/255.0 alpha:1]];
    }];
}

@end
