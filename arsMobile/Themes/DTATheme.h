//
//  DTATheme.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DTATheme : NSObject

/**
 button's foreground color
 */
@property (nonatomic, strong, readonly) UIColor *btnForegroundColor;
@property (nonatomic, strong, readonly) UIColor *btnFrontColor;
@property (nonatomic, strong, readonly) UIColor *btnDisabledColor;
@property (nonatomic, strong, readonly) UIColor *errColor;

@end
