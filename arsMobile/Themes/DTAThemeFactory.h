//
//  DTAThemeFactory.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTATheme;

@interface DTAThemeFactory : NSObject {
    DTATheme *_curtheme;
}

@property(nonatomic, strong, readonly) NSArray<DTATheme*> *themes;

/**
 initializes with preloaded themes
 */
- (instancetype)initWithThemes:(NSArray *)themes;

/**
 returns current selected theme
 */
- (DTATheme*)curTheme;

/**
 changes current theme from installed in the system with index
 */
- (void)changeTheme:(NSUInteger) index;

@end
