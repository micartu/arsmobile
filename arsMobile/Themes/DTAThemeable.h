//
//  DTAThemeable.h
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTATheme;

/**
 @author Michael Artuerhof

 interface for themeable views
 */
@protocol DTAThemeable <NSObject>

/**
 theme used for customization of the view
 */
@property(nonatomic, strong) DTATheme *theme;

/**
 should be called in the moment the theme (set in property theme) must be applied to the view
 */
- (void)applyTheme;

@end
