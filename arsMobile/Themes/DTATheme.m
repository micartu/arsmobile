//
//  DTATheme.m
//  arsMobile
//
//  Created by Michael Artuerhof on 06.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTATheme.h"

@interface DTATheme ()
@property (nonatomic, strong, readwrite) UIColor *btnForegroundColor;
@property (nonatomic, strong, readwrite) UIColor *btnDisabledColor;
@property (nonatomic, strong, readwrite) UIColor *btnFrontColor;
@property (nonatomic, strong, readwrite) UIColor *errColor;
@end

@implementation DTATheme

@end
