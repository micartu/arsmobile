//
//  DTAAlertAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAAlertAssembly.h"
#import "DTAAlertFactory.h"

@implementation DTAAlertAssembly

- (id<DTAFabricAlertProtocol>)alertFactory {
    return [TyphoonDefinition withClass:[DTAAlertFactory class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

@end
