//
//  DTAAlertFactory.h
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTAFabricAlertProtocol.h"

@interface DTAAlertFactory : NSObject <DTAFabricAlertProtocol>

@end
