//
//  DTAAlertProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@protocol DTAAlertProtocol <NSObject>

- (void)showMessage:(NSString*)message
          withTitle:(NSString*)title
          andAction:(void(^)(UIAlertAction *action))handler;
- (void)showErrorWithMessage:(NSString*)message;
- (void)showWarningWithMessage:(NSString*)message;
- (void)showYesNoDialogWithMessage:(NSString *)message
                      andYesAction:(void(^)(UIAlertAction *action))yesHandler
                       andNoAction:(void(^)(UIAlertAction *action))noHandler;
- (void)inputDialogWithTitle:(NSString*)title
                 withMessage:(NSString*)message
                 andOkAction:(void(^)(UIAlertAction *action, UIAlertController *alertController))yesHandler;
- (void)chooseDialogWithTitle:(NSString*)title
                  withMessage:(NSString*)message
             withActionTitles:(NSArray*)titles
                   andActions:(NSArray*)actions
                    andStyles:(NSArray*)styles;

@end
