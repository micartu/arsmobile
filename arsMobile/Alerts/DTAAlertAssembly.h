//
//  DTAAlertAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import <UIKit/UIKit.h>

@protocol DTAFabricAlertProtocol;

@interface DTAAlertAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<DTAFabricAlertProtocol>)alertFactory;

@end
