//
//  DTAFabricAlertProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@protocol DTAFabricAlertProtocol <NSObject>

/**
 shows a message with custom title and message
 */
- (UIAlertController*)acWithMessage:(NSString*)message
                           andTitle:(NSString*)title
                          andAction:(void(^)(UIAlertAction *action))handler;

/**
 shows an error message
 */
- (UIAlertController*)acErrorWithMessage:(NSString*)message;

/**
 shows an error message
 */
- (UIAlertController*)acErrorWithMessage:(NSString*)message
                               andAction:(void(^)(UIAlertAction *action))handler;

/**
 shows a warning message
 */
- (UIAlertController*)acWarningWithMessage:(NSString*)message;

- (UIAlertController *)acInputDialogWithTitle:(NSString*)title
                                  withMessage:(NSString*)message
                                  andOkAction:(void(^)(UIAlertAction *action, UIAlertController *alertController))yesHandler
                              andCancelAction:(void(^)(UIAlertAction *action))cancelHandler;
- (UIAlertController *)acInputDialogWithTitle:(NSString*)title
                                  withMessage:(NSString*)message
                                  andOkAction:(void(^)(UIAlertAction *action, UIAlertController *alertController))yesHandler;
- (UIAlertController *)acYesNoDialogWithMessage:(NSString *)message
                                   andYesAction:(void(^)(UIAlertAction *action))yesHandler
                                    andNoAction:(void(^)(UIAlertAction *action))noHandler;
- (UIAlertController *)acChooseDialogWithTitle:(NSString*)title
                                   withMessage:(NSString*)message
                              withActionTitles:(NSArray*)titles
                                    andActions:(NSArray*)actions
                                     andStyles:(NSArray*)styles;

@end
