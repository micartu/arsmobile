//
//  DTAAlertFactory.m
//  arsMobile
//
//  Created by Michael Artuerhof on 12/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAAlertFactory.h"

@implementation DTAAlertFactory

- (UIAlertController*)acWithMessage:(NSString*)message
                           andTitle:(NSString*)title
                          andAction:(void(^)(UIAlertAction *action))handler {
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:title
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:handler];
    [ac addAction:defaultAction];
    return ac;
}

- (UIAlertController*)acErrorWithMessage:(NSString*)message
                               andAction:(void(^)(UIAlertAction *action))handler {
    return [self acWithMessage:message
                      andTitle:@"Ошибка!"
                     andAction:handler];
}

- (UIAlertController*)acErrorWithMessage:(NSString*)message {
    return [self acErrorWithMessage:message
                          andAction:^(UIAlertAction * action) {}];
}

- (UIAlertController*)acWarningWithMessage:(NSString*)message {
    return [self acWithMessage:message
                      andTitle:@"Внимание!"
                     andAction:^(UIAlertAction * action) {}];
}

- (UIAlertController *)acInputDialogWithTitle:(NSString*)title
                                  withMessage:(NSString*)message
                                  andOkAction:(void(^)(UIAlertAction *action, UIAlertController *alertController))yesHandler
                              andCancelAction:(void(^)(UIAlertAction *action))cancelHandler {
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:title
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [ac addTextFieldWithConfigurationHandler:^(UITextField *textField) {}];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          yesHandler(action, ac);
                                                      }];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"Отмена"
                                                       style:UIAlertActionStyleDefault
                                                     handler:cancelHandler];
    [ac addAction:yesAction];
    [ac addAction:noAction];
    return ac;
}

- (UIAlertController *)acInputDialogWithTitle:(NSString*)title
                                  withMessage:(NSString*)message
                                  andOkAction:(void(^)(UIAlertAction *action, UIAlertController *alertController))yesHandler {
    return [self acInputDialogWithTitle:title
                            withMessage:message
                            andOkAction:yesHandler
                        andCancelAction:^(UIAlertAction *action) {}];
}

- (UIAlertController *)acYesNoDialogWithMessage:(NSString *)message
                                   andYesAction:(void(^)(UIAlertAction *action))yesHandler
                                    andNoAction:(void(^)(UIAlertAction *action))noHandler {
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Внимание!"
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Да"
                                                        style:UIAlertActionStyleDefault
                                                      handler:yesHandler];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"Нет"
                                                       style:UIAlertActionStyleDefault
                                                     handler:noHandler];
    [ac addAction:yesAction];
    [ac addAction:noAction];
    return ac;
}


- (UIAlertController *)acChooseDialogWithTitle:(NSString*)title
                                   withMessage:(NSString*)message
                              withActionTitles:(NSArray*)titles
                                    andActions:(NSArray*)actions
                                     andStyles:(NSArray*)styles {
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:title
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    assert(titles.count == actions.count && actions.count == styles.count);
    for (int i = 0; i < titles.count; i++) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:titles[i]
                                                         style:[styles[i] integerValue]
                                                       handler:actions[i]];
        [ac addAction:action];
    }
    return ac;
}

@end
