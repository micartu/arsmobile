//
//  DTAFPrinterProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@class DTACartItem;

@protocol DTAFPrinterProtocol <NSObject>

@property (nonatomic, copy) NSString *printerAddress;
@property (nonatomic, readonly) BOOL isConnected;

- (void)printNonFiscalText:(NSString *)text
            withCompletion:(void (^)(BOOL success, NSError *error))completion;

- (void)openShiftForOperator:(NSString*)opName withCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)performXReportWithCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)performZReportWithCompletion:(void (^)(BOOL success, NSError *error))completion;

- (void)openChequeForOperatorName:(NSString*)op
                         andBuyer:(NSString*)buyer
                   isReturnCheque:(BOOL)retCheque
                   withCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)addItemToCheque:(DTACartItem*)item withCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)payCheque:(double)sum paymentByCash:(BOOL)isCash withCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)closeChequeWithCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)cancelChequeWithCompletion:(void (^)(BOOL success, NSError *error))completion;
- (void)sendCopyOfChequeToAddress:(NSString*)address withCompletion:(void (^)(BOOL success, NSError *error))completion;

@end
