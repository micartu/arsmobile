//
//  DTAFPrinterService.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <ExternalAccessory/ExternalAccessory.h>
#import "DTAFPrinterService.h"
#import "ARSTasker.h"
#import "ARSFPrinter.h"
#import "ARSFReport.h"
#import "arsprinter_err_codes.h"
#import "constants.h"
#import "DTACartItem.h"
#import "DTAItem.h"
#import "DTACategory.h"

static NSString* const kPrinterProtocol = @"com.datecs.printer.fiscal.2";
static const NSTimeInterval kTimeout4operation = 10; // seconds

@interface DTAFPrinterService () <ARSFPrinterProtocol, ARSTaskerProtocol>
{
    dispatch_queue_t _queue;
    BOOL _isBusy;
    BOOL _isConnected;
    BOOL _deviceFound;
    NSInteger _lastCommandTaskID;
    ARSFPrinter *_printer;
    ARSTasker *_tasker;
    NSTimer *_errTimer;
    EAAccessory *_accessory;
    void (^_defaultCompletion)(BOOL success, NSError *error);
}
@end

@implementation DTAFPrinterService

@synthesize isConnected, printerAddress;

- (instancetype)init {
    self = [super init];
    if (self) {
        _isBusy = NO;
        _lastCommandTaskID = -1;
        _isConnected = NO;
        _queue = dispatch_queue_create("ars_printer_queue", DISPATCH_QUEUE_SERIAL);
        _defaultCompletion = nil;
        _printer = [[ARSFPrinter alloc] init];
        _printer.delegate = self;
        [self setPrinterAddress:[[NSUserDefaults standardUserDefaults] valueForKey:kDefaultsKeySelectedPrinterName]];
    }
    return self;
}

- (BOOL)tryToConnect2Printer {
    if (!_accessory) {
        [self setPrinterAddress:[[NSUserDefaults standardUserDefaults] valueForKey:kDefaultsKeySelectedPrinterName]];
        if (_deviceFound)
            return YES;
    }
    return NO;
}

- (BOOL)isConnected {
    return _isConnected;
}

- (void)setPrinterAddress:(NSString *)address {
    _deviceFound = NO;
    if (address) {
        NSArray *connected = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
        for (int i = 0; i < connected.count; i++) {
            EAAccessory* accessory = connected[i];
            if ([address isEqualToString:accessory.name]) {
                NSUInteger indexOfPrinter = [accessory.protocolStrings indexOfObject:kPrinterProtocol];
                if (indexOfPrinter == NSNotFound) {
                    _isConnected = NO;
                    NSLog(@"device (%@) isn't an ARS Printer!", accessory.name);
                    return;
                }
                _deviceFound = YES;
                _accessory = accessory;
                [_printer connectToPrinter:_accessory];
                _tasker = [[ARSTasker alloc] initWithPrinter:_printer];
                _tasker.delegate = self;
                break;
            }
        }
    }
}

#pragma mark - ARSFPrinterProtocol

- (void)connected {
    NSLog(@"CONNECTED TO PRINTER");
    _isConnected = YES;
}

- (void)disconnected {
    NSLog(@"DISCONNECTED FROM PRINTER");
    _isConnected = NO;
}

- (void)statusChanged:(NSString *)status {
    NSLog(@"status: %@", status);
}

#pragma mark - ARSTaskerProtocol

- (void)printerAnswered:(NSArray *)ans forSeq:(int)seq {
    if (seq == (int)_lastCommandTaskID) {
        [self stopTimer];
        if (ans.count > 0) {
            NSString *ret = ans[0];
            int errCode = (int)[ret integerValue];
            _isBusy = NO;
            if (!errCode) {
                if (_defaultCompletion) {
                    _defaultCompletion(YES, nil);
                }
                return;
            }
            else {
                NSError *er = [NSError errorWithDomain:kArsMobileDomain code:-1
                                              userInfo:@{NSLocalizedDescriptionKey:[self errorDescriptionForCode:-errCode]}];
                if (-errCode == 777)
                    _isConnected = NO;
                if (_defaultCompletion)
                    _defaultCompletion(NO, er);
            }
            _defaultCompletion = nil;
        }
    }
}

#pragma mark - public methods

- (void)printNonFiscalText:(NSString *)text
            withCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        [_tasker openNonFiscalReceipt];
        [_tasker printNonFiscalText:text];
        _lastCommandTaskID = [_tasker closeNonFiscalReceipt];
        _defaultCompletion = completion;
    });
}

- (void)openShiftForOperator:(NSString*)opName withCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker openDayForOperator:30
                                                 andPass:30
                                                andBuyer:@""
                                         andOperatorName:opName];
        _defaultCompletion = completion;
    });
}

- (void)performXReportWithCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker xReport];
        _defaultCompletion = completion;
    });
}

- (void)performZReportWithCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker zReport];
        _defaultCompletion = completion;
    });
}

- (void)openChequeForOperatorName:(NSString*)op
                         andBuyer:(NSString*)buyer
                   isReturnCheque:(BOOL)retCheque
                   withCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker openChequeForOperator:30
                                                    andPass:30
                                                   andBuyer:buyer
                                            andOperatorName:op
                                             isReturnCheque:retCheque];
        _defaultCompletion = completion;
    });
}

- (void)addItemToCheque:(DTACartItem*)item withCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker registerItem:item.item.title
                                          forTaxGr:item.item.taxType
                                     andDepartment:0
                                   andDiscountType:item.discountPercentage > 0.001 ? 2 : 0
                                  andDiscountValue:item.discountPercentage
                                       andQuantity:item.count
                                          andPrice:item.item.price];
        _defaultCompletion = completion;
    });
}

- (void)payCheque:(double)sum paymentByCash:(BOOL)isCash withCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker paymentSum:sum ofType:isCash ? PaymentCash : PaymentCard];
        _defaultCompletion = completion;
    });
}

- (void)closeChequeWithCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker closeCheque];
        _defaultCompletion = completion;
    });
}

- (void)cancelChequeWithCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker cancelCheque];
        _defaultCompletion = completion;
    });
}

- (void)sendCopyOfChequeToAddress:(NSString*)address withCompletion:(void (^)(BOOL success, NSError *error))completion {
    if (![self canRunNextCommandWithCompletion:completion])
        return;
    dispatch_async(_queue, ^{
        _lastCommandTaskID = [_tasker sendCopyOfChequeToEMailOrTelephone:address];
        _defaultCompletion = completion;
    });
}

#pragma mark - Private Methods

- (BOOL)canRunNextCommandWithCompletion:(void (^)(BOOL success, NSError *error))completion {
    NSError *er = nil;
    [self checkConnectionState:&er];
    if (er) {
        if (completion)
            completion(NO, er);
        return NO;
    }
    _isBusy = YES;
    [self startTimer];
    return YES;
}

- (void)startTimer {
    [self stopTimer];
    _errTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeout4operation
                                                 target:self
                                               selector:@selector(timeoutForOperation)
                                               userInfo:nil
                                                repeats:NO];
}

- (void)stopTimer {
    if (_errTimer.isValid) {
        [_errTimer invalidate];
        _errTimer = nil;
    }
}

- (void)timeoutForOperation {
    if (_isBusy) {
        _isBusy = NO;
        _isConnected = NO;
        if (_defaultCompletion) {
            NSError *er = [NSError errorWithDomain:kArsMobileDomain code:-1
                                          userInfo:@{NSLocalizedDescriptionKey: @"Нет связи с принтером (timeout)"}];
            _defaultCompletion(NO, er);
            _defaultCompletion = nil;
        }
    }
}

- (NSString*)errorDescriptionForCode:(int)errCode {
    switch (errCode) {
        case ERR_DEVICE_DAY_IS_OPENED:
            return @"Операционный день уже открыт";

        case ERR_DEVICE_DAY_IS_CLOSED:
            return @"Операционный день не открыт";

        case ERR_R_OPEN_BON:
            return @"Кассовый чек уже открыт";

        case ERR_R_PAY_STARTED:
            return @"Оплата начата";
    }
    return [NSString stringWithFormat:@"Неизвестный код ошибки: %ld", (long)errCode];
}

- (void)checkConnectionState:(NSError**)er {
    if (!_isConnected) {
        if (![self tryToConnect2Printer]) {
            *er = [NSError errorWithDomain:kArsMobileDomain code:-1
                                  userInfo:@{NSLocalizedDescriptionKey: @"Нет связи с принтером"}];
        }
    }
    if (_isBusy) {
        *er = [NSError errorWithDomain:kArsMobileDomain code:-1
                              userInfo:@{NSLocalizedDescriptionKey: @"Дождитесь завершения предыдущей операции"}];
    }
}

@end
