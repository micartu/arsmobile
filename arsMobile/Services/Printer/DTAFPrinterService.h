//
//  DTAFPrinterService.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTAFPrinterProtocol.h"

@interface DTAFPrinterService : NSObject <DTAFPrinterProtocol>

@end
