//
//  DTAServiceAssembly.h
//  arsMobile
//
//  Created by Michael Artuerhof on 07.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@protocol DTAStorageProtocol;
@protocol DTAFPrinterProtocol;

@interface DTAServiceAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<DTAStorageProtocol>)dataStorageService;
- (id<DTAFPrinterProtocol>)fiscalPrinterService;

@end
