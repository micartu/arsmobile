//
//  DTAStorageHandler.h
//  arsMobile
//
//  Created by Michael Artuerhof on 13/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTAStorageProtocol.h"

@interface DTAStorageHandler : NSObject <DTAStorageProtocol>

@end
