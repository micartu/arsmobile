//
//  DTAStorageHandler.m
//  arsMobile
//
//  Created by Michael Artuerhof on 13/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAStorageHandler.h"
#import <MagicalRecord/MagicalRecord.h>
#import "DTACategory.h"
#import "DTAItem.h"
#import "DTACartItem.h"
#import "Category+CoreDataClass.h"
#import "CartItem+CoreDataClass.h"
#import "Item+CoreDataClass.h"
#import "constants.h"

@implementation DTAStorageHandler

#pragma mark - DTAStorageProtocol - Categories

- (void)createCategoryWithName:(NSString*)name andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        Category *cat = [Category MR_createEntityInContext:context];
        NSNumber *num = [defaults objectForKey:kDefaultKeyCountForCategory];
        NSInteger cid;
        if (num)
            cid = [num longValue];
        else
            cid = 1;

        cat.cid = cid;
        cat.name = name;

        [defaults setObject:[NSNumber numberWithLong:++cid] forKey:kDefaultKeyCountForCategory];
        [defaults synchronize];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)modifyCategory:(DTACategory*)category andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", category.cid];
        Category *cat = [Category MR_findFirstWithPredicate:predicate inContext:context];
        if (cat)
            cat.name = category.title;
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)deleteCategory:(DTACategory*)category andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", category.cid];
        Category *cat = [Category MR_findFirstWithPredicate:predicate inContext:context];
        predicate = [NSPredicate predicateWithFormat:@"(category.cid == %" SCNd64 ")", category.cid];
        NSArray *items = [Item MR_findAllWithPredicate:predicate inContext:context];
        for (Item *i in items)
            [i MR_deleteEntity];
        predicate = [NSPredicate predicateWithFormat:@"(item.category.cid == %" SCNd64 ")", category.cid];
        items = [CartItem MR_findAllWithPredicate:predicate inContext:context];
        for (CartItem *i in items)
            [i MR_deleteEntity];
        [cat MR_deleteEntity];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)fetchAllCategoriesWithCompletion:(void (^)(NSArray<DTACategory *> *))completion {
    NSArray<Category*> *el = [Category MR_findAllSortedBy:@"name" ascending:YES];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:el.count];
    for (Category *cat in el) {
        DTACategory *c = [self wrapCategory:cat];
        [items addObject:c];
    }
    if (completion)
        completion(items);
}

- (void)getCategoryWithId:(NSInteger)cid andCompletion:(void (^)(DTACategory* cat))completion {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", cid];
    Category *c = [Category MR_findFirstWithPredicate:predicate];
    DTACategory *cat = c ? [self wrapCategory:c] : nil;
    if (completion)
        completion(cat);
}

#pragma mark - DTAStorageProtocol - Items

- (void)createItem:(DTAItem*)item andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSInteger cid = item.category ? item.category.cid : -1;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", cid];
        Category *c = [Category MR_findFirstWithPredicate:predicate inContext:context];
        if (!c) {
            NSLog(@"Category for item wasn't found: cannot create a not connected item");
            return;
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *num = [defaults objectForKey:kDefaultKeyCountForItem];
        NSInteger iid;
        if (num)
            iid = [num longValue];
        else
            iid = 1;
        Item *i = [Item MR_createEntityInContext:context];
        i.iid = iid;
        i.name = item.title;
        i.price = [[NSDecimalNumber alloc] initWithFloat:item.price];
        i.category = c;
        i.taxType = item.taxType;
        i.measureUnit = item.measureUnit;
        [c addItemObject:i];

        [defaults setObject:[NSNumber numberWithLong:++iid] forKey:kDefaultKeyCountForItem];
        [defaults synchronize];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)modifyItem:(DTAItem*)item andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(iid == %" SCNd64 ")", item.iid];
        Item *i = [Item MR_findFirstWithPredicate:predicate inContext:context];
        assert(i);
        i.name = item.title;
        i.price = [[NSDecimalNumber alloc] initWithFloat:item.price];
        i.taxType = item.taxType;
        i.measureUnit = item.measureUnit;
        if (item.category) { // update category?
            predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", item.category.cid];
            Category *c = [Category MR_findFirstWithPredicate:predicate inContext:context];
            if (c)
                i.category = c;
        }
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)deleteItem:(DTAItem*)item andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(iid == %" SCNd64 ")", item.iid];
        Item *i = [Item MR_findFirstWithPredicate:predicate inContext:context];
        assert(i);
        predicate = [NSPredicate predicateWithFormat:@"(item.iid == %" SCNd64 ")", item.iid];
        NSArray *items = [CartItem MR_findAllWithPredicate:predicate inContext:context];
        for (CartItem *c in items)
            [c MR_deleteEntity];
        [i MR_deleteEntity];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)fetchAllItemsForCategoryID:(NSInteger)cid withCompletion:(void (^)(NSArray<DTAItem *> *))completion {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", cid];
    Category *c = [Category MR_findFirstWithPredicate:predicate];
    if (!c) {
        if (completion)
            completion(nil);
        return;
    }
    DTACategory *cat = [self wrapCategory:c];
    predicate = [NSPredicate predicateWithFormat:@"(category.cid == %" SCNd64 ")", cid];
    NSArray<Item*> *el = [Item MR_findAllSortedBy:@"name" ascending:YES withPredicate:predicate];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:el.count];
    for(Item *i in el) {
        DTAItem *item = [self wrapItem:i andCategory:cat];
        [items addObject:item];
    }
    if (completion)
        completion(items);
}

#pragma mark - DTAStorageProtocol - CartItems

- (void)createCartItem:(DTAItem*)item andCount:(float)count andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(iid == %" SCNd64 ")", item.iid];
        Item *i = [Item MR_findFirstWithPredicate:predicate inContext:context];
        assert(i);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *num = [defaults objectForKey:kDefaultKeyCountForCartItem];
        NSInteger cid;
        if (num)
            cid = [num longValue];
        else
            cid = 1;
        CartItem *ci = [CartItem MR_createEntityInContext:context];
        ci.cid = cid;
        ci.count = count;
        ci.item = i;
        [i addCartObject:ci];

        [defaults setObject:[NSNumber numberWithLong:++cid] forKey:kDefaultKeyCountForCartItem];
        [defaults synchronize];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)createOrModifyCartForItem:(DTAItem*)item andCount:(float)count andCompletion:(void (^)(void))completion {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(item.iid == %" SCNd64 ")", item.iid];
    CartItem *ci = [CartItem MR_findFirstWithPredicate:predicate];
    if (ci) { // modify it
        DTACartItem *c = [self wrapCartItem:ci];
        c.count += count;
        return [self modifyCartItem:c andCompletion:completion];
    }
    else { // create a new cart item
        return [self createCartItem:item andCount:1 andCompletion:completion];
    }
}

- (void)modifyCartItem:(DTACartItem*)item andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", item.cid];
        CartItem *i = [CartItem MR_findFirstWithPredicate:predicate inContext:context];
        i.count = item.count;
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)deleteCartItem:(DTACartItem*)item andCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cid == %" SCNd64 ")", item.cid];
        CartItem *i = [CartItem MR_findFirstWithPredicate:predicate inContext:context];
        [i MR_deleteEntity];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)deleteAllCartItemsWithCompletion:(void (^)(void))completion {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *context) {
        NSArray<CartItem *> *items = [CartItem MR_findAllInContext:context];
        for (CartItem *i in items)
            [i MR_deleteEntity];
    } completion:^(BOOL success, NSError *error) {
        if (completion)
            completion();
    }];
}

- (void)fetchAllCartItemsWithCompletion:(void (^)(NSArray<DTACartItem *> *))completion {
    NSArray *el = [CartItem MR_findAllSortedBy:@"cid" ascending:YES];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:el.count];
    for(CartItem *i in el) {
        DTACartItem *item = [self wrapCartItem:i];
        [items addObject:item];
    }
    if (completion)
        completion(items);
}

- (DTACartItem*)findInCartTheItem:(DTAItem*)item {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(item.iid == %" SCNd64 ")", item.iid];
    CartItem *it = [CartItem MR_findFirstWithPredicate:predicate];
    if (it) {
        DTACartItem *cit = [self wrapCartItem:it];
        return cit;
    }
    return nil;
}

- (NSInteger)countAllCartItems {
    return [CartItem MR_countOfEntities];
}

#pragma mark - inner methods

- (NSInteger)countItemsForCategory:(Category*)cat {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(category.cid == %" SCNd64 ")", cat.cid];
    return [Item MR_countOfEntitiesWithPredicate:predicate];
}

- (DTACategory*)wrapCategory:(Category*)cat {
    DTACategory *c = [DTACategory new];
    c.cid = cat.cid;
    c.title = cat.name;
    c.count = [self countItemsForCategory:cat];
    return c;
}

- (DTAItem*)wrapItem:(Item*)item andCategory:(DTACategory*)cat {
    DTAItem *i = [DTAItem new];
    i.title = item.name;
    i.price = [item.price floatValue];
    i.iid = item.iid;
    i.taxType = item.taxType;
    i.measureUnit = item.measureUnit;
    i.category = cat;
    return i;
}

- (DTACartItem*)wrapCartItem:(CartItem*)item {
    DTACartItem *ci = [DTACartItem new];
    DTACategory *cat = [self wrapCategory:item.item.category];
    DTAItem *i = [self wrapItem:item.item andCategory:cat];
    ci.cid = item.cid;
    ci.count = item.count;
    ci.item = i;
    return ci;
}

@end
