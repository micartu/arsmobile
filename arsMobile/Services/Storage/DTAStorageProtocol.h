//
//  DTAStorageProtocol.h
//  arsMobile
//
//  Created by Michael Artuerhof on 13/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

@class DTACategory;
@class DTAItem;
@class DTACartItem;

@protocol DTAStorageProtocol <NSObject>

- (void)createCategoryWithName:(NSString*)name andCompletion:(void (^)(void))completion;
- (void)getCategoryWithId:(NSInteger)cid andCompletion:(void (^)(DTACategory* cat))completion;
- (void)modifyCategory:(DTACategory*)category andCompletion:(void (^)(void))completion;
- (void)deleteCategory:(DTACategory*)category andCompletion:(void (^)(void))completion;
- (void)fetchAllCategoriesWithCompletion:(void (^)(NSArray<DTACategory *> *))completion;

- (void)createItem:(DTAItem*)item andCompletion:(void (^)(void))completion;
- (void)modifyItem:(DTAItem*)item andCompletion:(void (^)(void))completion;
- (void)deleteItem:(DTAItem*)item andCompletion:(void (^)(void))completion;
- (void)fetchAllItemsForCategoryID:(NSInteger)cid withCompletion:(void (^)(NSArray<DTAItem *> *))completion;

- (void)createCartItem:(DTAItem*)item andCount:(float)count andCompletion:(void (^)(void))completion;
- (void)createOrModifyCartForItem:(DTAItem*)item andCount:(float)count andCompletion:(void (^)(void))completion;
- (void)modifyCartItem:(DTACartItem*)item andCompletion:(void (^)(void))completion;
- (void)deleteCartItem:(DTACartItem*)item andCompletion:(void (^)(void))completion;
- (void)deleteAllCartItemsWithCompletion:(void (^)(void))completion;
- (void)fetchAllCartItemsWithCompletion:(void (^)(NSArray<DTACartItem *> *))completion;
- (DTACartItem*)findInCartTheItem:(DTAItem*)item;
- (NSInteger)countAllCartItems;

@end
