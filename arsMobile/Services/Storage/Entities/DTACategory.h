//
//  DTACategory.h
//  arsMobile
//
//  Created by Michael Artuerhof on 13/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTACategory : NSObject

@property (nonatomic) int64_t cid;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) NSInteger count;

@end
