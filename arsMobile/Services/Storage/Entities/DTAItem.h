//
//  DTAItem.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTACategory;

@interface DTAItem : NSObject

@property (nonatomic) int64_t iid;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) float price;
@property (strong, nonatomic) NSString *measureUnit;
@property (nonatomic) int16_t taxType;
@property (strong, nonatomic) DTACategory *category;

@end
