//
//  DTACartItem.h
//  arsMobile
//
//  Created by Michael Artuerhof on 26/12/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTAItem;

@interface DTACartItem : NSObject

@property (nonatomic) int64_t cid;
@property (assign, nonatomic) float count;
@property (strong, nonatomic) DTAItem *item;
@property (assign, nonatomic) float discountPercentage;

@end
