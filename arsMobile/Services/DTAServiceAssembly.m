//
//  DTAServiceAssembly.m
//  arsMobile
//
//  Created by Michael Artuerhof on 07.12.17.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTAServiceAssembly.h"
#import "DTAFPrinterService.h"
#import "DTAFPrinterProtocol.h"
#import "DTAFPrinterService.h"
#import "DTAStorageProtocol.h"
#import "DTAStorageHandler.h"

@implementation DTAServiceAssembly

- (id<DTAStorageProtocol>)dataStorageService {
    return [TyphoonDefinition withClass:[DTAStorageHandler class] configuration:^(TyphoonDefinition *definition) {
        [definition setScope:TyphoonScopeSingleton];
    }];
}

- (id<DTAFPrinterProtocol>)fiscalPrinterService {
    return [TyphoonDefinition withClass:[DTAFPrinterService class] configuration:^(TyphoonDefinition *definition) {
        [definition setScope:TyphoonScopeSingleton];
    }];
}

@end
