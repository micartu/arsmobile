//
//  DTRBottomView.m
//  ios-POSService
//
//  Created by Michael Artuerhof on 16/08/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTRBottomView.h"

@implementation DTRBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self initialize];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self initialize];
    }
    
    return self;
}

- (void)initialize
{
    UIView *border = [UIView new];
    border.backgroundColor = [UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:211.0/255.0 alpha:1];
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.frame.size.width, 1);
    [self addSubview:border];
//    self.layer.masksToBounds = NO;
//    self.layer.shadowOffset = CGSizeMake(-15, 20);
//    self.layer.shadowRadius = 5;
//    self.layer.shadowOpacity = 0.5;
}

@end
