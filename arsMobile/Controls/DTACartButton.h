//
//  DTACartButton.h
//  ios-POSService
//
//  Created by Michael Artuerhof on 16/08/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTACartButton : UIControl
- (void)setBadgeCount:(NSInteger)count;
@end
