//
//  DTACartButton.m
//  ios-POSService
//
//  Created by Michael Artuerhof on 16/08/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACartButton.h"

static const CGFloat kAnimationDuration = 0.3;
static const CGFloat kAnimationDamping = 0.6;
static const CGFloat kAnimationScale = 1.4;

@interface DTACartButton () {
    BOOL _animate;
}
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *badgeLabel;
@end

@implementation DTACartButton

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)typhoonDidInject {
    [self drawItemsCountBadge];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didRecieved {
    [self drawItemsCountBadge];
}

- (void)initSubviews {
    self.frame = CGRectMake(0, 0, 44, 44);
    self.userInteractionEnabled = YES;
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.frame = CGRectMake(0, 0, 44, 44);
    [self.button setImage:[UIImage imageNamed:@"cart"] forState:UIControlStateNormal];
    [self.button setTitle:@"" forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(sendEvent) forControlEvents:UIControlEventTouchUpInside];
    
    self.badgeLabel = [[UILabel alloc] init];
    self.badgeLabel.frame = CGRectMake(26, 6, 14, 14);
    self.badgeLabel.backgroundColor = [UIColor redColor];
    self.badgeLabel.layer.cornerRadius = 7;
    self.badgeLabel.layer.masksToBounds = YES;
    self.badgeLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:8];
    self.badgeLabel.textAlignment = NSTextAlignmentCenter;
    self.badgeLabel.textColor = [UIColor whiteColor];
    self.badgeLabel.userInteractionEnabled = NO;
    
    [self addSubview:self.button];
    [self addSubview:self.badgeLabel];
}

- (void)drawItemsCountBadge {
    self.badgeLabel.frame = _count > 9 ? CGRectMake(26, 6, 18, 14) : CGRectMake(26, 6, 14, 14);
    self.badgeLabel.alpha = _count > 0 ? 1 : 0;
    self.badgeLabel.text = [@(_count) stringValue];
    if (_animate) {
        self.badgeLabel.transform = CGAffineTransformScale(CGAffineTransformIdentity,
                                                           kAnimationScale,
                                                           kAnimationScale);
        [UIView animateWithDuration:kAnimationDuration
                              delay:0
             usingSpringWithDamping:kAnimationDamping
              initialSpringVelocity:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations: ^{
                             self.badgeLabel.transform = CGAffineTransformIdentity;
                             _animate = NO;
                         }
                         completion:nil];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self sendEvent];
}

- (void)sendEvent {
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)setBadgeCount:(NSInteger)count {
    if (count > 0 && _count != count)
        _animate = YES;
    _count = count;
    [self drawItemsCountBadge];
}

@end
