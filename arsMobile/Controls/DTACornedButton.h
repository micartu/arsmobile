//
//  DTACornedButton.h
//  ios-POSService
//
//  Created by Michael Artuerhof on 16/08/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface DTACornedButton : UIButton

@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor *backColor;
@property (nonatomic, strong) IBInspectable UIColor *frontColor;
@property (nonatomic, strong) IBInspectable UIColor *disabledBackColor;
@property (nonatomic, strong) IBInspectable UIColor *disabledFrontColor;

@end
