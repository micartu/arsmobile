//
//  DTACornedButton.m
//  ios-POSService
//
//  Created by Michael Artuerhof on 16/08/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "DTACornedButton.h"

static const CGFloat kDefaultRadius = 5.0;
static const CGFloat kDefaultBorderWidth = 2.0;
static const CGFloat kAnimationDuration = 0.3;
static const CGFloat kAnimationDamping = 0.6;
static const CGFloat kAnimationScale = 1.03;

@implementation DTACornedButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    if (self.borderWidth == 0)
        self.borderWidth = kDefaultBorderWidth;

    if (self.cornerRadius == 0)
        self.cornerRadius = kDefaultRadius;

    if (!self.backColor)
        self.backColor = [UIColor whiteColor];

    if (!self.frontColor)
        self.frontColor = [UIColor blueColor];
}

#pragma mark - Property Setters

- (void)setFrontColor:(UIColor*)color {
    _frontColor = color;
    self.layer.borderColor = color.CGColor;
}

- (void)setBackColor:(UIColor*)color {
    _backColor = color;
    self.layer.backgroundColor = color.CGColor;
}

- (void)setBorderWidth:(CGFloat)width {
    _borderWidth = width;
    self.layer.borderWidth = width;
}

- (void)setCornerRadius:(CGFloat)radius {
    _cornerRadius = radius;
    
    self.layer.cornerRadius = self.cornerRadius;
    if (self.cornerRadius > 0)
        self.layer.masksToBounds = YES;
}

#pragma mark - States

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    if (enabled) {
        self.layer.borderColor = self.frontColor.CGColor;
        self.layer.backgroundColor = self.backColor.CGColor;
    }
    else {
        self.layer.borderColor = self.disabledFrontColor.CGColor;
        self.layer.backgroundColor = self.disabledBackColor.CGColor;
    }
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity,
                                            kAnimationScale,
                                            kAnimationScale);
    [UIView animateWithDuration:kAnimationDuration
                          delay:0
         usingSpringWithDamping:kAnimationDamping
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations: ^{
                         self.transform = CGAffineTransformIdentity;
                     }
                     completion:nil];
}

@end
